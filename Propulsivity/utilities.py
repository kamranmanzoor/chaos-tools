#-------------------------------------------------------------------------------
# Name:        Utilities and support modules
# Purpose:     Auxiliary functions for Propulsivity
#
# Author:      Konstantin Khait
#
# Created:     01/12/2013
# Copyright:   (c) Konstantin Khait 2013
# Licence:     GPL
#-------------------------------------------------------------------------------

import hashlib

def __makehex(string):
    hexval = ""
    for c in string:
        hexval += str(hex(ord(c)))[2:]
    return hexval

def __hexdig(h):
    if (h >= '0') and (h <= '9'):
        return ord(h) - ord('0')
    else:
        if (h <= 'f') and (h >= 'a'):
            return ord(h) - ord('a') + 10
        else:
            if (h <= 'F') and (h >= 'A'):
                return ord(h) - ord('A') + 10
    raise Exception("It is not a hex char")

def __restorehex(hexline):
    odd = True
    result = ""
    code = 0
    for h in hexline:
        if odd:
            code = __hexdig(h) << 4
            odd = False
        else:
            code = code + __hexdig(h)
            odd = True
            result += chr(code)
    return result

def __gethash(msg):
    hashx = hashlib.sha1()
    hashx.update(msg.encode())
    return hashx.hexdigest()

