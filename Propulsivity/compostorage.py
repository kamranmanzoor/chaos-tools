#-------------------------------------------------------------------------------
# Name:        Quick access file storage
# Purpose:     Store files in hierarchical file system with limited access time
#
# Author:      Konstantin Khait
#
# Created:     30/11/2013
# Copyright:   (c) Componentality Oy, 2013
# Licence:     LGPL
#-------------------------------------------------------------------------------
# The main goal of this module is to create file path in the storage. If we
# store all files in a single folder, this folder will contain too many
# elements and search time becomes inacceptable. Therefore CompoStorage creates
# a set of subfolders splitting source name by subnames.
# For example, the file "itismyverylongfilenametobesaved" will be stored as
# "it/ismy/verylong/filenametobesave/d".
# In practice the source name is usually even longer because Propulsivity uses
# record names plus prefixes in a hex form.
#-------------------------------------------------------------------------------

import os

# Creates relative file path. Source file name is spllitted by 2 then
# 4 then 8 then 16 symbols to limit number of subdirectories in each folder
def __makepath(filename):
    quantum = 2
    fname = ""
    while len(filename) > 0:
        if len(filename) < quantum:
            quantum = len(filename)
        fsegment = filename[0:quantum]
        filename = filename[quantum:]
        if quantum < 16:
            quantum *= 2
        fname = os.path.join(fname, fsegment)
    return fname

# Write entire file. Creates a set of subdirectories and "content" file
# and stores given data there. Files are written and read in a whole only
def compostorageWriteFile(filename, content, rootdir):
    folder = os.path.join(rootdir, __makepath(filename))
    if not os.path.exists(folder):
        os.makedirs(folder)
    file = open(os.path.join(folder, "content"), 'w')
    file.write(content)
    file.close()
    return os.path.exists(os.path.join(folder, "content"))

# Reads file content with path creation
def compostorageReadFile(filename, rootdir):
    filename = os.path.join(rootdir, __makepath(filename))
    filename = os.path.join(filename, "content")
    if not os.path.exists(filename):
        return ""
    file = open(filename, 'r')
    content = file.read()
    file.close()
    return content
