#-------------------------------------------------------------------------------
# Name:        Manage pseudo database
# Purpose:     Managed indexed structures
#
# Author:      Konstantin Khait
#
# Created:     30/11/2013
# Copyright:   (c) Componentality Oy, 2013
# Licence:     LGPL
#-------------------------------------------------------------------------------

import compostorage
import os
import time
import utilities

def lock(name, rootdir, timeout):
    lockpath = compostorage.__makepath(utilities.__makehex(name))
    lockfile = os.path.join(rootdir, lockpath + ".lock")
    time_s = time.time()
    while time.time() - time_s < timeout:
        try:
            if os.path.exists(lockfile):
                time.sleep(0.1)
                continue
            else:
                os.makedirs(lockfile)
                break
        except:
            time.sleep(0.1)

def unlock(name, rootdir):
    lockpath = compostorage.__makepath(utilities.__makehex(name))
    lockfile = os.path.join(rootdir, lockpath + ".lock")
    if os.path.exists(lockfile):
        os.removedirs(lockfile)

def lockrecord(record_name, rootdir, timeout):
    lock(record_name + ".content", rootdir, timeout)
    lock(record_name + ".metadata", rootdir, timeout)

def unlockrecord(record_name, rootdir):
    unlock(record_name + ".content", rootdir)
    unlock(record_name + ".metadata", rootdir)

def __addIndex(index, contentfile, rootdir):
    indexref = utilities.__makehex(index)
    outline = ""
    if len(indexref) > 0:
        lock(indexref, rootdir, 3)
        try:
            prevcont = compostorage.compostorageReadFile(indexref, rootdir)
            prevcont = prevcont.split('\n')
            for contline in prevcont:
                if (contline == contentfile):
                    unlock(indexref, rootdir)
                    return
                else:
                    if contline != '':
                        outline += contline + '\n'
        except:
            pass
        outline += contentfile + '\n'
        compostorage.compostorageWriteFile(indexref, outline, rootdir)
        unlock(indexref, rootdir)

def __removeIndex(index, contentfile, rootdir):
    indexref = utilities.__makehex(index)
    lock(indexref, rootdir, 3)
    outline = ""
    if len(indexref) > 0:
        prevcont = compostorage.compostorageReadFile(indexref, rootdir)
        prevcont = prevcont.split('\n')
        for contline in prevcont:
            if (contline != contentfile):
                if contline != '':
                    outline += contline + '\n'
        compostorage.compostorageWriteFile(indexref, outline, rootdir)
    unlock(indexref, rootdir)

def __getByIndex(index, rootdir):
    indexref = utilities.__makehex(index)
    lock(indexref, rootdir, 3)
    cont = compostorage.compostorageReadFile(indexref, rootdir)
    unlock(indexref, rootdir)
    return cont.split('\n')

def save(record_id, content, metadata, rootdir):
    # Save record itself
    lock(record_id, rootdir, 3)
    compostorage.compostorageWriteFile(record_id, content, rootdir)
    if metadata:
        mdata = metadata.split('\n')
        corepath = os.path.join(rootdir, compostorage.__makepath(record_id))
        mfile = open(os.path.join(corepath, "metadata"), 'w')
        mfile.write(metadata)
        mfile.close()
        # Add indeces
        for i in mdata:
            i = i.strip()
            if i != "":
                __addIndex(i, record_id, rootdir)
    unlock(record_id, rootdir)

def remove(record_id, rootdir):
    lock(record_id, rootdir, 3)
    corepath = os.path.join(rootdir, compostorage.__makepath(record_id))
    path = os.path.join(corepath, "metadata")
    if os.path.exists(path):
        mfile = open(path, 'r')
        metadata = mfile.read().split('\n')
        mfile.close()
        for i in metadata:
            i = i.strip()
            if i != "":
                __removeIndex(i, record_id, rootdir)
        os.remove(os.path.join(corepath, "metadata"))
    os.remove(os.path.join(corepath, "content"))
    unlock(record_id, rootdir)

def addAttribute(record_id, index, rootdir):
    lock(record_id, rootdir, 3)
    corepath = os.path.join(rootdir, compostorage.__makepath(record_id))
    path = os.path.join(corepath, "metadata")
    if not os.path.exists(path):
        metadata = []
    else:
        mfile = open(path, 'r')
        metadata = mfile.read().split('\n')
        mfile.close()
    newline = ""
    for i in metadata:
        i = i.strip()
        if i == index:
            unlock(record_id, rootdir)
            return
        else:
            newline += i + '\n'
    newline += index
    mfile = open(os.path.join(corepath, "metadata"), 'w')
    mfile.write(newline)
    mfile.close()
    __addIndex(index, record_id, rootdir)
    unlock(record_id, rootdir)

def removeAttribute(record_id, index, rootdir):
    lock(record_id, rootdir, 3)
    corepath = os.path.join(rootdir, compostorage.__makepath(record_id))
    mfile = open(os.path.join(corepath, "metadata"), 'r')
    metadata = mfile.read().split('\n')
    mfile.close()
    newline = ""
    for i in metadata:
        i = i.strip()
        if i != index:
            newline += i + '\n'
    mfile = open(os.path.join(corepath, "metadata"), 'w')
    mfile.write(newline)
    mfile.close()
    __removeIndex(index, record_id, rootdir)
    unlock(record_id, rootdir)

def changeAttribute(recordid, oldvalue, newvalue, rootdir):
    removeAttribute(recordid, oldvalue, rootdir)
    addAttribute(recordid, newvalue, rootdir)

def load(record_id, rootdir):
    lock(record_id, rootdir, 3)
    content = compostorage.compostorageReadFile(record_id, rootdir)
    unlock(record_id, rootdir)
    return content

def loadMetadata(record_id, rootdir):
    lock(record_id, rootdir, 3)
    try:
        corepath = os.path.join(rootdir, compostorage.__makepath(record_id))
        mpath = os.path.join(corepath, "metadata")
        mfile = open(mpath, 'r')
        metadata = mfile.read().split('\n')
        mfile.close()
    except:
        metadata = []
    unlock(record_id, rootdir)
    return metadata

def list(index, rootdir):
    return __getByIndex(index, rootdir)