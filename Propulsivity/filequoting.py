#-------------------------------------------------------------------------------
# Name:        Extended directory management functions
# Purpose:     Remove stale data and keep directory size limited
#
# Author:      Konstantin Khait
#
# Created:     01/12/2013
# Copyright:   (c) Konstantin Khait 2013
# Licence:     GPL
#-------------------------------------------------------------------------------

import time
import os

# Get time of file modification
def __getFileTime(filename):
    return os.path.getmtime(filename)

# Create list of files in given folder
def __createFileList(folder):
    filelist = []
    for subdir, dirs, files in os.walk(folder):
        for file in files:
            fpath = os.path.join(folder, file)
            if os.path.exists(fpath):
                filelist.append(fpath)
    return filelist

# Get file size
def __getFileSize(filename):
    return os.path.getsize(filename)

# Sort files by size
def __sortBySize(filelist):
    i = 0
    while i < len(filelist) / 2:
        j = 1
        while j < len(filelist):
            if __getFileTime(filelist[j - 1]) < __getFileTime(filelist[j]):
                temp = filelist[j - 1]
                filelist[j - 1] = filelist[j]
                filelist[j] = temp
            j += 1
        i += 1
    return filelist

# Get total size of entire folder
def __getFolderSize(folder):
    filelist = __createFileList(folder)
    size = 0
    for file in filelist:
        size += __getFileSize(file)
    return size

# If size of the folder is larger than quote, remove oldest files
# until size becomes less or equal to setto value provided
def keepQuote(folder, quote, setto = None):
    if not setto:
        setto = quote
    if not os.path.exists(folder):
        return
    size = __getFolderSize(folder)
    if size < quote:
        return
    filelist = __createFileList(folder)
    filelist = __sortBySize(filelist)
    while size > setto:
        size -= __getFileSize(filelist[0])
        os.remove(filelist[0])
        del filelist[0]

# Split file to smaller chunks
def splitFile(filename, output_dir, chunk_size):
    name_postfix = ".p.part."
    basename = os.path.basename(filename)
    if chunk_size <= len(basename) + len(name_postfix) + 20:
        return False
    chunk_size -= len(basename) + len(name_postfix) + 12
    size = __getFileSize(filename)
    written = 0
    file = open(filename, "rb")
    i = 0
    while written < size:
        part = file.read(chunk_size)
        target_name = basename + name_postfix + str(i)
        next_name = basename + name_postfix + str(i + 1)
        target_path = os.path.join(output_dir, target_name)
        wfile = open(target_path, "wb")
        if written + len(part) < size:
            wfile.write(next_name.encode("utf-8") + b'\x00')
        else:
            wfile.write(b'\x00')
        wfile.write(part)
        wfile.close()
        written += len(part)
        i += 1
    file.close()
    return basename + name_postfix + "0"

# Make file from smaller chunks
def buildFile(source, input_dir, result):
    _result = True
    try:
        wfile = open(result, "wb")
        while True:
            file = open(os.path.join(input_dir, source), "rb")
            part = file.read()
            _part = bytearray()
            _next = ""
            is_next_name = True
            for b in part:
                if is_next_name:
                    if b != 0:
                        _next += chr(b)
                    else:
                        is_next_name = False
                else:
                    _part.append(b)
            wfile.write(_part)
            file.close()
            if _next != "":
                source = _next
            else:
                break
        wfile.close()
    except:
        _result = False
    return _result

# Make a list of chunks
def listChunks(source, input_dir):
    _result =[source]
    try:
        while True:
            file = open(os.path.join(input_dir, source), "rb")
            part = file.read()
            _next = ""
            is_next_name = True
            for b in part:
                if is_next_name:
                    if b != 0:
                        _next += chr(b)
                    else:
                        _result.append(_next)
                        break
            file.close()
            if _next != "":
                source = _next
            else:
                break
    except:
        _result = []
    return _result

