#-------------------------------------------------------------------------------
# Name:        Document
# Purpose:     Simplify documents management in pseudobase
#
# Author:      Konstantin Khait
#
# Created:     04/12/2013
# Copyright:   (c) Componentality Oy, 2013
# Licence:     GPL
#-------------------------------------------------------------------------------
# This is the API file providing high level document (practically file)
# abstraction over pseudobase functions. Simpifies access to raw data
# records
#-------------------------------------------------------------------------------

import pseudobase
import utilities
import records

# Create record name as a hex representation of the document name. It is to
# allow using any characters in document name
def makeRecordName(docname):
    return utilities.__makehex(docname)

# Make document name from record name
def makeDocumentName(recname):
    return utilities.__restorehex(recname)

# Write document in a whole
def write(docname, content, rootdir):
    pseudobase.lockrecord(docname, rootdir, 3)
    name = makeRecordName(docname)
    pseudobase.save(name, content, None, rootdir)
    pseudobase.addAttribute(name, "_document_", rootdir)
    pseudobase.unlockrecord(docname, rootdir)

# Read document
def read(docname, rootdir):
    name = makeRecordName(docname)
    try:
        content = pseudobase.load(name, rootdir)
    except:
        content = ""
    return content

# Create document. Not needed in practice as "write" can be used instead of it
def create(docname, rootdir):
    name = makeRecordName(docname)
    try:
        content = pseudobase.load(name, rootdir)
    except:
        content = ""
    if content != "":
        return False
    else:
        write(docname, content, rootdir)
    return True

# Remove the document
def remove(docname, rootdir):
    name = makeRecordName(docname)
    try:
        pseudobase.remove(name, rootdir)
    except:
        return False
    return True

# List all documents
def list(rootdir):
    docs = pseudobase.list("_document_", rootdir)
    result = []
    for d in docs:
        result.append(utilities.__restorehex(d))
    return result
