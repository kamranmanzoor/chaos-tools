#-------------------------------------------------------------------------------
# Name:        Tagging of records
# Purpose:     Processing of record tags and filtering
#
# Author:      Konstantin Khait
#
# Created:     01/12/2013
# Copyright:   (c) Konstantin Khait, 2013
# Licence:     GPL
#-------------------------------------------------------------------------------

import records
import pseudobase
import utilities

def addTags(record, tagset, rootdir):
    for key in tagset.keys():
        # Remove older value for the tag to avoid extra attributes
        oldvalue = getTagValue(record, key, rootdir)
        if oldvalue != "":
            removeTag(record, key, oldvalue, rootdir)
        pseudobase.lockrecord(record, rootdir, 3)
        pseudobase.addAttribute(record, key + ".tag", rootdir)
        pseudobase.addAttribute(record, key + "=" + tagset[key] + ".tagval", rootdir)
        pseudobase.unlockrecord(record, rootdir)

def findByTag(tag, rootdir):
    result = pseudobase.list("tag." + tag, rootdir)
    result.extend(pseudobase.list(tag + ".tag", rootdir))
    return result

def findByTagValue(tag, value, rootdir):
    result = pseudobase.list("tagval." + tag + "=" + value, rootdir)
    result.extend(pseudobase.list(tag + "=" + value + ".tagval", rootdir))
    return result

def selectAll(tagset, rootdir):
    target = None
    for key in tagset.keys():
        select = findByTagValue(key, tagset[key], rootdir)
        if target == None:
            target = select
        else:
            target = target and select
    return list(target)

def selectAny(tagset, rootdir):
    target = set()
    for key in tagset.keys():
        select = findByTagValue(key, tagset[key], rootdir)
        target = target or select
    return list(target)

def removeTags(record, tagset, rootdir):
    pseudobase.lockrecord(record, rootdir, 3)
    for key in tagset.keys():
        pseudobase.removeAttribute(record, "tag." + key, rootdir)
        pseudobase.removeAttribute(record, "tagval." + key + "=" + tagset[key], rootdir)
        pseudobase.removeAttribute(record, key + ".tag", rootdir)
        pseudobase.removeAttribute(record, key + "=" + tagset[key] + ".tagval", rootdir)
    pseudobase.unlockrecord(record, rootdir)

def __roundTo(x, granularity):
    negative = False
    x = float(x)
    granularity = float(granularity)
    if x < 0:
        negative = True
        x = -x
    x = float(int(x / granularity)) * granularity
    if negative:
        x = -x - granularity
    return x

def __roundToInterval(x, granularity):
    granularity = float(granularity)
    result = []
    result.append(__roundTo(x, granularity))
    result.append(result[0] + granularity)
    return result


def addTag(record, key, value, rootdir):
    tagset = {}
    records.addToRecordSet(key, value, tagset)
    addTags(record, tagset, rootdir)

def removeTag(record, key, value, rootdir):
    tagset = {}
    records.addToRecordSet(key, value, tagset)
    removeTags(record, tagset, rootdir)

def addIntervalTag(record, key, value, granularity, rootdir):
    granularity = float(granularity)
    interval = __roundToInterval(value, granularity)
    tagset = {}
    records.addToRecordSet(key + ".low.", str(interval[0]), tagset)
    records.addToRecordSet(key + ".up.", str(interval[1]), tagset)
    records.addToRecordSet(key + ".val.", value, tagset)
    addTags(record, tagset, rootdir)

def removeIntervalTag(record, key, value, granularity, rootdir):
    granularity = float(granularity)
    interval = __roundToInterval(value, granularity)
    tagset = {}
    records.addToRecordSet(key + ".low.", str(interval[0]), tagset)
    records.addToRecordSet(key + ".up.", str(interval[1]), tagset)
    records.addToRecordSet(key + ".val.", value, tagset)
    removeTags(record, tagset, rootdir)

def findByValue(key, value, rootdir):
    tag = key + ".val."
    return findByTagValue(tag, value, rootdir)

def findByInterval(key, value, granularity, rootdir):
    granularity = float(granularity)
    interval = __roundToInterval(value, granularity)
    tagset = {}
    records.addToRecordSet(key + ".low.", str(interval[0]), tagset)
    records.addToRecordSet(key + ".up.", str(interval[1]), tagset)
    return selectAll(tagset, rootdir)

def getTagValue(record, tag, rootdir):
    metadata = pseudobase.loadMetadata(record, rootdir)
    keyline = "tagval." + tag + "="
    for m in metadata:
        if m[0:len(keyline)] == keyline:
            return m[len(keyline):]
    keyline = tag + "="
    for m in metadata:
        if m[0:len(keyline)] == keyline:
            res = m[len(keyline):]
            return res[0:len(res) - len(".tagval")]
    return ""

def getIntervalTagValue(record, tag, rootdir):
    return getTagValue(record, tag + ".val.", rootdir)

def sortByTagValue(recordset, tag, rootdir):
    i = 0
    while i < len(recordset) / 2:
        j = 1
        while j < len(recordset):
            if recordset[j] == '':
                break
            recprev = recordset[j - 1]
            reccur = recordset[j]
            val = getTagValue(reccur, tag, rootdir)
            valprev = getTagValue(recprev, tag, rootdir)
            if float(val) < float(valprev):
                recordset[j] = recprev
                recordset[j - 1] = reccur
            j += 1
        i += 1
    return recordset

def sortByIntervalTagValue(recordset, tag, rootdir):
    return sortByTagValue(recordset, tag + ".val.", rootdir)

