#-------------------------------------------------------------------------------
# Name:        Context substitutor
# Purpose:     Provide context substitution functions
#
# Author:      Konstantin Khait
#
# Created:     01/12/2013
# Copyright:   (c) Konstantin Khait 2013
# Licence:     GPL
#-------------------------------------------------------------------------------
# Simple, but very important module which sequentially substitutes the set of
# variables implemented as dictionary to the given text. "substitute" is only
# an API function to be used, it does cyclic substitution until nothing to
# substitute.
#-------------------------------------------------------------------------------

import records
import time

# Substitute all the set of variables once
def contextSubstitution(source, context):
    dest = source
    for key in context.keys():
        varname = "$(" + key + ")"
        dest = dest.replace(varname, context[key])
    return dest

# Substitute variables until nothing to do (may cause infinite loop!)
def substitute(source, context):
    while True:
        dest = contextSubstitution(source, context)
        if dest == source:
            return dest
        else:
            source = dest

# Join two contexts
def sumContexts(ctx1, ctx2):
    result = ctx1
    for c in ctx2.keys():
        if c != "":
            result[c] = ctx2[c]
    return result

# Combine two contexts using timestamps of each attribute
# Each context is represented with two context structures joined by tuples
def __combine(struct1, struct2):
    ctx1, tms1 = struct1
    ctx2, tms2 = struct2
    result = {}
    tms = {}
    for c1 in ctx1.keys():
        # If c1 presents in both of contexts
        if c1 in ctx2.keys():
            # If there is a timestamp for ctx1[c1]...
            if c1 in tms1.keys():
                # If there are both time stamps
                if c1 in tms2.keys():
                    t1 = tms1[c1]
                    t2 = tms2[c1]
                    # Choose the freshiest instance
                    if float(t1) >= float(t2):
                        result[c1] = ctx1[c1]
                        tms[c1] = tms1[c1]
                    else:
                        result[c1] = ctx2[c1]
                        tms[c1] = tms2[c1]
                else:
                    # If no second timestamp, use ctx1[c1]
                    result[c1] = ctx1[c1]
                    tms[c1] = tms1[c1]
            else:
                # If there is only timestamp for ctx2[c1], use it
                if c1 in tms2.keys():
                    result[c1] = ctx2[c1]
                    tms[c1] = tms2[c1]
                else:
                    # No timestamps at all
                    result[c1] = ctx1[c1]
        else:
            # If there is no such item in ctx2 at all
            if c1 in tms1.keys():
                result[c1] = ctx1[c1]
                tms[c1] = tms1[c1]
            else:
                # ctx2 has no such item, ctx1 has no time stamp
                result[c1] = ctx1[c1]
    for c2 in ctx2.keys():
        if not (c2 in result.keys()):
            result[c2] = ctx2[c2]

    return result, tms

def combine(struct1, struct2):
    result = __combine(struct1, struct2)
    return result

def addTimeStamps(ctx):
    tms = {}
    for c in ctx.keys():
        tms[c] = time.time()
    return ctx, tms
