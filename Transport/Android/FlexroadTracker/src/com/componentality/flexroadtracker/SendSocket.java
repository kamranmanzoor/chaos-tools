package com.componentality.flexroadtracker;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import android.os.StrictMode;
import android.util.Log;

public class SendSocket {
	
	static Socket s_send;
	static OutputStream out_stream;
	static InetSocketAddress addr_send;
	static String address = "proxy.multi-portal.org";
	static int port = 1979;
	static String TAG = "Flexroad Tracker";
	
	public SendSocket() throws IOException{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		s_send = new Socket();//(address, port);
		addr_send = new InetSocketAddress(address, port);
		
		Log.v(TAG,"try new send Socket "+address+":"+port);
		s_send.setSoTimeout(5000);
		s_send.connect(addr_send, 5000);
	    out_stream = s_send.getOutputStream();
	}

public void write(byte[] buffer)
{
	if(isConnected()){
		try {
			out_stream.write(buffer);
			Log.v(TAG,"Socket write "+new String(buffer));
		} catch (IOException e) {
			Log.v(TAG,"Socket write error "+e);
		}
	}
	close();
}

public static void close()
{
	if (isConnected())
		try {
			s_send.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
}

public static boolean isConnected()
{
	return s_send.isConnected();
}

public static boolean isClosed()
{
	return s_send.isClosed();
}

}
