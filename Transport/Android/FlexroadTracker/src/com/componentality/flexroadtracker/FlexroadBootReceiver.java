package com.componentality.flexroadtracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class FlexroadBootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent myIntent = new Intent(context, FlexroadService.class);
	    context.startService(myIntent);

	}

}
