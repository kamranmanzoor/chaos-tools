#ifndef CTD_CMD_H
#define CTD_CMD_H

#include "../../CommonLibs/common/common_utilities.h"
#include "ctd_chunk.h"
#include "ctd_ctx.h"

namespace Componentality
{
	namespace CTD
	{
		class CommandProcessor
		{
		protected:
			ChunkProvider& mProvider;
		public:
			CommandProcessor(ChunkProvider& provider) : mProvider(provider) {};
			virtual ~CommandProcessor() {};
			virtual CST::Common::blob operator()(const CST::Common::blob&);
		};

		class Exchanger;

		class Agent
		{
		public:
			Agent() {};
			virtual ~Agent() {};
		public:
			virtual void onStart(Exchanger&) = 0;
			virtual void onEnd(Exchanger&) = 0;
			virtual void onConnect(Exchanger&) = 0;
			virtual void onDisconnect(Exchanger&) = 0;
		};

		class DefAgent : public Agent
		{
		protected:
			std::string mReadFolder;
			std::string mWriteFolder;
			Context& mContext;
		public:
			DefAgent(Context& ctx, const std::string readfolder, const std::string writefolder);
			virtual ~DefAgent();
		public:
			virtual void onStart(Exchanger&);
			virtual void onEnd(Exchanger&);
			virtual void onConnect(Exchanger&);
			virtual void onDisconnect(Exchanger&);
		};

		class StdAgent : public Agent
		{
		protected:
			std::string mReadFolder;
			std::string mWriteFolder;
			bool mOneAtATime;
			bool mPut;
			Context& mContext;
		public:
			StdAgent(Context& ctx, const std::string readfolder, const std::string writefolder, const bool put = false, const bool one_at_a_time = false);
			virtual ~StdAgent();
		public:
			virtual void onStart(Exchanger&);
			virtual void onEnd(Exchanger&);
			virtual void onConnect(Exchanger&);
			virtual void onDisconnect(Exchanger&);
		};

        class GetAgent : public Agent
		{
		protected:
			std::list<std::string> mRequest;
			std::string mWriteFolder;
			bool mOneAtATime;
			int mCounter;
			Context& mContext;
		public:
			GetAgent(Context& ctx, const std::list<std::string>& request, const std::string writefolder, const bool one_at_a_time = false);
			virtual ~GetAgent();
		public:
			virtual void onStart(Exchanger&);
			virtual void onEnd(Exchanger&);
			virtual void onConnect(Exchanger&);
			virtual void onDisconnect(Exchanger&);
		};

		class SendAgent : public Agent
		{
		protected:
			std::list<std::string> mSendList;
			std::string mWriteFolder;
			bool mOneAtATime;
			bool mPut;
			Context& mContext;
		public:
			SendAgent(Context& ctx, const std::list<std::string>& sendlist, const std::string writefolder, const bool put = false, const bool one_at_a_time = false);
			virtual ~SendAgent();
		public:
			virtual void onStart(Exchanger&);
			virtual void onEnd(Exchanger&);
			virtual void onConnect(Exchanger&);
			virtual void onDisconnect(Exchanger&);
		};

		class Exchanger : protected CommandProcessor
		{
		protected:
			CST::Common::blob mReadBuffer;
			CST::Common::blob mWriteBuffer;
			CST::Common::mutex mReadLock;
			CST::Common::mutex mWriteLock;
			CST::Common::mutex mAgentsLock;
			std::list<Agent*> mAgents;
		public:
			Exchanger(ChunkProvider&);
			virtual ~Exchanger();
		public:
			virtual void onReceived(CST::Common::blob data);
			virtual void send(CST::Common::blob data);
			virtual Exchanger& operator()(void);
			virtual bool isThereReadCommand();
			virtual bool isThereWriteCommand();
			virtual CST::Common::blob getDataToWrite(const size_t maxdata);
			virtual Exchanger& operator+= (Agent&);
			virtual Exchanger& operator-= (Agent&);
			virtual operator std::list<Agent*>();
		public:
			virtual void check(const std::string& chunk_name);
			virtual void get(const std::string& chunk_name);
			virtual void put(const std::string& chunk_name, CST::Common::blob data);
		public:
			virtual void onStart();
			virtual void onEnd();
			virtual void onConnect();
			virtual void onDisconnect();
		};

		class ExchangeProvider
		{
		public:
			ExchangeProvider() {};
			virtual ~ExchangeProvider() {};
		public:
			virtual operator Exchanger&() = 0;
		};

		struct SERVER_HANDLER
		{
			int mSocket;
			Exchanger* mProcessor;
			CST::Common::ThreadSet* mRunnable;
			bool mExitIfNoData;
		};

	}
}

#endif