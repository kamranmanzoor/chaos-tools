#ifndef CTD_SRV_H
#define CTD_SRV_H

#include "ctd_cmd.h"
#include "ctd_ctx.h"
#include "../../CommonLibs/common/common_utilities.h"

namespace Componentality
{
	namespace CTD
	{
		class Server : public Context, protected CST::Common::ThreadSet, public ExchangeProvider
		{
		protected:
			int mPort;
			int mSocket;
			std::list<CST::Common::thread*> mThreads;
			Exchanger mExchanger;
		public:
			Server(const int port);
			virtual ~Server();
			virtual void exit();
			virtual operator Exchanger&() { return mExchanger; }
		public:
			virtual void run() = 0;
		};

		class TCPServer : public Server
		{
		public:
			TCPServer(const int port);
			virtual ~TCPServer();
		public:
			virtual void run();
		};

		class UDPServer : public Server
		{
		protected:
			int mTimeout;
		public:
			UDPServer(const int port, const int timeout = 5);
			virtual ~UDPServer();
		public:
			virtual void run();
		};


	}
}

#endif