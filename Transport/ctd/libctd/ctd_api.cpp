#include "ctd_api.h"
#include "ctd_ctx.h"
#include "ctd_cmd.h"
#include "ctd_srv.h"
#include "ctd_cli.h"

using namespace Componentality::CTD;

static void srun(Server* svr)
{
	svr->run();
}

static void crun(Client* cli)
{
	cli->run();
}

Server* Componentality::CTD::createServer(const PROTOCOL proto, const int port)
{
	Server* svr;
	if (proto == TCP)
		svr = new TCPServer(port);
	else
		svr = new UDPServer(port);
	return svr;
}

CST::Common::thread* Componentality::CTD::runServer(Server* svr)
{
	return new CST::Common::thread((CST::Common::thread::threadfunc) srun, svr);
}

void Componentality::CTD::stopServer(CST::Common::thread* th, Server* svr)
{
	svr->exit();
	th->join();
	std::list<Agent*> aglist = svr->operator Exchanger &().operator std::list<Agent*>();
	for (std::list<Agent*>::iterator i = aglist.begin(); i != aglist.end(); i++)
	{
		svr->operator Exchanger &().operator-=(**i);
		delete *i;
	}
	delete svr;
	delete th;
}

Client* Componentality::CTD::createClient(const PROTOCOL proto, const std::string host, const int port, const int lport)
{
	Client* cli;
	if (proto == TCP)
		cli = new TCPClient(host, port);
	else
		if (lport > 0)
			cli = new UDPClient(host, port, lport, 5);
		else
			cli = new UDPClient(host, port);
	return cli;
}

CST::Common::thread* Componentality::CTD::runClient(Client* cli, const bool blocking)
{
	if (!blocking)
		return new CST::Common::thread((CST::Common::thread::threadfunc) crun, cli);
	else
	{
		crun(cli); return NULL;
	};
}

void Componentality::CTD::stopClient(CST::Common::thread* th, Client* cli)
{
	cli->exit();
	th->join();
	std::list<Agent*> aglist = cli->operator Exchanger &().operator std::list<Agent*>();
	for (std::list<Agent*>::iterator i = aglist.begin(); i != aglist.end(); i++)
	{
		cli->operator Exchanger &().operator-=(**i);
		delete *i;
	}
	delete cli;
	delete th;
}

Agent* Componentality::CTD::queueToCheck(Server* svr, const std::string readfolder, const std::string writefolder, const PROTOCOL proto)
{
	Agent* ag = new StdAgent(*svr, readfolder, writefolder, false, proto == UDP);
	svr->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::queueToPut(Server* svr, const std::string readfolder, const std::string writefolder, const PROTOCOL proto)
{
	Agent* ag = new StdAgent(*svr, readfolder, writefolder, true, proto == UDP);
	svr->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::queueToCheck(Server* svr, const std::list<std::string> sendlist, const std::string folder, const PROTOCOL proto)
{
	Agent* ag = new SendAgent(*svr, sendlist, folder, false, proto == UDP);
	svr->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::queueToPut(Server* svr, const std::list<std::string> sendlist, const std::string folder, const PROTOCOL proto)
{
	Agent* ag = new SendAgent(*svr, sendlist, folder, true, proto == UDP);
	svr->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::queueToGet(Server* svr, const std::list<std::string> files, const std::string folder, const PROTOCOL proto)
{
	Agent* ag = new GetAgent(*svr, files, folder, proto == UDP);
	svr->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::queueToCheck(Client* cli, const std::string readfolder, const std::string writefolder, const PROTOCOL proto)
{
	Agent* ag = new StdAgent(*cli, readfolder, writefolder, false, proto == UDP);
	cli->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::queueToPut(Client* cli, const std::string readfolder, const std::string writefolder, const PROTOCOL proto)
{
	Agent* ag = new StdAgent(*cli, readfolder, writefolder, true, proto == UDP);
	cli->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::queueToCheck(Client* cli, const std::list<std::string> sendlist, const std::string folder, const PROTOCOL proto)
{
	Agent* ag = new SendAgent(*cli, sendlist, folder, false, proto == UDP);
	cli->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::queueToPut(Client* cli, const std::list<std::string> sendlist, const std::string folder, const PROTOCOL proto)
{
	Agent* ag = new SendAgent(*cli, sendlist, folder, true, proto == UDP);
	cli->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::queueToGet(Client* cli, const std::list<std::string> files, const std::string folder, const PROTOCOL proto)
{
	Agent* ag = new GetAgent(*cli, files, folder, proto == UDP);
	cli->operator Exchanger &().operator+=(*ag);
	return ag;
}

void Componentality::CTD::stopTransfer(Server* svr, Agent* ag)
{
	svr->operator Exchanger &().operator-=(*ag);
	delete ag;
}

void Componentality::CTD::stopTransfer(Client* cli, Agent* ag)
{
	cli->operator Exchanger &().operator-=(*ag);
	delete ag;
}

Agent* Componentality::CTD::setupExchange(Server* srv, const std::string readfolder, const std::string writefolder)
{
	Agent* ag = new DefAgent(*srv, readfolder, writefolder);
	srv->operator Exchanger &().operator+=(*ag);
	return ag;
}

Agent* Componentality::CTD::setupExchange(Client* cli, const std::string readfolder, const std::string writefolder)
{
	Agent* ag = new DefAgent(*cli, readfolder, writefolder);
	cli->operator Exchanger &().operator+=(*ag);
	return ag;
}
