#include "ctd_chunk.h"

using namespace Componentality::CTD;

ChunkRotator::ChunkRotator()
{
	mIndex = 0;
}

ChunkRotator::~ChunkRotator()
{
}

std::pair<std::string, CST::Common::blob> ChunkRotator::get()
{
	mLock.lock();
	std::map<std::string, Chunk>::iterator mCurrent = locate();
	if (mCurrent != mChunks.end())
	{
		mLock.unlock(); 
		std::pair<std::string, CST::Common::blob> result(mCurrent->first, mCurrent->second.mContent);
		return result;
	}
	else
	{
		mLock.unlock();
		return std::pair<std::string, CST::Common::blob>(std::string(), CST::Common::blob());
	}
}

size_t ChunkRotator::getIndex()
{
	mLock.lock();
	size_t result = mIndex;
	mLock.unlock();
	return result;
}

ChunkRotator& ChunkRotator::operator++()
{
	mLock.lock();
	mIndex++;
	mLock.unlock();
	return *this;
}

std::map<std::string, Chunk>::iterator ChunkRotator::locate()
{
	std::map<std::string, Chunk>::iterator mCurrent = mChunks.begin();

	for (size_t i = 0; i < mIndex; i++)
	{
		if (mCurrent == mChunks.end())
		{
			mIndex = 0;
			return mCurrent;
		}
		else
			mCurrent++;
	}

	return mCurrent;
}

size_t ChunkRotator::getSize()
{
	mLock.lock();
	size_t size = mChunks.size();
	mLock.unlock();
	return size;
}