#ifndef CTD_CLI_H
#define CTD_CLI_H

#include "ctd_cmd.h"
#include "ctd_ctx.h"

namespace Componentality
{
	namespace CTD
	{
		class Client : public Context, protected CST::Common::ThreadSet, public ExchangeProvider
		{
		protected:
			Exchanger mExchange;
			std::string mHost;
			int mPort;
		public:
			Client(const std::string host, const int port);
			virtual ~Client();
			virtual operator Exchanger&() { return mExchange; }
			virtual void exit() { CST::Common::ThreadSet::exit(); }
		public:
			virtual void run() = 0;
		};


		class TCPClient : public Client
		{
		public:
			TCPClient(const std::string host, const int port);
			virtual ~TCPClient();
		public:
			virtual void run();
		};

		class UDPClient : public Client
		{
		protected:
			int mLocalPort;
			int mTimeout;
		public:
			UDPClient(const std::string host, const int port, const int localPort, const int timeout = 5);
			UDPClient(const std::string host, const int port, const int timeout = 5);
			virtual ~UDPClient();
		public:
			virtual void run();
		};

	}
}

#endif