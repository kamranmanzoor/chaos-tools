#include "ctd_srv.h"
#include "ctd_ctx.h"
#ifndef WIN32
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define closesocket close
#else
#include <Windows.h>
typedef int socklen_t;
#endif

using namespace Componentality::CTD;

unsigned int TCP_BUF_SIZE = 4096;
unsigned int UDP_BUF_SIZE = 4096;

Server::Server(const int port) : mPort(port), mExchanger(*this)
{
}

Server::~Server()
{
	mExchanger.onEnd();
}

void Server::exit()
{
	closesocket(mSocket);
	ThreadSet::exit();
}

/***********************************************************************************************************************************/

void server_handle(SERVER_HANDLER* handler)
{
	struct timeval tv;

	tv.tv_sec = 3;
	tv.tv_usec = 0;
	char* buf = new char[TCP_BUF_SIZE];
	handler->mProcessor->onConnect();
	do
	{
		CST::Common::blob dsend = handler->mProcessor->getDataToWrite(TCP_BUF_SIZE);
		size_t size_to_send = dsend.mSize;
		bool datasent = size_to_send != 0;
		char* ptr = dsend.mData;
		bool close_channel = false;
		while (size_to_send)
		{
			setsockopt(handler->mSocket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv, sizeof(struct timeval));
			int sent = send(handler->mSocket, ptr, size_to_send, 0);
			if (sent < 0)
			{
				close_channel = true;
				break;
			}
			size_to_send -= sent;
			ptr += sent;
		}
		dsend.purge();
		if (close_channel)
			break;
		setsockopt(handler->mSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
		int read = recv(handler->mSocket, buf, TCP_BUF_SIZE, 0);
		if (read > 0)
		{
			CST::Common::blob dread;
			dread.mData = buf;
			dread.mSize = read;
			handler->mProcessor->onReceived(dread);
		}
		else
		{
			if (!datasent)
				if (handler->mExitIfNoData)
					break;
				else
				{
					CST::Common::blob keepalive;
					keepalive.mSize = 2;
					keepalive.mData = (char*) (std::string() + (char) 0 + (char) 0).c_str();
					handler->mProcessor->send(keepalive);
				}
		}
		while (handler->mProcessor->isThereReadCommand() && !handler->mRunnable->getExit())
			(*handler->mProcessor)();
	} while (!handler->mRunnable->getExit());
	handler->mProcessor->onDisconnect();
	delete[] buf;
	delete handler;
	closesocket(handler->mSocket);
}

void TCPServer::run()
{
	int socket_type = SOCK_STREAM;
	int socket_proto = IPPROTO_TCP;
	int sock = socket(PF_INET, socket_type, socket_proto);

	if (sock < 0)
		return;

	struct sockaddr_in serv_addr, cli_addr;
	serv_addr.sin_family = PF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(mPort);

	int result = bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
	if (result < 0)
	{
		return;
	}

	mSocket = sock;

	listen(sock, 5);

	mExchanger.onStart();

	while (!Server::getExit())
	{
		socklen_t clilen = (socklen_t) sizeof(cli_addr);

		/* Accept actual connection from the client */
		int clisock = accept(sock, (struct sockaddr *)&cli_addr,
			&clilen);

		if (clisock > 0)
		{
			SERVER_HANDLER *sh = new SERVER_HANDLER;
			sh->mSocket = clisock;
			sh->mProcessor = &mExchanger;
			sh->mRunnable = this;
			sh->mExitIfNoData = false;
			ThreadSet::run((CST::Common::thread::threadfunc)server_handle, sh);
		}
	}

	closesocket(sock);
}

TCPServer::TCPServer(const int port) : Server(port)
{
#ifdef WIN32
	{
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2, 2), &wsaData);
	}
#endif
}

TCPServer::~TCPServer()
{
}

/******************************************************************************************************************************/

UDPServer::UDPServer(const int port, const int timeout) : Server(port), mTimeout(timeout)
{
#ifdef WIN32
	{
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2, 2), &wsaData);
	}
#endif
}

UDPServer::~UDPServer()
{

}

void UDPServer::run()
{
	int socket_type = SOCK_DGRAM;
	int socket_proto = IPPROTO_UDP;
	int sock = socket(AF_INET, socket_type, socket_proto);

	if (sock < 0)
		return;

	struct sockaddr_in serv_addr;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(mPort);

	int result = bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
	if (result < 0)
	{
		return;
	}

	mSocket = sock;

	struct timeval tv;

	char* buf = new char[UDP_BUF_SIZE];

	hostent* localHost = gethostbyname("0.0.0.0");
	char* localIP = inet_ntoa(*(struct in_addr *)*localHost->h_addr_list);

	struct sockaddr from;
	((sockaddr_in*)&from)->sin_family = AF_INET;
	((sockaddr_in*)&from)->sin_addr.s_addr = inet_addr(localIP);
	((sockaddr_in*)&from)->sin_port = htons(mPort);
	socklen_t from_len = sizeof(from);
	socklen_t *address_len = &from_len;

	bool connected = false;
	do
	{
		tv.tv_sec = mTimeout;
		tv.tv_usec = 0;
		CST::Common::blob datagram;
		setsockopt(mSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
		int read = recvfrom(mSocket, buf, UDP_BUF_SIZE, 0, (sockaddr*)&from, address_len);
		if (read > 0)
		{
			if (!connected)
				mExchanger.onConnect(), connected = true;
			datagram.mSize = read;
			datagram.mData = buf;
		}
		else
			if (connected)
				mExchanger.onDisconnect(), connected = false;

		if (datagram.mSize)
		{
			mExchanger.onReceived(datagram);
			mExchanger();

			CST::Common::blob dsend = mExchanger.getDataToWrite(0);
			if ((dsend.mSize <= UDP_BUF_SIZE) && (dsend.mSize > 0))
			{
				int size_to_send = dsend.mSize;
				int sent = sendto(mSocket, dsend.mData, dsend.mSize, 0, &from, *address_len);
				dsend.purge();
			}
		};
	} while (!getExit());
	delete[] buf;
}
