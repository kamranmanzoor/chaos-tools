#include "ctd_ctx.h"
#include "../../CommonLibs/common/common_utilities.h"

using namespace Componentality::CTD;
using namespace CST::Common;

Context::Context()
{
}

Context::~Context()
{
	mLock.lock();

	for (std::map<std::string, Chunk>::iterator i = mChunks.begin(); i != mChunks.end(); i++)
		i->second.mContent.purge();

	mChunks.clear();

	mLock.unlock();
}

void Context::load(const std::string& chunk_name, const CST::Common::blob data)
{
	mLock.lock();

	Chunk content;
	content.mContent = data;
	content.mReceived = false;
	if (mChunks.find(chunk_name) != mChunks.end())
		mChunks.find(chunk_name)->second.mContent.purge();
	mChunks[chunk_name] = content;

	mLock.unlock();
}

void Context::loadAs(const std::string& chunk_name, const std::string& file_name)
{
	mLock.lock();

	if (fileExists(file_name))
	{
		CST::Common::blob _content = fileRead(file_name);
		CST::Common::blob content = escapeCoding(_content);
		_content.purge();
		load(chunk_name, content);
	}

	mLock.unlock();
}

void Context::loadFile(const std::string& file_name)
{
	loadAs(fileShortName(file_name), file_name);
}

void Context::loadFolder(const std::string& folder_name)
{
	std::list<std::string> names = listFiles(folder_name);
	for (std::list<std::string>::iterator i = names.begin(); i != names.end(); i++)
	{
		std::string fullname = fileJoinPaths(folder_name, *i);
		loadAs(*i, fullname);
	}
}

CST::Common::blob Context::save(const std::string& chunk_name)
{
	mLock.lock();

	CST::Common::blob result = mChunks[chunk_name].mContent;

	mLock.unlock();

	return result;
}

void Context::saveFolder(const std::string& folder_name, const bool received)
{
	mLock.lock();
	for (std::map<std::string, Chunk>::iterator i = mChunks.begin(); i != mChunks.end(); i++)
	{
		if (received && !i->second.mReceived)
			continue;
		saveFile(i->first, folder_name);
	}
	mLock.unlock();
}

void Context::saveAs(const std::string& chunk_name, const std::string& file_name)
{
	CST::Common::blob _content = save(chunk_name);
	CST::Common::blob content = escapeDecoding(_content);
	fileWrite(file_name, content);
	content.purge();
}

void Context::saveFile(const std::string& chunk_name, const std::string& folder_name)
{
	saveAs(chunk_name, fileJoinPaths(folder_name, chunk_name));
}

bool Context::exists(const std::string& chunk)
{
	mLock.lock();

	bool result = mChunks.find(chunk) != mChunks.end();

	mLock.unlock();

	return result;
}

CST::Common::blob Context::read(const std::string& chunk)
{
	mLock.lock();

	CST::Common::blob result = mChunks[chunk].mContent;
	mChunks[chunk].mSent = true;

	mLock.unlock();
	
	return result;
}
	
void Context::write(const std::string& chunk, const CST::Common::blob content)
{
	mLock.lock();


	if (mChunks.find(chunk) != mChunks.end())
	{
		mChunks.find(chunk)->second.mContent.purge();	
	};
	mChunks[chunk].mContent = content;
	mChunks[chunk].mReceived = true;

	mLock.unlock();
}
	
void Context::remove(const std::string& chunk)
{
	mLock.lock();

	if (mChunks.find(chunk) != mChunks.end())
	{
		mChunks.find(chunk)->second.mContent.purge();
		mChunks.erase(mChunks.find(chunk));
	}

	mLock.unlock();

}

std::list<std::string> Context::list(const bool received, const bool sent)
{
	std::list<std::string> result;
	mLock.lock();
	for (std::map<std::string, Chunk>::iterator i = mChunks.begin(); i != mChunks.end(); i++)
	{
		bool _received = !received || i->second.mReceived;
		bool _sent = !sent || i->second.mSent;
		if (_received && _sent)
			result.push_back(i->first);
	}
	mLock.unlock();
	return result;
}

void Context::reset()
{
	mLock.lock();
	mChunks.clear();
	mLock.unlock();
}