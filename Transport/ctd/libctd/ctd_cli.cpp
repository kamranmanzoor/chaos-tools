#include "ctd_cli.h"
#ifndef WIN32
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define closesocket close
#else
#include <Windows.h>
typedef int socklen_t;
#endif

using namespace Componentality::CTD;

extern void server_handle(SERVER_HANDLER* handler);
extern unsigned int TCP_BUF_SIZE;
extern unsigned int UDP_BUF_SIZE;

Client::Client(const std::string host, const int port) : mExchange(*this)
{
	mHost = host;
	mPort = port;
#ifdef WIN32
	{
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2, 2), &wsaData);
	}
#endif
}

Client::~Client()
{
	mExchange.onEnd();
}

/*****************************************************************************************************/

TCPClient::TCPClient(const std::string host, const int port) : Client(host, port)
{
}

TCPClient::~TCPClient()
{
}

void TCPClient::run()
{
	hostent* localHost = gethostbyname(mHost.c_str());
	if (!localHost)
		throw mHost;
	char* localIP = inet_ntoa(*(struct in_addr *)*localHost->h_addr_list);

	struct sockaddr_in sa;
	sa.sin_family = PF_INET;
	sa.sin_addr.s_addr = inet_addr(localIP);
	sa.sin_port = htons(mPort);

	int socket_type = SOCK_STREAM;
	int socket_proto = IPPROTO_TCP;
	int sock = socket(PF_INET, socket_type, socket_proto);

	if (sock < 0)
		return;

	/* Now connect to the server */
	if (connect(sock, (struct sockaddr *)&sa, sizeof(sa)) < 0)
		return;

	SERVER_HANDLER *sh = new SERVER_HANDLER;
	sh->mSocket = sock;
	sh->mProcessor = &mExchange;
	sh->mRunnable = this;
	sh->mExitIfNoData = true;

	mExchange.onStart();
	server_handle(sh);
}

/******************************************************************************************************************/

UDPClient::UDPClient(const std::string host, const int port, const int localPort, const int timeout) : Client(host, port), mLocalPort(localPort), mTimeout(timeout)
{
}

UDPClient::UDPClient(const std::string host, const int port, const int timeout) : Client(host, port), mLocalPort(port + 1), mTimeout(timeout)
{
#ifdef WIN32
	{
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2, 2), &wsaData);
	}
#endif
}

UDPClient::~UDPClient()
{
}

void UDPClient::run()
{
	hostent* localHost = gethostbyname(mHost.c_str());
	char* localIP = inet_ntoa(*(struct in_addr *)*localHost->h_addr_list);

	struct sockaddr_in sa;
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr(localIP);
	sa.sin_port = htons(mLocalPort);

	int socket_type = SOCK_DGRAM;
	int socket_proto = IPPROTO_UDP;
	int sock = socket(AF_INET, socket_type, socket_proto);

	if (sock < 0)
		return;

	struct timeval tv;
	tv.tv_sec = mTimeout;
	tv.tv_usec = 0;
	char* buf = new char[UDP_BUF_SIZE];
	struct sockaddr from;

	mExchange.onStart();
	int read = 1;
	do
	{
		((sockaddr_in*)&from)->sin_family = AF_INET;
		((sockaddr_in*)&from)->sin_addr.s_addr = inet_addr(localIP);
		((sockaddr_in*)&from)->sin_port = htons(mPort);
		socklen_t from_len = sizeof(from);
		socklen_t *address_len = &from_len;
		mExchange.onConnect();
		if ((read <= 0) && !mExchange.isThereWriteCommand())
		{
			mExchange.onDisconnect();
			exit();
			break;
		};
		CST::Common::blob dsend = mExchange.getDataToWrite(0);
		if ((dsend.mSize <= UDP_BUF_SIZE) && (dsend.mSize > 0))
		{
			int sent = sendto(sock, dsend.mData, dsend.mSize, 0, &from, *address_len);
		}
		setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
		read = recvfrom(sock, buf, UDP_BUF_SIZE, 0, (sockaddr*)&from, address_len);
		if (read > 0)
		{
			CST::Common::blob dread; dread.mData = buf; dread.mSize = read;
			mExchange.onReceived(dread);
			mExchange();
			CST::Common::blob dsend = mExchange.getDataToWrite(0);
			if ((dsend.mSize <= UDP_BUF_SIZE) && (dsend.mSize > 0))
			{
				int sent = sendto(sock, dsend.mData, dsend.mSize, 0, &from, *address_len);
			}
		}
	} while (!getExit());
	mExchange.onEnd();
	delete[] buf;
}