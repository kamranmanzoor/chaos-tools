#include "cache_chunk.h"
#include <fstream>
#include <memory.h>

using namespace Componentality::CTD;
using namespace CST::Common;

const std::string SIGNATURE = "$-CNK-$";

static void pack_chunk(const CST::Common::blob data, const std::string* sname, const std::string next, CST::Common::blob& res, std::string& current)
{
	DIGEST sha;
	if (data.mSize)
		sha = SHA_Converter().operator()(data);
	else
		memset(&sha, 0, sizeof(sha));
	std::string sha_str = SHA_serialize(sha);
	CST::Common::blob result;
	result.mSize = data.mSize + SIGNATURE.size() + next.size() + (sname ? sname->size() : 0) + 1;
	result.mData = new char[result.mSize];
	char* ptr = result.mData;
	memcpy(ptr, SIGNATURE.c_str(), SIGNATURE.size());
	ptr += SIGNATURE.size();
	memcpy(ptr, next.c_str(), next.size());
	ptr += next.size();
	if (sname)
	{
		memcpy(ptr, sname->c_str(), sname->size());
		ptr += sname->size();
	}
	*ptr++ = 0;
	memcpy(ptr, data.mData, data.mSize);
	ptr += data.mSize;
	size_t check = ptr - result.mData;
	res = result;
	current = sha_str;
}

std::pair<CST::Common::blob, std::string> unpack_chunk(const std::string& chunk, std::string& fname, bool read)
{
	std::pair<CST::Common::blob, std::string> result;

	CST::Common::blob raw = fileRead(chunk);
	char* ptr = raw.mData;
	if (raw.mSize < (int)SIGNATURE.size())
	{
		raw.purge();
		return result;
	}
	if (memcmp(ptr, SIGNATURE.c_str(), SIGNATURE.size()))
	{
		raw.purge();
		return result;
	}
	ptr += SIGNATURE.size();
	if (raw.mSize - (ptr - raw.mData) < sizeof(DIGEST))
	{
		raw.purge();
		return result;
	}
	char* zeroptr = (char*)memchr(raw.mData, 0, raw.mSize);
	if (NULL == zeroptr)
	{
		raw.purge();
		return result;
	}
	std::string sha_str;
	for (size_t i = 0; i < sizeof(DIGEST); i++)
		sha_str += *(ptr++);
	std::string name;
	while (*ptr)
		name += *ptr++;
	if (read)
	{
		ptr += 1;
		CST::Common::blob content;
		content.mSize = raw.mSize - (ptr - raw.mData);
		content.mData = new char[content.mSize];
		memcpy(content.mData, ptr, content.mSize);
		raw.purge();
		DIGEST sha1 = SHA_Converter().operator()(content);
		CST::Common::blob xdec = x_decode(fileShortName(chunk));
		std::string sha_x;
		for (size_t i = 0; i < xdec.mSize; i++) sha_x += xdec.mData[i];
		DIGEST sha = SHA_deserialize(sha_x);
		xdec.purge();
		if (memcmp(&sha, &sha1, sizeof(DIGEST)))
		{
			content.purge();
			return result;
		}
		result.first = content;
	}
	result.second = sha_str;
	if (!name.empty())
		fname = name;
	return result;
}

std::pair<std::list<std::string>, std::string> Componentality::CTD::Chunkenizer::split(
	const std::string& file,
	const std::string& target_dir,
	const size_t chunk_size)
{
	std::string sname = fileShortName(file);
	size_t extraspace = SIGNATURE.size() + sizeof(DIGEST) + 1;
	size_t size = chunk_size - extraspace;

	std::pair<std::list<std::string>, std::string> result;
	std::ifstream ifile;
	ifile.open(file.c_str(), std::ios::binary);
	if (!ifile.good() || ifile.bad())
		return result;
	CST::Common::blob data; data.mSize = size; data.mData = new char[data.mSize];
	std::string next;
	for (size_t i = 0; i < sizeof(DIGEST); i++) next += char(0);
	ifile.seekg(0, std::ios::end);
	unsigned long long fsize = ifile.tellg();
	ifile.seekg(0);
	unsigned long long nchunks = (fsize + size - 1) / size;
	for (long long i = (long long)(fsize - size); ; i -= size)
	{
		if (i < 0)
			size -= (size_t)(-i), i = 0;
		ifile.seekg(i);
		ifile.read(data.mData, size);
		size_t read = (size_t) ifile.gcount();
		CST::Common::blob chunk; chunk.mData = data.mData; chunk.mSize = read;
		CST::Common::blob packed_data;
		std::string packed_name;
		pack_chunk(chunk, i == 0 ? &sname : NULL, next, packed_data, packed_name);
		if (packed_name.empty())
		{
			result.first.clear();
			result.second.clear();
			data.purge();
			ifile.close();
			return result;
		}
		CST::Common::blob n; n.mData = (char*)packed_name.c_str(); n.mSize = packed_name.size();
		next = packed_name;
		packed_name = x_encode(n);
		fileWrite(fileJoinPaths(target_dir, packed_name), packed_data);
		result.first.push_front(packed_name);
		packed_data.purge();
		if (i == 0)
		{
			result.second = packed_name;
			break;
		}
		if (i - size <= 0)
		{
			extraspace += sname.size();
			size = chunk_size - extraspace;
		}
	}
	data.purge();
	ifile.close();
	return result;
};

std::pair<bool, std::pair<std::string, std::string> > Componentality::CTD::Chunkenizer::merge(
	const std::string& first_chunk, 
	const std::string& source_dir,
	std::string& target_dir)
{
	std::pair<bool, std::pair<std::string, std::string> > result;
	std::list<std::string> chunks;
	std::string chunk = first_chunk;
	result.second.second = fileShortName(chunk);
	std::string name;
	std::string zero;
	for (size_t i = 0; i < sizeof(DIGEST); i++) zero += char(0);
	do
	{
		if (!fileExists(chunk))
		{
			result.first = false;
			return result;
		}
		else
		{
			chunks.push_back(chunk);
		}
		std::pair<CST::Common::blob, std::string> unpacked = unpack_chunk(chunk, name, false);
		if (!name.empty())
			result.second.first = name;
		if (unpacked.second == zero)
		{
			result.first = true;
			break;
		}
		if (!unpacked.second.empty())
		{
			CST::Common::blob fromx; fromx.mData = (char*) unpacked.second.c_str(); fromx.mSize = unpacked.second.size();
			std::string from = x_encode(fromx);
			chunk = fileJoinPaths(source_dir, from);
			result.second.second = from;
		}
	} while (true);
	if (name.empty())
		result.first = false;
	if (!result.first)
		return result;
	std::ofstream ofile;
	ofile.open(fileJoinPaths(target_dir, result.second.first).c_str(), std::ios::binary);
	if (ofile.bad() || !ofile.good())
	{
		result.first = false;
		return result;
	};
	for (std::list<std::string>::iterator i = chunks.begin(); i != chunks.end(); i++)
	{
		std::pair<CST::Common::blob, std::string> unpacked = unpack_chunk(*i, name, true);
		ofile.write(unpacked.first.mData, unpacked.first.mSize);
		unpacked.first.purge();
	}
	ofile.close();
	return result;
};