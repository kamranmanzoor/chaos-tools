#include "ctd_api.h"
#include "../libcache/cache_chunk.h"
#include <iostream>
#include <vector>
#include <stdlib.h>

#ifdef WIN32
#define sscanf sscanf_s
#endif

std::string USAGE_LINE = "Usage:\n"
						"\t ctdd server (TCP|UDP) <port> <commands>\n"
						"\t ctdd client (TCP|UDP) <host> <port> <commands>\n"
						"\t ctdd split <file> <directory>\n"
						"\t ctdd merge <file> <src_directory> <target_directory>\n"
						"\n"
						"Commands:\n"
						"\t --check <read-folder> <write-folder>\n"
						"\t --put <read-folder> <write-folder>\n"
						"\t --get {<file>} <folder>\n"
						"\t --offer {<file>} <folder>\n"
						"\t --send {<file>} <folder>\n"
						"\t --exc <read-folder> <write-folder>\n"
						"\n";

Componentality::CTD::Server* svr = NULL;
Componentality::CTD::Client* cli = NULL;
int port = 0;
Componentality::CTD::PROTOCOL proto;

std::pair<std::list<std::string>, std::string> getFilesAndDir(std::vector<std::string>& args, int& i)
{
	std::list<std::string> prev;
	std::string last;

	for (i += 1; i < (int)args.size(); i++)
	{
		if (args[i].substr(0, 2) == "--")
		{
			i--;
			break;
		}
		else
		{
			if (!last.empty())
				prev.push_back(last);
			last = args[i];
		}
	}

	return std::pair<std::list<std::string>, std::string>(prev, last);
}

std::list<Componentality::CTD::Agent*> queueCommands(std::vector<std::string>& args, int startidx)
{
	std::list<Componentality::CTD::Agent*> result;
	for (int i = startidx; i < (int) args.size(); i++)
	{
		if ((i < (int)args.size()) && (args[i] == "--check"))
		{
			if (i < (int) args.size() - 2)
			{
				if (svr)
					result.push_back(Componentality::CTD::queueToCheck(svr, args[i + 1], args[i + 2], proto));
				if (cli)
					result.push_back(Componentality::CTD::queueToCheck(cli, args[i + 1], args[i + 2], proto));
				i += 2;
			}
			else
				std::cout << "Warning: --check needs two parameters" << std::endl;
		}
		if ((i < (int)args.size()) && (args[i] == "--put"))
		{
			if (i < (int) args.size() - 2)
			{
				if (svr)
					result.push_back(Componentality::CTD::queueToPut(svr, args[i + 1], args[i + 2], proto));
				if (cli)
					result.push_back(Componentality::CTD::queueToPut(cli, args[i + 1], args[i + 2], proto));
				i += 2;
			}
			else
				std::cout << "Warning: --put needs two parameters" << std::endl;
		}
		if ((i < (int)args.size()) && (args[i] == "--get"))
		{
			std::pair<std::list<std::string>, std::string> fnd = getFilesAndDir(args, i);
			if (svr)
				result.push_back(Componentality::CTD::queueToGet(svr, fnd.first, fnd.second, proto));
			if (cli)
				result.push_back(Componentality::CTD::queueToGet(cli, fnd.first, fnd.second, proto));
		}
		if ((i < (int)args.size()) && (args[i] == "--offer"))
		{
			std::pair<std::list<std::string>, std::string> fnd = getFilesAndDir(args, i);
			if (svr)
				result.push_back(Componentality::CTD::queueToCheck(svr, fnd.first, fnd.second, proto));
			if (cli)
				result.push_back(Componentality::CTD::queueToCheck(cli, fnd.first, fnd.second, proto));
		}
		if ((i < (int)args.size()) && (args[i] == "--send"))
		{
			std::pair<std::list<std::string>, std::string> fnd = getFilesAndDir(args, i);
			if (svr)
				result.push_back(Componentality::CTD::queueToPut(svr, fnd.first, fnd.second, proto));
			if (cli)
				result.push_back(Componentality::CTD::queueToPut(cli, fnd.first, fnd.second, proto));
		}
		if ((i < (int)args.size()) && (args[i] == "--exc"))
		{
			if (i < (int)args.size() - 2)
			{
				if (svr)
					result.push_back(Componentality::CTD::setupExchange(svr, args[i + 1], args[i + 2]));
				if (cli)
					result.push_back(Componentality::CTD::setupExchange(cli, args[i + 1], args[i + 2]));
				i += 2;
			}
			else
				std::cout << "Warning: --exc needs two parameters" << std::endl;
		}
	}
	return result;
}

int main(int argc, char** argv)
{
	std::vector<std::string> args;
	for (int i = 1; i < argc; i++)
	{
		if (!std::string(argv[i]).empty())
			args.push_back(argv[i]);
	}
	if (args.size() < 3)
	{
		std::cout << USAGE_LINE << std::endl;
		return 1;
	}

	if (args[0] == "split")
	{
		std::string res = Componentality::CTD::Chunkenizer().split(args[1], args[2]).second;
		if (res.empty())
		{
			std::cout << "ERROR" << std::endl;
			return 2;
		}
		else
		{
			std::cout << res << std::endl;
			return 0;
		}
	}

	if (args[0] == "merge")
	{
		std::pair<bool, std::pair<std::string, std::string> > res = 
			Componentality::CTD::Chunkenizer().merge(args[1], args[2], args[3]);
		if (!res.first)
		{
			std::cout << "ERROR" << std::endl;
			return 2;
		}
		else
		{
			std::cout << res.second.first << std::endl;
			return 0;
		}
	}

	if (args[1] == "TCP")
	{
		proto = Componentality::CTD::TCP;

	}
	else if (args[1] == "UDP")
	{
		proto = Componentality::CTD::UDP;
	}
	else
	{
		std::cout << "Please specify TCP or UDP as a protocol";
		return 1;
	}

	if (args[0] == "server")
	{
		sscanf(args[2].c_str(), "%d", &port);
		if (port == 0)
		{
			std::cout << "Wrong port number." << std::endl;
			return 1;
		}
		svr = Componentality::CTD::createServer(proto, port);
	}
	else if (args[0] == "client")
	{
		if (args.size() < 4)
		{
			std::cout << USAGE_LINE << std::endl;
			return 1;
		}
		sscanf(args[3].c_str(), "%d", &port);
		cli = Componentality::CTD::createClient(proto, args[2], port);
	}
	else
	{
		std::cout << "Please specify client or server mode";
		return 1;
	}

	if (svr)
	{
		std::list<Componentality::CTD::Agent*> ags = queueCommands(args, 3);
		CST::Common::thread* ths = Componentality::CTD::runServer(svr);
		while (true)
			CST::Common::sleep(1000);
	}
	else if (cli)
	{
		std::list<Componentality::CTD::Agent*> ags = queueCommands(args, 4);
		CST::Common::thread* thc = Componentality::CTD::runClient(cli);
	}

	return 0;
}