#-------------------------------------------------------------------------------
# Name:        DrivityProxy
# Purpose:     Unpack received records and transfer data to the backend
#              via Web-based API
#              Third Party Neutral Edition
#
# Author:      Konstantin Khait
#
# Created:     18/11/2013
# Copyright:   (c) Componentality Oy
# Licence:     GPL
#-------------------------------------------------------------------------------

import argparse
import os
import subprocess
import tempfile
import json
import urllib
import sys
import hashlib
import time

print("Chaos Tools Library")
print("Drivity Proxy: secure location proxy server")
print("Copyright (C) Componentality Oy, 2013")

SERVER = 'http://api.flexroad.ru/senddata.py'

if os.path.exists('/usr/local/bin/drivity'):
    DRIVITY = '/usr/local/bin/drivity'
else:
    if os.path.exists('/usr/bin/drivity'):
        DRIVITY = '/usr/bin/drivity'
    else:
        if os.path.exists('/usr/sbin/drivity'):
            DRIVITY = '/usr/sbin/drivity'
        else:
            if os.path.exists('c:\Drivity\drivity.exe'):
                DRIVITY = 'c:\Drivity\drivity.exe'
            else:
                print('drivity not found')
                quit()

python = sys.version[0]

if python == '3':
    from urllib.parse import urlencode
    from urllib.request import urlopen

# Do parsing of parameters
parser = argparse.ArgumentParser()
parser.add_argument('--folder')
parser.add_argument('--backend')
args = parser.parse_args()
folder = args.folder
backend = args.backend

if not (os.path.exists(folder)):
    print("Folder " + folder + " not found")
    quit()

if backend:
    SERVER = backend

def createFileList():
    filelist = {}
    i = 0
    for subdir, dirs, files in os.walk(folder):
        for file in files:
            filelist[i] = file
            i += 1
    return filelist

def unpackFile(file):
    cmd = DRIVITY + " decrypt " + os.path.join(folder, file) + " " + temp
    print("Decrypting " + cmd)
    subprocess.call(cmd, shell = True)

def processRequest(vid, jobj):
    try:
        # Send JSON to componentality server
        data_to_send = json.dumps(jobj).encode()
        # If avl == 1 it means that the valid location record is here
        # if avl == 0 it means that invalid location record is here
        # if avl == "" it means there is no such item
        data_to_send = json.dumps(jobj).encode()
        dblock = {'data':data_to_send, 'vid':vehicle_id, 'checksumm':hashlib.md5(data_to_send).hexdigest()}
        if ('avl' in jobj):
            if str(jobj['avl']) == '1':
                if 'lat' in jobj:
                    dblock['lat'] = jobj['lat']
                if 'lon' in jobj:
                    dblock['lon'] = jobj['lon']
                if 'alt' in jobj:
                    dblock['alt'] = jobj['alt']
                if 'alt' in jobj:
                    dblock['alt'] = jobj['alt']
        if 'tms' in jobj:
            dblock['tms'] = jobj['tms']

        if int(python) < 3:
            data = urllib.urlencode(dblock)
            r = urllib.urlopen(SERVER, data)
        else:
            data = urlencode(dblock)
            r = urlopen(SERVER, data.encode())

        print("Sending data: " + data)
        print("Received " + r.read().decode())
    except:
        print("ERROR")

# Extract manifest from bytearray representation of drivity record
def extractManifest(content):
    prev_n = False
    index = 0
    manifest = ""
    while index < len(content):
        if (python == '3'):
            contid = (content[index] == 10)
        else:
            contid = (content[index] == '\n')
        if contid == True:
            if prev_n == False:
                prev_n = True
            else:
                return manifest
        else:
            if prev_n == True:
                manifest += '\n'
                prev_n = False
            try:
                manifest += chr(content[index])
            except:
                manifest += content[index]
        index += 1
    return ""

# Get value for the given key in manifest
def findInManifest(key, manifest):
    manifest = manifest.split("\n")
    for kvp in manifest:
        kvp = kvp.split("=")
        if len(kvp) > 1:
            if kvp[0] == key:
                return kvp[1]
    return ""

while True:
    filelist = createFileList()
    tfile, temp = tempfile.mkstemp()

    try:
        for i in filelist:
            unpackFile(filelist[i])
            # Extract vehicle ID from the manifest
            file = open(os.path.join(folder, filelist[i]), 'rb')
            content = file.read()
            # Get manifest from the record
            manifest = extractManifest(content)
            # Extract vehicle ID from manifest
            vehicle_id = findInManifest("vid", manifest)
            file.close()
            # Decode JSON
            file = open(temp, 'rb')
            content = file.read().decode()
            file.close()
            try:
                # Load JSON content from decoded file
                # No non-JSON data allowed
                jobj = json.loads(content)
                # Send data to the back end
                processRequest(vehicle_id, jobj)
            finally:
                # Remove data chunk when processed
                os.remove(os.path.join(folder, filelist[i]))

            # Close and remove temporary files
        os.close(tfile)
        os.remove(temp)
        time.sleep(10)
    except:
        # Close and remove temporary files
        os.close(tfile)
        os.remove(temp)
        time.sleep(10)
        print("SOME ERROR IN MAIN LOOP")