#!/usr/bin/python
#-------------------------------------------------------------------------------
# Name:        EasySend
# Purpose:     Receive easy JSON records and transfer data to the backend
#              via Web-based API
#              Third Party Neutral Edition
#
# Author:      Konstantin Khait
#
# Created:     25/11/2013
# Copyright:   (c) Componentality Oy
# Licence:     GPL
#-------------------------------------------------------------------------------

import argparse
import os
import json
import socket
import sys
import time

python = sys.version[0]

# Do parsing of parameters
parser = argparse.ArgumentParser()
parser.add_argument('--port')
parser.add_argument('--ip')
parser.add_argument('--file')
parser.add_argument('--update')
args = parser.parse_args()
port = args.port
ip = args.ip
file = args.file
update_mode = args.update

TCP_PORT = port
if not TCP_PORT:
    TCP_PORT = "1979"

HOST, PORT = ip, int(TCP_PORT)

print("EasySend connects to IP " + ip + " port " + str(TCP_PORT))
# Create socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.settimeout(10)
sock.connect((HOST, PORT))

file = open(file, "r")
content = file.read()
file.close();

try:
    jobj = json.loads(content)
    if update_mode:
        jobj['tms'] = str(time.time())
    content = json.dumps(jobj)
except:
    print("Parsing error: not a JSON object")

sock.sendall(content.encode())
readdata = sock.recv(1024).decode("utf-8")

sock.close();

print("Response: " + readdata)