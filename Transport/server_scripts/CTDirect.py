#!/usr/bin/python
#-------------------------------------------------------------------------------
# Name:        CTDirect
# Purpose:     TCP based data transport for Chaos
#
# Author:      Konstantin Khait
#
# Created:     16/11/2013
# Copyright:   (c) Componentality Oy 2013
# Licence:     GPL
#-------------------------------------------------------------------------------

import argparse
import os
import subprocess
import time
import threading
import socket
import base64
import sys

python = sys.version[0]

if python == '3':
    import socketserver
else:
    import SocketServer as socketserver

print("Chaos Tools Library")
print("Direct TCP Synchronization Tool")
print("Copyright (C) Componentality Oy, 2013")

# Do parsing of parameters
parser = argparse.ArgumentParser()
parser.add_argument('--port')
parser.add_argument('--input')
parser.add_argument('--output')
parser.add_argument('--mode')
parser.add_argument('--ip')
parser.add_argument('--kill')
parser.add_argument('--send')
parser.add_argument('--accept')
args = parser.parse_args()
port = args.port
infolder = args.input
outfolder = args.output
mode = args.mode
ip = args.ip
kill = args.kill
sendfilter = args.send
receivefilter = args.accept

# Check parameters validity
if (not infolder) or (not outfolder) or (not mode):
    print("Usage: CTDirect --mode=(client [--ip=<server-ip>] | server) \n\t--input=<input-folder> \n\t--output=<output-folder>")
    print("\t[--port=<TCP port>] \n\t[--kill=yes] \n\t[--send=<sending-filter>] \n\t[--accept=<receive-filter>]")
    quit()

# Setup IP address (used for server only) and port
TCP_IP = '0.0.0.0'
if port:
    TCP_PORT = int(port)
else:
    TCP_PORT = 1978

def checkFilter(name, filt):
    return str.find(name, filt) != -1

# Function sends data chunk by chunk to avoid overflow
def send(sock, byteline):
    data = byteline.decode("utf-8")
    while len(data):
        if len(data) > 1024:
            chunk = data[0:1024]
            data = data[1024:]
        else:
            chunk = data;
            data = ""
        sock.sendall(chunk.encode())

# Command represents single command
# Formats (no space characters!):
#   Check: c <file>
#   Get:   g <file>
#   Put:   p <file> \x00 <content>
#   Quit:  q
class Command:
    def create(this, cmdstr):
        this.cmd = None
        this.fname = ""
        this.content = None
        if len(cmdstr) > 0:
            this.cmd = cmdstr[0]
            # Check if argument splitter is present
            pos = cmdstr.find("\x00")
            if pos != -1:
                this.fname = cmdstr[1: pos]
                this.content = cmdstr[pos + 1:]
            else:
                this.fname = cmdstr[1:]
                this.content = None
    def onCheck(this):
        global receivefilter
        # Check if the name not passing filter
        # in this case we say it is already here (we don't need it)
        if receivefilter:
            if not checkFilter(this.fname, receivefilter):
                return "yes"
        # Check file presence
        fullname = os.path.join(infolder, this.fname)
        if os.path.exists(fullname):
            return "yes"
        else:
            return "no"
    def onPut(this):
        fullname = os.path.join(infolder, this.fname)
        file = open(fullname, "wb")
        if this.content:
            file.write(base64.b64decode(this.content)) # Base64 is used to be able to proceed with \x00 in file content
        file.close();
        return "ok"
    def onGet(this):
        fullname = os.path.join(outfolder, this.fname)
        file = open(fullname, "rb")
        content = file.read()
        content = base64.b64encode(content)
        file.close();
        return content.decode("utf-8")
    def onQuit(this):
        return "quit"
    def execute(this):
        if this.cmd == 'c':
            return this.onCheck();
        if this.cmd == 'p':
            return this.onPut();
        if this.cmd == 'g':
            return this.onGet();
        if this.cmd == 'q':
            return this.onQuit();
    def getCmd(this):
        return this.cmd
    def getFName(this):
        return this.fname;
    def getContent(this):
        return this.content

# Command parser accumulates content and proceed with commands one by one
class CommandParse:
    def __init__(this):
        this.data = None
        this.action = 'none'
        this.response = None
    def getAction(this):
        return this.action
    def getResponse(this):
        return this.response
    # This method is called upon next data block received
    # Just adds it to the buffer
    def received(this, datablock):
        if not this.data:
            this.data = datablock.decode("ascii")
        else:
            this.data += datablock.decode("ascii")
    # Command processing
    def process(this):
        global kill
        global outfolder
        # Action is a _result_ of command execution, i.e. it is an output for the command
        this.action = 'none'
        # Response is an answer to be returned to sender
        this.response = None
        if (not this.data):
            return False;
        # Search for command end marker
        dpair = this.data.find("\x00\x00")
        if (dpair >= 0):
            # Packet is a command (or candidate)
            packet = this.data[0: dpair]
            this.data = this.data[dpair + 2:]
            command = Command()
            # Create and execute new command
            command.create(packet)
            result = command.execute()
            # Get to be unconditionally responded with put. Command parser returns file content
            # to be returned to sender
            if command.getCmd() == 'g':
                if command.getFName():
                    this.action = 'reply'
                    this.response = "p" + command.getFName() + '\x00' + result + "\x00"
                    if kill == 'yes':
                        fullname = os.path.join(outfolder, command.getFName())
                        if os.path.exists(fullname):
                            os.remove(fullname)
                else:
                    this.action = 'error'
            # Check must be only responded with Get if no such item available yet
            # Otherwise it is responded with neutral answer "+" just to avoid getting stuck
            # in recv()
            if command.getCmd() == 'c':
                if command.getFName():
                    if result == 'no':
                        this.action = 'reply'
                        this.response = "g" + command.getFName() + "\x00";
                    else:
                        this.action = 'reply'
                        this.response = '+\x00'
            # Put is always responded with "+".
            if command.getCmd() == 'p':
                this.action = 'reply'
                this.response = '+\x00'
            # Quit is only affecting server. It means that client has nothing else
            # to request, therefore server should send its data and end link when
            # everything is done
            if command.getCmd() == 'q':
                this.action = 'quit'
            return True;
        return False

# Request forms the list of Check commands for an output folder
def request(outfolder):
    global sendfilter
    # Neutral command is used to avoid empty data line (used as keep alive)
    data = "+\x00\x00"
    for subdir, dirs, files in os.walk(outfolder):
        for file in files:
            basename = os.path.basename(file)
            # If send filter defined, check if file name matches it
            if sendfilter:
                if not checkFilter(basename, sendfilter):
                    continue
            data += 'c' + basename + "\x00\x00"
    return data

# Create command respond block (bytes[])
def respond(cmd):
    global outfolder
    global mode
    # For reply it is easily constructing response
    if cmd.getAction() == 'reply':
        if cmd.getResponse():
            data = (cmd.getResponse() + "\x00").encode()
            return data
    # For quit it builds server's data set
    if (cmd.getAction() == 'quit') and (mode == 'server'):
        data = request(outfolder)
        return data.encode()
    return "+\x00\x00".encode();

# Handler for the server socket
class TCPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        cmd = CommandParse()
        while True:
            try:
                self.data = self.request.recv(1024).strip()
            except:
                break
            cmd.received(self.data)
            cont = True
            quitflag = False
            # Proceed with existing commands queue
            while cont:
                cont = cmd.process();
                # If quit received we should stop processing this link
                # but finish with existing command queue first
                # Existing command queue may provoke new commands from
                # a client
                if cmd.getAction == 'quit':
                    quitflag = True
                self.data = respond(cmd)
                if self.data:
                    try:
                        send(self.request, self.data)
                    except:
                        self.request.close()
                        break
            if quitflag:
                self.request.close();

if __name__ == "__main__":

    # Make directories if not yet there
    if not os.path.exists(infolder):
        os.makedirs(infolder)
    if not os.path.exists(outfolder):
        os.makedirs(outfolder)

    print("Input folder: " + infolder)
    print("Output folder: " + outfolder)

    if mode == 'server':
        print("CTDirect is running as a server on port " + str(TCP_PORT))
        HOST, PORT = TCP_IP, int(TCP_PORT)
        # Simply open socket connection for multithread mode
        server = socketserver.ThreadingTCPServer((HOST, PORT), TCPHandler)
        server.serve_forever()

    if mode == 'client':
        if not ip:
            print("Server IP address not defined")
            quit()
        HOST, PORT = ip, int(TCP_PORT)

        print("CTDirect is running as a client connecting to IP " + ip + " port " + str(TCP_PORT))
        # Create socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(10)
        sock.connect((HOST, PORT))
        try:
            # Make a request
            data = request(outfolder)
            data += "q\x00\x00"
            send(sock, data.encode())
            cmd = CommandParse()
            while True:
                # Get and proceed with feedback
                received = sock.recv(1024)
                cmd.received(received)
                cont = True
                while cont:
                    cont = cmd.process();
                    # In contrast to server, client doesn't answer neutral '+' to
                    # avoid infinite dialogue
                    if cmd.getAction() != 'none':
                        data = respond(cmd)
                        if not data:
                            quit()
                        else:
                            send(sock, data)
        except:
            sock.close()

