/****************************************************************************************************/
/* RSA Cryptography Components 2008                   * RSACrypt.h                                  */
/****************************************************************************************************/
/* Copyright (C) Konstantin A. Khait, 2008                                                          */
/****************************************************************************************************/
/* RSA cryptography adapter to CST middleware style                                                 */
/****************************************************************************************************/
#include "RSACrypt.h"
#include "time.h"
#include <stdlib.h>

#define min(x, y) ((x < y) ? (x) : (y))

#define GUARD 16

/* Use RSA module */
extern "C"
{
	#include "rsa.h"
};
#include <list>
/******************/

static int KEY_LENGTH = 2048;
static int PT_LEN     = 48;
static int INTSIZE    = 4;

void long_store(char* data, long val)
{
	data[0] = (val >> 0) & 0xFF;
	data[1] = (val >> 8) & 0xFF;
	data[2] = (val >> 16) & 0xFF;
	data[3] = (val >> 24) & 0xFF;
};

void long_restore(long& val, char* data)
{
	val = 0;
	val |= data[0] << 0;
	val |= data[1] << 8;
	val |= data[2] << 16;
	val |= data[3] << 24;
};

//////////////////////////////////////////////////////////////////////////////////////
using namespace Crypto::RSA;

int f_rng(void *)
{
	time_t timer;
	time(&timer);
	srand((int)timer);
	return rand();
};

inline rsa_context __make_ctx_pkey(const std::string str)
{
	std::string N;
	std::string E;
	size_t pos = str.find((char)0xFF);
	if (pos == (size_t) -1)
		throw std::string("RSA: Invalid public key");
	N = str.substr(0, pos);
	E = str.substr(pos + 1);

	if (E[E.size() - 1] == (char) 0xFF)
		E = E.substr(0, E.size() - 1);

	rsa_context ctx;
	memset(&ctx, 0, sizeof(ctx));
	
	mpi_read_string(&ctx.N, 16, N.c_str());
	mpi_read_string(&ctx.E, 16, E.c_str());

	return ctx;
}

inline rsa_context __make_ctx_skey(const std::string str)
{
	std::string rest = str;
	std::string E;
	std::string D;
	std::string P;
	std::string Q;
	std::string DP;
	std::string DQ;
	std::string QP;

	if (rest[rest.size() - 1] == (char) 0xFF)
		rest = rest.substr(0, rest.size() - 1);
	size_t pos = rest.find((char)0xFF);
	if (pos == (size_t) -1)
		throw std::string("RSA: Invalid secure key");
	E = rest.substr(0, pos);
	rest = rest.substr(pos + 1);
	pos = rest.find((char)0xFF);
	if (pos == (size_t) -1)
		throw std::string("RSA: Invalid secure key");
	D = rest.substr(0, pos);
	rest = rest.substr(pos + 1);
	pos = rest.find((char)0xFF);
	if (pos == (size_t) -1)
		throw std::string("RSA: Invalid secure key");
	P = rest.substr(0, pos);
	rest = rest.substr(pos + 1);
	pos = rest.find((char)0xFF);
	if (pos == (size_t) -1)
		throw std::string("RSA: Invalid secure key");
	Q = rest.substr(0, pos);
	rest = rest.substr(pos + 1);
	pos = rest.find((char)0xFF);
	if (pos == (size_t) -1)
		throw std::string("RSA: Invalid secure key");
	DP = rest.substr(0, pos);
	rest = rest.substr(pos + 1);
	pos = rest.find((char)0xFF);
	if (pos == (size_t) -1)
		throw std::string("RSA: Invalid secure key");
	DQ = rest.substr(0, pos);
	QP = rest;

	rsa_context ctx;
	memset(&ctx, 0, sizeof(ctx));

	mpi_read_string(&ctx.E, 16, E.c_str());
	mpi_read_string(&ctx.D, 16, D.c_str());
	mpi_read_string(&ctx.P, 16, P.c_str());
	mpi_read_string(&ctx.Q, 16, Q.c_str());
	mpi_read_string(&ctx.DP, 16, DP.c_str());
	mpi_read_string(&ctx.DQ, 16, DQ.c_str());
	mpi_read_string(&ctx.QP, 16, QP.c_str());

	ctx.len = KEY_LENGTH / 8;

	return ctx;
};

KeyPair KeyGen::operator()(const SIGNAL_TYPE)
{
	rsa_context ctx;
	rsa_init(&ctx, RSA_PKCS_V15, 0, &f_rng, NULL);
	rsa_gen_key(&ctx, KEY_LENGTH, 65537);

	std::string pkey;
	std::string skey;

	char templine[8192];
	int  templen = 8192;

	// Make public key and first part of private key
	mpi_write_string(&ctx.N, 16, templine, &templen);
	pkey += templine;
	pkey += (char) 0xFF;
	templen = 8192;
	mpi_write_string(&ctx.E, 16, templine, &templen);
	pkey += templine;
	pkey += (char) 0xFF;
	skey += templine;
	skey += (char) 0xFF;
	// Make remaining parts of secure key
	templen = 8192;
	mpi_write_string(&ctx.D, 16, templine, &templen);
	skey += templine;
	skey += (char) 0xFF;
	templen = 8192;
	mpi_write_string(&ctx.P, 16, templine, &templen);
	skey += templine;
	skey += (char) 0xFF;
	templen = 8192;
	mpi_write_string(&ctx.Q, 16, templine, &templen);
	skey += templine;
	skey += (char) 0xFF;
	templen = 8192;
	mpi_write_string(&ctx.DP, 16, templine, &templen);
	skey += templine;
	skey += (char) 0xFF;
	templen = 8192;
	mpi_write_string(&ctx.DQ, 16, templine, &templen);
	skey += templine;
	skey += (char) 0xFF;
	templen = 8192;
	mpi_write_string(&ctx.QP, 16, templine, &templen);
	skey += templine;
	skey += (char) 0xFF;

	StandardComponents::StringToBlock stb;
	Cell<DataBlock> key_holder;
	stb.GetOutput().Link(key_holder.GetDataInput());

	KeyPair kp;
	stb.GetInput().operator ()(pkey);
	kp.y = key_holder.GetValue();
	stb.GetInput().operator ()(skey);
	kp.x = key_holder.GetValue();

	if (0 != rsa_check_pubkey(&ctx))
		throw std::string("RSA: Invalid public key");

	return kp;
};

void ShortBlockEncrypt::Run(const DataBlock pkey)
{
	this->public_key = pkey;
};

DataBlock ShortBlockEncrypt::operator()(const DataBlock _source)
{
	if (NULL == this->public_key.data)
		return DataBlock(0);

	if (_source.length > PT_LEN)
		throw std::string("RSA: Invalid public key");

	DataBlock source(PT_LEN);
	if (_source.length == PT_LEN)
		memcpy(source.data, _source.data, PT_LEN);
	else
		memcpy(source.data, _source.data, min(_source.length, source.length));

	// Parse public key
	Cell<std::string> str_holder;
	StandardComponents::BlockToString bts;
	bts.GetOutput().Link(str_holder.GetDataInput());
	bts.GetInput().operator ()(this->public_key);

	rsa_context ctx = __make_ctx_pkey(str_holder.GetValue());

	if (0 != rsa_check_pubkey(&ctx))
		return DataBlock(0);

	DataBlock target(INTSIZE + KEY_LENGTH / 8 + GUARD);

	ctx.len = KEY_LENGTH / 8;
	int res = rsa_pkcs1_encrypt(&ctx, RSA_PUBLIC, 
		(int) source.length, (unsigned char*) source.data, 
		((unsigned char*) target.data) + INTSIZE);

	StandardComponents::BlockUtilizer().GetInput().operator ()(source);

	long_store((char*)target.data, _source.length);

	return target;
};

void ShortBlockDecrypt::Run(const DataBlock skey)
{
	this->secure_key = skey;
};

DataBlock ShortBlockDecrypt::operator()(const DataBlock _source)
{
	if (NULL == this->secure_key.data)
		return DataBlock(0);

	DataBlock source(_source.length);
	memcpy(source.data, ((unsigned char*) _source.data) + INTSIZE, _source.length);
	long real_size;
	long_restore(real_size, (char*)_source.data);

	// Parse secure key
	Cell<std::string> str_holder;
	StandardComponents::BlockToString bts;
	bts.GetOutput().Link(str_holder.GetDataInput());
	bts.GetInput().operator ()(this->secure_key);

	rsa_context ctx = __make_ctx_skey(str_holder.GetValue());

	DataBlock target(PT_LEN);

	int olen;

	int res = rsa_pkcs1_decrypt(&ctx, RSA_PRIVATE, &olen, (unsigned char*) source.data, (unsigned char*) target.data, target.length);
	if (res == 0)
	{
		target.length = olen;
	}

	StandardComponents::BlockUtilizer().GetInput().operator ()(source);
	target.length = real_size;

	return target;
};

DataBlock Encryptor::operator ()(const DataBlock source)
{
	unsigned int elementary_block_size = PT_LEN;
	std::list<DataBlock> blocks;
	std::list<DataBlock> crypted_blocks;
	unsigned long remaining_size = source.length;
	for (unsigned long i = 0; i < (source.length + elementary_block_size - 1) / elementary_block_size; i++)
	{
		DataBlock current_block;
		current_block.length = min(remaining_size, elementary_block_size);
		current_block.data = ((char*)source.data) + i * elementary_block_size;
		blocks.push_back(current_block);
		remaining_size -= current_block.length;
	};
	unsigned long total_size = 0;
	for (std::list<DataBlock>::iterator i = blocks.begin(); i != blocks.end(); i++)
	{
		DataBlock crypted_block = ShortBlockEncrypt::operator ()(*i);
		crypted_blocks.push_back(crypted_block);
		total_size += crypted_block.length;
	};
	DataBlock result;
	result.length = total_size;
	result.data = new char[result.length];
	char* placement_ptr = (char*) result.data;
	for (std::list<DataBlock>::iterator i = crypted_blocks.begin(); i != crypted_blocks.end(); i++)
	{
		memcpy(placement_ptr, i->data, i->length);
		placement_ptr += i->length;
		if (i->data)
			delete i->data;
	};
	return result;
};

DataBlock Decryptor::operator ()(const DataBlock source)
{
	unsigned int elementary_block_size = KEY_LENGTH / 8 + INTSIZE + GUARD;
	unsigned long block_num = source.length  / elementary_block_size;
	char* placement_ptr = static_cast<char*>(source.data);
	unsigned long total_size = 0;
	std::list<DataBlock> blocks;
	for (unsigned long i = 0; i < block_num; i++)
	{
		DataBlock block_to_decrypt;
		block_to_decrypt.length = elementary_block_size - INTSIZE;
		block_to_decrypt.data = placement_ptr;
		DataBlock decrypted_block = ShortBlockDecrypt::operator ()(block_to_decrypt);
		total_size += decrypted_block.length;
		blocks.push_back(decrypted_block);
		placement_ptr += block_to_decrypt.length + INTSIZE;
	};
	DataBlock result;
	result.length = total_size;
	result.data = new char[result.length];
	char* result_ptr = reinterpret_cast<char*>(result.data);
	for (std::list<DataBlock>::iterator i = blocks.begin(); i != blocks.end(); i++)
	{
		memcpy(result_ptr, i->data, i->length);
		result_ptr += i->length;
		delete i->data;
	};
	return result;
};

std::string Crypto::RSA::Sign(const std::string data, const std::string secure_key)
{
	rsa_context ctx = __make_ctx_skey(secure_key);
	
	size_t len = KEY_LENGTH / 8;
	unsigned char* signature = new unsigned char[len];

	int result = rsa_pkcs1_sign( &ctx, RSA_PRIVATE, SIG_RSA_RAW, data.size(), (unsigned char *) data.c_str(), signature );

	std::string retval;
	if (result == 0)
	{
		for (size_t i = 0; i < len; i++)
			retval += (char) signature[i];
	}

	delete signature;
	return retval;
};

bool Crypto::RSA::Verify(const std::string data, const std::string signature, const std::string public_key)
{
	rsa_context ctx = __make_ctx_pkey(public_key);
	ctx.len = signature.size();

	int result = rsa_pkcs1_verify( &ctx, RSA_PUBLIC, SIG_RSA_RAW, data.size(), (unsigned char *)data.c_str(), (unsigned char *) signature.c_str() );

	return result == 0;
};