/****************************************************************************************************/
/* CST Middleware 2008                        * Memory.cpp                                          */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components to represent memory management operations and handling untyped data blocks.  */
/****************************************************************************************************/
#include "Memory.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
using namespace CSTMiddleWare::StandardComponents;

DataBlock BlockConcatenator::operator()(const Joint<DataBlock, DataBlock> message)
{
	DataBlock result;
	result.length = message.x.length + message.y.length;
	if (result.length > 0)
	{
		result.data = new char[result.length];
	}
	else
	{
		result.data = NULL;
	};
	if (message.x.length > 0)
		memcpy(result.data, message.x.data, message.x.length);
	if (message.y.length > 0)
		memcpy(reinterpret_cast<char*>(result.data) + message.x.length, message.y.data, message.y.length);
	return result;
};

Joint<DataBlock, DataBlock> BlockSplitter::operator()(const DataBlock message)
{
	Joint<DataBlock, DataBlock> result;
	result.x.length = this->split_position;
	result.y.length = message.length - this->split_position;
	if (result.y.length < 0)
		throw int(0);
	if (result.x.length > 0)
	{
		result.x.data = new char[result.x.length];
		memcpy(result.x.data, message.data, result.x.length);
	}
	else
		result.x.data = NULL;
	if (result.y.length > 0)
	{
		result.y.data = new char[result.y.length];
		memcpy(result.y.data, reinterpret_cast<char*>(message.data) + result.x.length, result.y.length);
	}
	else
		result.y.data = NULL;
	return result;
};

void BlockSplitter::Run(const int position)
{
	this->split_position = position;
};

Joint<DataBlock, DataBlock> BlockDuplicator::operator ()(const DataBlock source)
{
	Joint<DataBlock, DataBlock> result;

	result.x = source;
	if (result.x.length <= 0)
	{
		result.x.length = result.y.length = 0;
		result.x.data = result.y.data = NULL;
	}
	else
	{
		result.y.length = result.x.length;
		result.y.data = new char[result.y.length];
		memcpy(result.y.data, source.data, result.y.length);
	};
	return result;
};

DataBlock BlockCreator::operator ()(const int size)
{
	DataBlock result;
	result.length = size;
	if (size > 0)
		result.data = new char[size];
	else
		result.data = NULL;
	return result;
};

void BlockUtilizer::Run(const DataBlock source)
{
	if (source.data != NULL)
		delete[] source.data;
};

DataBlock BlockComposer::operator ()(const Joint<void*, int> source)
{
	DataBlock result;
	result.data = source.x;
	result.length = source.y;
	if (result.length <= 0)
	{
		result.data = NULL;
		result.length = 0;
	};
	return result;
};

Joint<void*, int> BlockDecomposer::operator()(const DataBlock source)
{
	Joint<void*, int> result;
	result.x = source.data;
	result.y = source.length;
	if (result.y <= 0)
	{
		result.y = 0;
		result.x = NULL;
	};
	return result;
};

long SubblockScanner::operator()(const Joint<Joint<DataBlock, long>, DataBlock> source)
{
	char* position_to_scan = static_cast<char*>(source.x.x.data) + source.x.y;
	long  length_of_source = source.x.x.length - source.x.y - source.y.length;

	// No area to scan
	if (length_of_source < 0)
		return -1;

	for (; length_of_source >= 0; length_of_source--)
	{
		if (0 == memcmp(position_to_scan, source.y.data, source.y.length))
			return static_cast<long>(position_to_scan - static_cast<char*>(source.x.x.data));
		else
			position_to_scan++;
	};

	return -1;
};
