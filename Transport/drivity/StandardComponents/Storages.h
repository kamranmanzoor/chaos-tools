/****************************************************************************************************/
/* CST Middleware 2008                        * Storages.h                                          */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components to represent typical data storage structures.                                */
/****************************************************************************************************/
#ifndef STORAGES_H
#define STORAGES_H

#include "MessageEngine.h"
#include <list>
#include <map>

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;

namespace CSTMiddleWare
{

namespace StandardComponents
{

	// Constant value generator
	template<class Type>
	class Constant : public Translator<SIGNAL_TYPE, Type>
	{
	private:
		const Type cvalue;										// Value to keep and return
	public:
		// Constructor and destructor
		Constant(const Type value);
		virtual ~Constant();
		// Value retrieval (for non-CST interface)
		virtual const Type& GetValue() const {return cvalue;};
	protected:
		// Translate SIGNAL to Type
		virtual Type operator()(const SIGNAL_TYPE src_msg);
	};

	// Variable
	template<class Type>
	class Cell : public Consumer<SIGNAL_TYPE>, 
		         public Producer<Type>, 
				 public Producer<SIGNAL_TYPE>,
				 public Entity<SIGNAL_TYPE>, 
				 public Entity<Type>
	{
	protected:
		Type value;												// Value to store
	protected:
		TriggeringInputGate<Type>			input;				// Value update gate
		TriggeringInputGate<SIGNAL_TYPE>    read_input;			// Read request
		DirectOutputGate<Type>				output;				// Value retrieve gate
		DirectOutputGate<SIGNAL_TYPE>		notification;		// Notification about value updated
	public:
		// Constructors and destructor
		Cell();
		Cell(const Type init_value);
		virtual ~Cell();
		// Shortcuts for gates retrieval
		virtual InputGate<Type>& GetDataInput() {return input;};
		virtual OutputGate<Type>& GetDataOutput() {return Producer<Type>::GetOutput();};
		virtual InputGate<SIGNAL_TYPE>& GetReadCtrlInput() {return read_input;};
		virtual OutputGate<SIGNAL_TYPE>& GetNotification() {return notification;};
		// Value management (detour of CST interface)
		virtual Type GetValue() {return value;};
		virtual void SetValue(Type v) {value = v;};
	protected:
		// Message processing functions
		virtual Entity<Type>& GetMessage(InputGate<Type>& source_address, const Type message);
		virtual Entity<SIGNAL_TYPE>& GetMessage(InputGate<SIGNAL_TYPE>& source_address, const SIGNAL_TYPE message);
		virtual Entity<Type>& SendMessage(OutputGate<Type>& destination_address, const Type message);
		virtual Entity<SIGNAL_TYPE>& SendMessage(OutputGate<SIGNAL_TYPE>& destination_address, const SIGNAL_TYPE message);
		// Thread safety functions
		virtual void Lock() {Entity<Type>::Lock(); Entity<SIGNAL_TYPE>::Lock();};
		virtual void Unlock() {Entity<Type>::Unlock(); Entity<SIGNAL_TYPE>::Unlock();};
	};

	// List of items
	template<class Type>
	class List : public Entity<Type>,
				 public Entity<std::list<Type> >,
				 public Entity<SIGNAL_TYPE>,
				 public Entity<int>,
				 public Producer<Type>,
				 public Producer<int>,
				 public Producer< std::list<Type> >,
				 public Producer<SIGNAL_TYPE>,
				 public MultiIn<Type, int>,
				 public MultiIn< std::list<Type>, int >,
				 public MultiIn<SIGNAL_TYPE, int>
	{
	public:
		// Actions == gate selectors
		const int APPEND;
		const int ADD_FIRST;
		const int INSERT;
		const int REQUEST_CURRENT;
		const int REQUEST_FIRST;
		const int REQUEST_LAST;
		const int REQUEST_ALL;
		const int GO_NEXT;
		const int GO_PREVIOUS;
		const int REQUEST_SIZE;
		const int REMOVE;
		const int COPY_FROM;
		const int APPEND_FROM;
		const int GO_TO_START;
		const int GO_TO_END;
	protected:
		std::list<Type>						list;			// List of items
		typename std::list<Type>::iterator	pointer;		// Pointer to the current position
	protected:
		TriggeringInputGate<Type>			append;			// Append list (push_back)
		TriggeringInputGate<Type>			add_first;		// Add new item as the first one (push_front)
		TriggeringInputGate<Type>			insert;			// Insert item before the current one
		TriggeringInputGate<SIGNAL_TYPE>	request;		// Request the current list item
		TriggeringInputGate<SIGNAL_TYPE>	request_first;	// Request the first list item
		TriggeringInputGate<SIGNAL_TYPE>	request_last;	// Request the last list item
		TriggeringInputGate<SIGNAL_TYPE>	request_all;	// Request entire list
		TriggeringInputGate<SIGNAL_TYPE>	go_next;		// Shift pointer to the next item
		TriggeringInputGate<SIGNAL_TYPE>	go_prev;		// Shift pointer to the previous item
		TriggeringInputGate<SIGNAL_TYPE>	request_size;	// Get size of the list
		TriggeringInputGate<SIGNAL_TYPE>	remove;			// Remove current item
		TriggeringInputGate<std::list<Type> > copy_from;    // Copy from another list
		TriggeringInputGate<std::list<Type> > append_from;  // Append with another list
		TriggeringInputGate<SIGNAL_TYPE>	go_to_start;	// Set pointer to the head
		TriggeringInputGate<SIGNAL_TYPE>	go_to_end;		// Set pointer to (after) the tail

		DirectOutputGate<Type>				return_item;	// Item transfer
		DirectOutputGate<SIGNAL_TYPE>		error_ntf;		// Error notification
		DirectOutputGate<int>				return_size;	// Get size of the list
		DirectOutputGate<std::list<Type> >  return_list;	// Transfer of the entire list
	public:
		// Constructors and destructor
		List();
		virtual ~List();
		// Retrieve & set the value
		virtual std::list<Type>					GetValue() {return list;};
		virtual void							SetValue(const std::list<Type> _list) {list = _list;};
		// Shortcuts to gates
		virtual InputGate<Type>&				GetAppendGate() {return append;};
		virtual InputGate<Type>&				GetAddFirstGate() {return add_first;};
		virtual InputGate<Type>&				GetInsertGate()	{return insert;};
		virtual InputGate< std::list<Type> >&	GetAppendFromGate() {return append_from;};
		virtual InputGate< std::list<Type> >&	GetCopyFromGate() {return copy_from;};
		virtual InputGate<SIGNAL_TYPE>&			GetRequestItemGate() {return request;};
		virtual InputGate<SIGNAL_TYPE>&			GetRequestFirstGate() {return request_first;};
		virtual InputGate<SIGNAL_TYPE>&			GetRequestLastGate() {return request_last;};
		virtual InputGate<SIGNAL_TYPE>&			GetRequestAllGate()	{return request_all;};
		virtual InputGate<SIGNAL_TYPE>&			GetRequestSizeGate()	{return request_size;};
		virtual InputGate<SIGNAL_TYPE>&			GetRemoveGate() {return remove;};
		virtual InputGate<SIGNAL_TYPE>&			GetNextGate() {return go_next;};
		virtual InputGate<SIGNAL_TYPE>&			GetPreviousGate() {return go_prev;};
		virtual InputGate<SIGNAL_TYPE>&			GetFirstGate() {return go_to_start;};
		virtual InputGate<SIGNAL_TYPE>&			GetLastGate() {return go_to_end;};

		virtual OutputGate<Type>&				GetItemGate() {return return_item;};
		virtual OutputGate< std::list<Type> >&  GetListGate() {return return_list;};
		virtual OutputGate<int>&				GetSizeGate() {return return_size;};
		virtual OutputGate<SIGNAL_TYPE>&		GetErrorGate() {return error_ntf;};
	protected:
		// Message processing functions
		virtual Entity<Type>&				GetMessage(InputGate<Type>& gate, const Type message);
		virtual Entity<SIGNAL_TYPE>&		GetMessage(InputGate<SIGNAL_TYPE>& gate, const SIGNAL_TYPE message);
		virtual Entity< std::list<Type> >&	GetMessage(InputGate< std::list<Type> >& gate, const std::list<Type> message);
	protected:
		// Multithreading
		virtual void Lock()
		{
			Entity<Type>::Lock();
			Entity<std::list<Type> >::Lock();
			Entity<SIGNAL_TYPE>::Lock();
			Entity<int>::Lock();
		};
		virtual void Unlock()
		{
			Entity<Type>::Unlock();
			Entity<std::list<Type> >::Unlock();
			Entity<SIGNAL_TYPE>::Unlock();
			Entity<int>::Unlock();
		};
	};

	// Class to represent Table or Map as the sorted list of <key, value> pairs
	template<class Key, class Value>
	class Table : Entity< Joint<Key, Value> >,			 		// For Add
				  Entity<SIGNAL_TYPE>,							// For error notification
				  Entity<Key>,									// For Find
				  Entity< std::list<Key> >,						// For conversion to the list of keys
				  Entity< std::map<Key, Value> >,				// For conversion to STL map
				  MultiIn<Key, int>,							// For remove and find
				  MultiIn<SIGNAL_TYPE, int>,					// For conversion requests
				  Consumer< Joint<Key, Value> >,				// For add
				  Producer< Joint<Key, Value> >,				// For find
				  Producer<SIGNAL_TYPE>,						// For error notification
				  Consumer<Key>,								// For find
				  Producer< std::list<Key> >,					// For conversion 1
				  Producer< std::map<Key, Value > >,			// For conversion 2
				  Consumer< std::map<Key, Value > >				// For conversion from map
	{
	private:
		// List of operations
		const int CLEAN;
		const int CONVERT_TO_LIST;
		const int CONVERT_TO_MAP;
		const int FIND;
		const int REMOVE;
	protected:
		std::map<Key, Value> table;								// Internal table to keep values
	protected:
		TriggeringInputGate< Joint<Key, Value> >	add_input;	// Add new pair
		TriggeringInputGate<Key>					find_input; // Find value for the given key
		TriggeringInputGate<Key>					delete_input; // Remove the pair identified by given key
		TriggeringInputGate<SIGNAL_TYPE>			conv_to_list; // Request conversion to std::list
		TriggeringInputGate<SIGNAL_TYPE>			conv_to_map;  // Request conversion to std::map
		TriggeringInputGate<SIGNAL_TYPE>			clean_input;  // Remove all content
		TriggeringInputGate< std::map<Key, Value> >	map_input;	  // Convert from map

		DirectOutputGate< Joint<Key, Value> >       result_output;// Get found value
		DirectOutputGate<SIGNAL_TYPE>				error_output; // Get error notification
		DirectOutputGate< std::list<Key> >			list_output;  // List of keys (for conversion)
		DirectOutputGate< std::map<Key, Value> >    map_output;	  // Map (for conversion)
	public:
		Table();
		virtual ~Table();
		// Retrieve & set the value
		virtual std::map<Key, Value>			GetValue() {return table;};
		virtual void							SetValue(const std::map<Key, Value> _table) {table = _table;};
	protected:
		// Message handling
		  // Add new item
		virtual Entity< Joint<Key, Value> >& GetMessage(InputGate< Joint<Key, Value> >& gate,
														const Joint<Key, Value> message);
		  // Find / Remove
		virtual Entity<Key>& GetMessage(InputGate<Key>& gate, const Key message);
		  // Request clean up or conversion
		virtual Entity<SIGNAL_TYPE>& GetMessage(InputGate<SIGNAL_TYPE>& gate, const SIGNAL_TYPE value);
		  // Append from existing map
		virtual Entity< std::map<Key, Value> >& GetMessage(InputGate< std::map<Key, Value> >& gate,
															const std::map<Key, Value> message);
	public:
		// Shortcuts to gates
		virtual InputGate< Joint<Key, Value> >&    GetAddInputGate()           {return add_input;}
		virtual InputGate<Key>&                    GetFindInputGate()          {return find_input;};
		virtual InputGate<Key>&                    GetRemoveInputGate()        {return delete_input;};
		virtual InputGate<SIGNAL_TYPE>&            GetCleanupInputGate()       {return clean_input;};
		virtual InputGate<SIGNAL_TYPE>&            GetConvertToListInputGate() {return conv_to_list;};
		virtual InputGate<SIGNAL_TYPE>&            GetConvertToMapInputGate()  {return conv_to_map;};
		virtual InputGate< std::map<Key, Value> >& GetAppendFromMapInputGate() {return map_input;};

		virtual OutputGate< Joint<Key, Value> >&   GetResultOutputGate()       {return result_output;};
		virtual OutputGate<SIGNAL_TYPE>&		   GetErrorOutputGate()        {return error_output;};
		virtual OutputGate< std::list<Key> >&      GetListOutputGate()		   {return list_output;};
		virtual OutputGate< std::map<Key, Value> >& GetMapOutputGate()         {return map_output;};
	protected:
		// Thread safety
		virtual void Lock()
		{
			Entity< Joint<Key, Value> >::Lock();
			Entity<SIGNAL_TYPE>::Lock();
			Entity<Key>::Lock();
			Entity< std::list<Key> >::Lock();
			Entity< std::map<Key, Value> >::Lock();
		};
		virtual void Unlock()
		{
			Entity< Joint<Key, Value> >::Unlock();
			Entity<SIGNAL_TYPE>::Unlock();
			Entity<Key>::Unlock();
			Entity< std::list<Key> >::Unlock();
			Entity< std::map<Key, Value> >::Unlock();
		};
	};

	// Class to perform list iteration
	template<class Type>
	class ListIterator : public Entity< std::list<Type> >, 
						 public Entity<Type>,
						 public Entity<SIGNAL_TYPE>,
						 public Consumer<SIGNAL_TYPE>,
						 public Consumer< std::list<Type> >,
						 public Producer<Type>
	{
	private:
		bool abort_requested;							// True if abort requested
	protected:
		// Gates
		TriggeringInputGate< std::list<Type> > input;
		TriggeringInputGate<SIGNAL_TYPE>       abort_input;
		DirectOutputGate<Type>                 output;
	public:
		// Constructor and destructor
		ListIterator();
		virtual ~ListIterator();
		// Shortcuts to gates
		virtual InputGate< std::list<Type> >& GetDataInput()  {return input;};
		virtual InputGate<SIGNAL_TYPE>&       GetAbortInput() {return abort_input;};
		virtual OutputGate<Type>&             GetDataOutput() {return output;};
	protected:
		// Translate SIGNAL to Type
		virtual Entity< std::list<Type> >& GetMessage(InputGate< std::list<Type> >& gate, const std::list<Type> src_msg);
		virtual Entity<SIGNAL_TYPE>& GetMessage(InputGate<SIGNAL_TYPE>& gate, const SIGNAL_TYPE src_msg);
	};

};
	
};

/***************************** IMPLEMENTATION *********************************/

using namespace CSTMiddleWare::StandardComponents;

template<class Type>
Constant<Type>::Constant(const Type value) : cvalue(value)
{
};

template<class Type>
Constant<Type>::~Constant()
{
};

template<class Type>
Type Constant<Type>::operator ()(const SIGNAL_TYPE src_msg)
{
	return cvalue;
};

template<class Type>
Cell<Type>::Cell()
	: input(*this), read_input(*this), output(*this), notification(*this),
	  Producer<Type>(output), Consumer<SIGNAL_TYPE>(read_input), Producer<SIGNAL_TYPE>(notification)
{
};

template<class Type>
Cell<Type>::Cell(const Type init_value) 
	: input(*this), read_input(*this), output(*this), notification(*this),
	  Producer<Type>(output), Consumer<SIGNAL_TYPE>(read_input), Producer<SIGNAL_TYPE>(notification),
	  value(init_value)
{
};

template<class Type>
Cell<Type>::~Cell()
{
};

template<class Type>
Entity<Type>& Cell<Type>::GetMessage(InputGate<Type>& source_address, const Type message)
{
	Lock();
	value = message;
	// Notify subscribers about cell content changed
	SendMessage(notification, SIGNAL);
	Unlock();
	return *this;
};

template<class Type>
Entity<SIGNAL_TYPE>& Cell<Type>::GetMessage(InputGate<SIGNAL_TYPE>& source_address, const SIGNAL_TYPE message)
{
	SendMessage(GetDataOutput(), value);
	return *this;
};

template<class Type>
Entity<Type>& Cell<Type>::SendMessage(OutputGate<Type>& destination_address, const Type message)
{
	if (&destination_address != &GetDataOutput())
		throw int(0);
	destination_address.operator()(message);
	return *this;
};

template<class Type>
Entity<SIGNAL_TYPE>& Cell<Type>::SendMessage(OutputGate<SIGNAL_TYPE>& destination_address, const SIGNAL_TYPE message)
{
	if (&destination_address != &GetNotification())
		throw int(0);
	destination_address.operator()(message);
	return *this;
};

/****************************** List ************************/

template<class Type>
List<Type>::List() :
		append(*this),			// Append list (push_back)
		add_first(*this),		// Add new item as the first one (push_front)
		insert(*this),			// Insert item before the current one
		request(*this),			// Request the current list item
		request_first(*this),	// Request the first list item
		request_last(*this),	// Request the last list item
		request_all(*this),		// Request entire list
		go_next(*this),			// Shift pointer to the next item
		go_prev(*this),			// Shift pointer to the previous item
		request_size(*this),	// Get size of the list
		remove(*this),			// Remove current item
		copy_from(*this),		// Copy from another list
		append_from(*this),		// Append with another list
		return_item(*this),		// Item transfer
		error_ntf(*this),		// Error notification
		return_size(*this),		// Get size of the list
		return_list(*this),		// Transfer of the entire list
		go_to_start(*this),
		go_to_end(*this),
		Producer<Type>(return_item),
		Producer< std::list<Type> >(return_list),
		Producer<int>(return_size),
		Producer<SIGNAL_TYPE>(error_ntf),
		APPEND(0),
		ADD_FIRST(1),
		INSERT(2),
		REQUEST_CURRENT(3),
		REQUEST_FIRST(4),
		REQUEST_LAST(5),
		REQUEST_ALL(6),
		GO_NEXT(7),
		GO_PREVIOUS(8),
		REQUEST_SIZE(9),
		REMOVE(10),
		COPY_FROM(11),
		APPEND_FROM(12),
		GO_TO_START(13),
		GO_TO_END(14)
{
	// Pointer shows the unpresent position
	this->pointer = this->list.end();

	MultiIn<Type, int>::operator [](APPEND)		= &append;
	MultiIn<Type, int>::operator [](ADD_FIRST)	= &add_first;
	MultiIn<Type, int>::operator [](INSERT)     = &insert;

	MultiIn<SIGNAL_TYPE, int>::operator [](REQUEST_CURRENT) = &request;
	MultiIn<SIGNAL_TYPE, int>::operator [](REQUEST_FIRST)   = &request_first;
	MultiIn<SIGNAL_TYPE, int>::operator [](REQUEST_LAST)    = &request_last;
	MultiIn<SIGNAL_TYPE, int>::operator [](REQUEST_ALL)     = &request_all;
	MultiIn<SIGNAL_TYPE, int>::operator [](GO_NEXT)			= &go_next;
	MultiIn<SIGNAL_TYPE, int>::operator [](GO_PREVIOUS)		= &go_prev;
	MultiIn<SIGNAL_TYPE, int>::operator [](REQUEST_SIZE)    = &request_size;
	MultiIn<SIGNAL_TYPE, int>::operator [](REMOVE)			= &remove;
	MultiIn<SIGNAL_TYPE, int>::operator [](GO_TO_START)		= &go_to_start;
	MultiIn<SIGNAL_TYPE, int>::operator [](GO_TO_END)		= &go_to_end;
	MultiIn<std::list<Type>, int>::operator [](COPY_FROM)   = &copy_from;
	MultiIn<std::list<Type>, int>::operator [](APPEND_FROM) = &append_from;
};

template<class Type>
List<Type>::~List()
{
};

template<class Type>
Entity<Type>& List<Type>::GetMessage(InputGate<Type>& gate, const Type message)
{
	// Add new item to the end of the list
	if (&gate == &append)
	{
		list.push_back(message);
		pointer = list.end();
		pointer--;
		return *this;
	};

	// Add new item to the head of the list
	if (&gate == &add_first)
	{
		list.push_front(message);
		pointer = list.begin();
		return *this;
	};

	// Insert the item at current position
	if (&gate == &insert)
	{
		pointer = list.insert(pointer, message);
		return *this;
	};

	// If unknown input gate - unrecoverable middleware error
	throw int(0);
};

template<class Type>
Entity<SIGNAL_TYPE>& List<Type>::GetMessage(InputGate<SIGNAL_TYPE>& gate, const SIGNAL_TYPE message)
{
	// Move pointer to the next item
	if (&gate == &go_next)
	{
		if (pointer == list.end())
			Entity<SIGNAL_TYPE>::SendMessage(error_ntf, SIGNAL);
		else
			pointer++;
		return *this;
	};

	// Move pointer to the previous item
	if (&gate == &go_prev)
	{
		if (pointer == list.begin())
			Entity<SIGNAL_TYPE>::SendMessage(error_ntf, SIGNAL);
		else
			pointer--;
		return *this;
	};

	// Move pointer to the start of list
	if (&gate == &go_to_start)
	{
		pointer = list.begin();
		return *this;
	};

	// Move pointer to the end of list
	if (&gate == &go_to_end)
	{
		pointer = list.end();
		return *this;
	};

	// Remove item from the list
	if (&gate == &remove)
	{
		if (pointer == list.end())
			Entity<SIGNAL_TYPE>::SendMessage(error_ntf, SIGNAL);
		else
			pointer = list.erase(pointer);
		return *this;
	};

	// Request current list item
	if (&gate == &request)
	{
		if (pointer == list.end())
			Entity<SIGNAL_TYPE>::SendMessage(error_ntf, SIGNAL);
		else
			Entity<Type>::SendMessage(return_item, *pointer);
		return *this;
	};
	
	// Request entire list
	if (&gate == &request_all)
	{
		Entity< std::list<Type> >::SendMessage(return_list, list);
		return *this;
	};

	// Request the first item of the list
	if (&gate == &request_first)
	{
		pointer = list.begin();
		if (pointer == list.end())
			Entity<SIGNAL_TYPE>::SendMessage(error_ntf, SIGNAL);
		else
			Entity<Type>::SendMessage(return_item, *pointer);
		return *this;
	};

	// Request the last item of the list
	if (&gate == &request_last)
	{
		pointer = list.end();
		pointer--;
		if (pointer == list.end())
			Entity<SIGNAL_TYPE>::SendMessage(error_ntf, SIGNAL);
		else
			Entity<Type>::SendMessage(return_item, *pointer);
		return *this;
	};

	// Request the size of the list
	if (&gate == &request_size)
	{
		Entity<int>::SendMessage(return_size, static_cast<int>(list.size()));
		return *this;
	};

	// If invalid source gate - unrecoverable error
	throw int(0);
};

template<class Type>
Entity< std::list<Type> >& List<Type>::GetMessage(InputGate< std::list<Type> >& gate, const std::list<Type> message)
{
	// Append this list from another one
	if (&gate == &append_from)
	{
		std::list<Type> source = message;
		for (typename std::list<Type>::iterator i = source.begin(); i != source.end(); i++)
			list.push_back(*i);
		return *this;
	};

	// Copy this list from another one
	if (&gate == &copy_from)
	{
		list = message;
		return *this;
	};

	throw int(0);
};

/*********************************** Table ********************************/

// Constructor for table
template<class Key, class Value>
Table<Key, Value>::Table() :
		add_input(*this),
		find_input(*this),
		delete_input(*this),
		conv_to_list(*this),
		conv_to_map(*this),
		result_output(*this),
		error_output(*this),
		list_output(*this),
		map_output(*this),
		map_input(*this),
		clean_input(*this),
        Consumer< Joint<Key, Value> >(add_input),
		Producer< Joint<Key, Value> >(result_output),
		Producer<SIGNAL_TYPE>(error_output),
		Consumer<Key>(find_input),
		Producer< std::list<Key> >(list_output),
		Producer< std::map<Key, Value > >(map_output),
		Consumer< std::map<Key, Value > >(map_input),
		CLEAN(0),
		CONVERT_TO_LIST(1),
		CONVERT_TO_MAP(2),
		FIND(3),
		REMOVE(4)
{
	MultiIn<Key, int>::operator [](FIND)	= &find_input;
	MultiIn<Key, int>::operator [](REMOVE)	= &delete_input;

	MultiIn<SIGNAL_TYPE, int>::operator [](CLEAN)			= &clean_input;
	MultiIn<SIGNAL_TYPE, int>::operator [](CONVERT_TO_LIST) = &conv_to_list;
	MultiIn<SIGNAL_TYPE, int>::operator [](CONVERT_TO_MAP)  = &conv_to_map;
};

// Destructor for table
template<class Key, class Value>
Table<Key, Value>::~Table()
{
}

// Add new item
template<class Key, class Value>
Entity< Joint<Key, Value> >& Table<Key, Value>::GetMessage(InputGate< Joint<Key, Value> >& gate,
														const Joint<Key, Value> message)
{
	table[message.x] = message.y;
	return *this;
};

// Find / Remove
template<class Key, class Value>
Entity<Key>& Table<Key, Value>::GetMessage(InputGate<Key>& gate, const Key message)
{
	// Process FIND request
	if (&gate == &find_input)
	{
		// Check is there such element in the table
		typename std::map<Key, Value>::iterator found = table.find(message);
		if (found != table.end())
		{
			// If yes return output data
			Joint<Key, Value> result;
			result.x = message;
			result.y = found->second;
			Entity< Joint<Key, Value> >::SendMessage(result_output, result);
		}
		else
		{
			// Otherwise return error signal
			Entity<SIGNAL_TYPE>::SendMessage(error_output, SIGNAL);
		};
		return *this;
	};

	// Process DELETE request
	if (&gate == &delete_input)
	{
		typename std::map<Key, Value>::iterator found = table.find(message);
		// Delete item
		if (found != table.end())
			table.erase(found);
		return *this;
	};

	// If some request which can't be handled
	throw int(0);
};

// Request clean up or conversion
template<class Key, class Value>
Entity<SIGNAL_TYPE>& Table<Key, Value>::GetMessage(InputGate<SIGNAL_TYPE>& gate, const SIGNAL_TYPE value)
{
	// If clean up requested
	if (&gate == &clean_input)
	{
		table.clear();
		return *this;
	};

	// If conversion to std::map requested
	if (&gate == &conv_to_map)
	{
		// Transfer map to the output
		Entity< std::map<Key, Value> >::SendMessage(map_output, table);
		return *this;
	};

	// If conversion to std::list requested
	if (&gate == &conv_to_list)
	{
		std::list<Key> result;
		// Form the list
		for (typename std::map<Key, Value>::iterator i = table.begin(); i != table.end(); i++)
		{
			result.push_back(i->first);
		};
		// Send the list
		Entity< std::list<Key> >::SendMessage(list_output, result);
		return *this;
	};

	// If unhandled request
	throw int(0);
};

// Append from existing map
template<class Key, class Value>
Entity< std::map<Key, Value> >& Table<Key, Value>::GetMessage(InputGate< std::map<Key, Value> >& gate,
											const std::map<Key, Value> message)
{
	std::map<Key, Value> source = message;
	for (typename std::map<Key, Value>::iterator i = source.begin(); i != source.end(); i++)
	{
		table[i->first] = i->second;
	};
	return *this;
};

// List iterator constructor
template<class Type>
ListIterator<Type>::ListIterator() :
	input(*this), output(*this), Consumer< std::list<Type> >(input), Producer<Type>(output),
	abort_input(*this), Consumer<SIGNAL_TYPE>(abort_input)
{
	abort_requested = false;
};

// List iterator destructor
template<class Type>
ListIterator<Type>::~ListIterator()
{
};

// List iterator
template<class Type>
Entity< std::list<Type> >& ListIterator<Type>::GetMessage(InputGate< std::list<Type> >& gate, const std::list<Type> src_msg)
{
	std::list<Type> list = src_msg;
	abort_requested = false;
	// Iterate all items of the source list
	for (typename std::list<Type>::iterator i = list.begin(); i != list.end(); i++)
	{
		// Check if abort is requested
		bool quit = abort_requested;
		if (quit)
			break;
		// Send the next message
		Entity<Type>::SendMessage(this->GetOutput(), *i);
	};
	return *this;
};

// Abort request processing
template<class Type>
Entity<SIGNAL_TYPE>& ListIterator<Type>::GetMessage(InputGate<SIGNAL_TYPE>& gate, const SIGNAL_TYPE message)
{
	abort_requested = true;

	return *this;
};

#endif
