/****************************************************************************************************/
/* CST Middleware 2008                        * Storages.cpp                                        */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components to represent typical data storage structures.                                */
/****************************************************************************************************/
#include "Storages.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
using namespace CSTMiddleWare::StandardComponents;

// This symbol just to have the .lib file not empty
int ____DUMMY_STORAGE = 0;