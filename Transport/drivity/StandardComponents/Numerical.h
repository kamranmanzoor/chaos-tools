/****************************************************************************************************/
/* CST Middleware 2008                        * Numerical.h                                         */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components to represent arithmetic operations.                                          */
/****************************************************************************************************/
#ifndef NUMERICAL_H
#define NUMERICAL_H

#include "MessageEngine.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;

namespace CSTMiddleWare
{

namespace StandardComponents
{

	// Up/Down counter
	template<class Type>
	class Counter : public Entity<SIGNAL_TYPE>,
					public MultiIn<SIGNAL_TYPE, int>,
					public Entity<Type>,
					public Producer<Type>
	{
	protected:
		Type			from;				// Value from which it starts counting
		Type			to;					// Value when it stops counting
		Type			step;				// Step to count
		Type			current;			// Current value
	protected:
		TriggeringInputGate<SIGNAL_TYPE> count_input;	// Ticks to count
		TriggeringInputGate<SIGNAL_TYPE> reset_input;	// Reset to base value
		DirectOutputGate<Type>			 value_output;	// Gate to transmit current counter value
	public:
		// Constructor and destructor
		Counter(Type _from = 0, Type _to = 100, Type _step = +1);
		virtual ~Counter();
	protected:
		// Process message
		virtual Entity<SIGNAL_TYPE>& GetMessage(InputGate<SIGNAL_TYPE>& gate, const SIGNAL_TYPE signal);
	};

	// Increment/Decrement maker
	template<class Type>
	class Increaser : public Function<Type>
	{
	protected:
		Type			step;						// Value to add
	public:
		Increaser(const Type _step = 1);
		virtual ~Increaser();
	protected:
		virtual Type operator()(const Type message);
	};

	// Scaler / Descaler
	template<class Type>
	class Scaler : public Function<Type>
	{
	protected:
		Type			factor;						// Value to add
	public:
		Scaler(const Type _factor = 1);
		virtual ~Scaler();
	protected:
		virtual Type operator()(const Type message);
	};

	// Summator
	template<class Type>
	class Summator : public Translator< Joint<Type, Type>, Type>
	{
	public:
		Summator() {};
		virtual ~Summator() {};
	protected:
		virtual Type operator()(const Joint<Type, Type> message);
	};

	// Subtractor
	template<class Type>
	class Subtractor : public Translator< Joint<Type, Type>, Type>
	{
	public:
		Subtractor() {};
		virtual ~Subtractor() {};
	protected:
		virtual Type operator()(const Joint<Type, Type> message);
	};

	// Multiplier
	template<class Type>
	class Multiplier : public Translator< Joint<Type, Type>, Type>
	{
	public:
		Multiplier() {};
		virtual ~Multiplier() {};
	protected:
		virtual Type operator()(const Joint<Type, Type> message);
	};

	// Divider
	template<class Type>
	class Divider : public Translator< Joint<Type, Type>, Type>
	{
	public:
		Divider() {};
		virtual ~Divider() {};
	protected:
		virtual Type operator()(const Joint<Type, Type> message);
	};

	/********************************** IMPLEMENTATION *****************************/

	template<class Type>
	Counter<Type>::Counter(Type _from, Type _to, Type _step)
		: from(_from), to(_to), step(_step),
		  count_input(*this), reset_input(*this), value_output(*this),
		  Producer<Type>(value_output)
	{
		current = from;
	};

	template<class Type>
	Counter<Type>::~Counter()
	{
	};

	template<class Type>
	Entity<SIGNAL_TYPE>& Counter<Type>::GetMessage(InputGate<SIGNAL_TYPE> &gate, const SIGNAL_TYPE signal)
	{
		// Reset
		if (&gate == &reset_input)
		{
			current = from;
			return *this;
		};
		// Count
		if (&gate == &count_input)
		{
			if (step >= 0)
			{
				if (current <= to)
					Entity<Type>::SendMessage(value_output, current);
				current += step;
				if (current > to)
					current = from;
			}
			else
			{
				if (current >= to)
					Entity<Type>::SendMessage(value_output, current);
				current += step;
				if (current < to)
					current = from;
			};
			return *this;
		};

		// Unknown gate
		throw int(0);
	};

	/******************************* Increaser *******************************/

	template<class Type>
	Increaser<Type>::Increaser(const Type _step) : step(_step)
	{
	};

	template<class Type>
	Increaser<Type>::~Increaser()
	{
	};

	template<class Type>
	Type Increaser<Type>::operator ()(const Type message)
	{
		return message + step;
	};

	template<class Type>
	Scaler<Type>::Scaler(const Type _factor) : factor(_factor)
	{
	};

	template<class Type>
	Scaler<Type>::~Scaler()
	{
	};

	template<class Type>
	Type Scaler<Type>::operator ()(const Type message)
	{
		return message * factor;
	};

	/*********************************** Summator, Subtractor, Multiplier, Divider ****************************/

	template<class Type>
	Type Summator<Type>::operator ()(const Joint<Type, Type> message)
	{
		return message.x + message.y;
	};

	template<class Type>
	Type Subtractor<Type>::operator ()(const Joint<Type, Type> message)
	{
		return message.x - message.y;
	};

	template<class Type>
	Type Multiplier<Type>::operator ()(const Joint<Type, Type> message)
	{
		return message.x - message.y;
	};

	template<class Type>
	Type Divider<Type>::operator ()(const Joint<Type, Type> message)
	{
		return message.x / message.y;
	};

};
	
};

#endif