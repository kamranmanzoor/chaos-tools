#ifndef STANDARD_COMPONENTS_H
#define STANDARD_COMPONENTS_H

#ifdef GetMessage
#undef GetMessage
#endif

#include "Elementary.h"
#include "Files.h"
#include "Numerical.h"
#include "Storages.h"
#include "Strings.h"
#include "Memory.h"

#endif