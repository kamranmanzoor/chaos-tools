/****************************************************************************************************/
/* Sockets & HTTP Adapter 2008            * Base64.h                                                */
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2008                                                            */
/****************************************************************************************************/
/* Support MIME/Base64 encoding/decoding functions.                                                 */
/* Reused from http://base64.sourceforge.net/b64.c                                                  */
/****************************************************************************************************/
#ifndef BASE64_H
#define BASE64_H

#include <string>
#include <list>
#include <map>

namespace Componentality
{

	namespace Simplicity
	{

		namespace Configuration
		{

			std::string BASE64_encode(std::string);
			std::string BASE64_decode(std::string);

		}; // namespace Configuration

	}; // namespace Simplicity

}; // namespace Componentality

#endif