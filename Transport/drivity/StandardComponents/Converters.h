/****************************************************************************************************/
/* CST Middleware 2008                        * Converters.h                                        */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Converters for several standard types.                                                           */
/****************************************************************************************************/
#ifndef CONVERTERS_H
#define CONVERTERS_H

#include "MessageEngine.h"
#include "StandardComponents.h"
#include <string>

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
using namespace CSTMiddleWare::StandardComponents;

namespace CSTMiddleWare
{

	namespace Utilities
	{

		// Supplementary type for Text as list of strings
		typedef std::list<std::string> Text;

		// Converter of the list of key=value pairs to std::map
		class TextToMap : public Translator< Text, std::map<std::string, std::string> >,
				 		  public Execute<std::string>
		{
		protected:
			std::string delimiter;
		public:
			TextToMap(const std::string = "=");
			virtual ~TextToMap();
		protected:
			virtual std::map<std::string, std::string> operator()(const Text);
			virtual void Run(const std::string delim) {delimiter = delim;};
		public:
			virtual InputGate<Text>& GetDataInputGate() {return Translator< Text, std::map<std::string, std::string> >::GetInput();}
			virtual InputGate<std::string>& GetDelimiterInputGate() {return Execute<std::string>::GetInput();};
		};

		// Map to text converter
		class MapToText : public Translator< std::map<std::string, std::string>, Text >,
						  public Execute<std::string>
		{
		protected:
			std::string delimiter;
		public:
			MapToText(const std::string = "=");
			virtual ~MapToText();
		protected:
			virtual Text operator()(const std::map<std::string, std::string>);
			virtual void Run(const std::string delim) {delimiter = delim;};
		public:
			virtual InputGate< std::map<std::string, std::string> >& GetDataInputGate() 
				{return Translator< std::map<std::string, std::string>, Text >::GetInput();}
			virtual InputGate<std::string>& GetDelimiterInputGate() {return Execute<std::string>::GetInput();};
		};

		// Convert DataBlock to hex code
		class BlockToXCode : public Translator<DataBlock, std::string>
		{
		public:
			BlockToXCode();
			virtual ~BlockToXCode();
		protected:
			virtual std::string operator()(const DataBlock);
		};

		// Convert hex code to DataBlock
		class XCodeToBlock : public Translator<std::string, DataBlock>
		{
		public:
			XCodeToBlock();
			virtual ~XCodeToBlock();
		protected:
			virtual DataBlock operator()(const std::string);
		};

		// Convert number to block
		template <class NUM>
		class NumberToBlock : public Translator<NUM, DataBlock>
		{
		public:
			NumberToBlock() {};
			virtual ~NumberToBlock() {};
			virtual DataBlock operator()(const NUM);
		};

		// Convert block to number
		template <class NUM>
		class BlockToNumber : public Translator<DataBlock, NUM>
		{
		public:
			BlockToNumber() {};
			virtual ~BlockToNumber() {};
			virtual NUM operator()(const DataBlock);
		};

		// Convert string to integer
		template <class NUM>
		class StringToNumber : public Translator<std::string, NUM>
		{
		public:
			StringToNumber() {};
			virtual ~StringToNumber() {};
			virtual NUM operator() (const std::string);
		};

		// Convert integer to string
		template <class NUM>
		class NumberToString : public Translator<NUM, std::string>
		{
		public:
			NumberToString() {};
			virtual ~NumberToString() {};
			virtual std::string operator() (const NUM);
		};


		///////////////////////////////// IMPLEMENTATION ///////////////////////////////

		template <class NUM>
		DataBlock NumberToBlock<NUM>::operator()(const NUM n)
		{
			DataBlock result(sizeof(n));
			for (int i = 0; i < sizeof(n); i++)
			{
				char val = (n >> (i * 8)) & 0xFF;
				static_cast<char*>(result.data)[i] = val;
			};
			return result;
		};

		template <class NUM>
		NUM BlockToNumber<NUM>::operator()(const DataBlock block)
		{
			if (block.length != sizeof(NUM))
				throw block;
			
			NUM result = 0;
			for (int i = 0; i < block.length; i++)
			{
				char byte = static_cast<char*>(block.data)[block.length - i - 1];
				result <<= 8;
				result |= byte & 0xFF;
			};

			return result;
		};

		template <class NUM>
		NUM StringToNumber<NUM>::operator()(const std::string str)
		{
			NUM result = 0;
			size_t startpos = 0;
			bool negative = false;;

			if (str.size() == 0)
				return result;

			// Process 'minus' sign
			if (str[0] == '-')
			{
				negative = true;
				startpos = 1;
			};

			for (size_t i = startpos; i < str.size(); i++)
			{
				char digit = str[i] - '0';
				result = result * 10 + digit;
			};

			if (negative)
				result = (-result);

			return result;
		};

		template <class NUM>
		std::string NumberToString<NUM>::operator() (const NUM _num)
		{
			NUM num = _num;
			std::string result;
			if (num == 0)
				return "0";
			if (num < 0)
			{
				num = -num;
				result.push_back('-');
			};
			std::string temp_result;
			while (num > 0)
			{
				char digit = (char) (num % 10) + '0';
				num = num / 10;
				temp_result.push_back(digit);
			};

			for (size_t i = 0; i < temp_result.size(); i++)
			{
				result.push_back(temp_result[temp_result.size() - i - 1]);
			};

			return result;
		};

	};

}

#endif