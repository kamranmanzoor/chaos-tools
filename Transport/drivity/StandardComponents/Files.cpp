/****************************************************************************************************/
/* CST Middleware 2008                        * Files.cpp                                           */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components to represent file management operations.                                     */
/****************************************************************************************************/
#include "Files.h"
#include <fstream>

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
using namespace CSTMiddleWare::StandardComponents;

// This symbol just to have the .lib file not empty
int ____DUMMY_FILES = 0;

FileWriter::FileWriter()
{
	position = 0;
};

FileWriter::~FileWriter()
{
};

bool FileWriter::operator()(const Joint<std::string, DataBlock> source_info)
{
	if (source_info.y.length < 0)
		return true;
	// Open file
	std::ofstream file(source_info.x.c_str(), std::ios::binary);
	if (file.bad())
		return false;
	file.seekp(position);
	// Try to seek to the right location
	if (static_cast<int>(file.tellp()) != position)
	{
		file.close();
		return false;
	};
	// Write data to file
	file.write(reinterpret_cast<char*>(source_info.y.data), source_info.y.length);
	// Return
	bool result = file.good();
	file.close();
	return result;
};

FileReader::FileReader()
{
	position = 0;
};

FileReader::~FileReader()
{
};

DataBlock FileReader::operator()(const Joint<std::string, int> source_info)
{
	// Prepare resule
	DataBlock result;
	result.data = NULL;
	result.length = 0;
	// Open file
	std::ifstream file;
	file.open(source_info.x.c_str(), std::ios::binary);
	if (file.bad())
	{
		return result;
	};
	file.seekg(position);
	int tellg = static_cast<int>(file.tellg());
	if (tellg != position)
	{
		file.close();
		return result;
	};
	// Read data
	file.seekg(0, std::ios::end);
	int length = static_cast<int>(file.tellg());
	file.seekg(position);
	result.length = length - position;
	// If required length < 0, read all data till the end
	if ((result.length > source_info.y) && (source_info.y >= 0))
		result.length = source_info.y;
	if (result.length > 0)
	{
		result.data = new char[result.length];
		file.read(reinterpret_cast<char*>(result.data), result.length);
	};
	// Check for errors
	if (file.bad())
	{
		if (result.data != NULL)
			delete[] result.data;
		result.data = NULL;
		result.length = 0;
	};
	file.close();
	return result;
};

FileExistenceCheck::FileExistenceCheck()
{
};

FileExistenceCheck::~FileExistenceCheck()
{
};

bool FileExistenceCheck::operator()(const std::string fname)
{
	std::ifstream file;

	file.open(fname.c_str());
	bool result = file.good();
	file.close();

	return result;
};
