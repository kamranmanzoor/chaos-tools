/****************************************************************************************************/
/* CST Middleware 2008                        * Elementary.h                                        */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components for CST MiddleWare. Controls to manage elementary data & constructions       */
/****************************************************************************************************/
#include "Elementary.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
using namespace CSTMiddleWare::StandardComponents;

Branch::Branch() : input(*this), Consumer<bool>(input), 
				   outputX(*this), outputY(*this)
{
	MultiOut<MessageEngine::SIGNAL_TYPE, bool>::operator [](false) = &outputX;
	MultiOut<MessageEngine::SIGNAL_TYPE, bool>::operator [](true)  = &outputY;
};

Branch::~Branch()
{
};

Entity<bool>& Branch::GetMessage(InputGate<bool>& source_address, const bool message)
{
	if (&source_address != &GetInput())
		throw int(0);
	SendMessage(*MultiOut<MessageEngine::SIGNAL_TYPE, bool>::GetOutputGate(message), SIGNAL);
	return *this;
};

Entity<SIGNAL_TYPE>& Branch::SendMessage(OutputGate<SIGNAL_TYPE>& distribution_address, const SIGNAL_TYPE message)
{
	if ((&distribution_address != &GetOutputX()) &&
		(&distribution_address != &GetOutputY()))
		throw int(0);
	distribution_address.operator ()(message);
	return *this;
};
