/****************************************************************************************************/
/* CST Middleware 2008                        * Memory.h                                            */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components to represent memory management operations and handling untyped data blocks.  */
/****************************************************************************************************/
#ifndef MEMORY_H
#define MEMORY_H
#include <string>
#include <string.h>
#include "MessageEngine.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;

namespace CSTMiddleWare
{

namespace StandardComponents
{

	// Data block representation
	struct DataBlock
	{
		void*		data;								// Pointer to data
		long	  	length;								// Length of data
		DataBlock() {data = NULL; length = 0;};
		DataBlock(const long l) {length = l; if (l > 0) data = new char[l]; else data = NULL;};
		DataBlock(void* const d, const long l) {data = d; length = l;};
		DataBlock(const std::string str) {data = new char[str.size()]; length = (long) str.size(); if (length) memcpy(data, str.c_str(), length);};
	};

	// Untypizer - convert typized data to DataBlock
	template<class Type>
	class Untypizer : public Translator<Type, DataBlock>
	{
	public:
		Untypizer() {};
		virtual ~Untypizer() {};
	protected:
		virtual DataBlock operator()(const Type message);
	};

	// Typizer - convert data block to typized data
	template<class Type>
	class Typizer : public Translator<DataBlock, Type>
	{
	public:
		Typizer() {};
		virtual ~Typizer() {};
	protected:
		virtual Type operator()(const DataBlock message);
	};

	// Concatenate blocks
	class BlockConcatenator : public Translator< Joint<DataBlock, DataBlock>, DataBlock >
	{
	public:
		BlockConcatenator() {};
		virtual ~BlockConcatenator() {};
	protected:
		virtual DataBlock operator()(const Joint<DataBlock, DataBlock> message);
	};

	// Split blocks
	class BlockSplitter : public Translator< DataBlock, Joint<DataBlock, DataBlock> >,
						  Execute<int>
	{
	private:
		int			split_position;
	public:
		BlockSplitter(const int pos = 0) : split_position(pos) {};
		virtual ~BlockSplitter() {};
	protected:
		virtual Joint<DataBlock, DataBlock> operator()(const DataBlock message);
		virtual void Run(const int position);
	protected:
		virtual void Lock()
		{
			Entity< Joint<DataBlock, DataBlock> >::Lock();
			Entity<DataBlock>::Lock();
			Entity<int>::Lock();
		};
		virtual void Unlock()
		{
			Entity< Joint<DataBlock, DataBlock> >::Unlock();
			Entity<DataBlock>::Unlock();
			Entity<int>::Unlock();
		};
	};

	// Block duplicator does two blocks with the same content from one source (one is leaving unchanged)
	class BlockDuplicator : public Translator< DataBlock, Joint<DataBlock, DataBlock> >
	{
	public:
		BlockDuplicator() {};
		virtual ~BlockDuplicator() {};
	protected:
		virtual Joint<DataBlock, DataBlock> operator()(const DataBlock source);
	};

	// Block creator
	class BlockCreator : public Translator<int, DataBlock>
	{
	public:
		BlockCreator() {};
		virtual ~BlockCreator() {};
	protected:
		virtual DataBlock operator()(const int size);
	};

	// Block utilizer
	class BlockUtilizer : public Execute<DataBlock>
	{
	public:
		BlockUtilizer() {};
		virtual ~BlockUtilizer() {};
	protected:
		virtual void Run(const DataBlock source);
	};

	// Compose block from data and size fields
	class BlockComposer : public Translator< Joint<void*, int>, DataBlock >
	{
	public:
		BlockComposer() {};
		virtual ~BlockComposer() {};
	protected:
		virtual DataBlock operator()(const Joint<void*, int> source);
	};

	// Decompose block to data and size
	class BlockDecomposer : public Translator< DataBlock, Joint<void*, int> >
	{
	public:
		BlockDecomposer() {};
		virtual ~BlockDecomposer() {};
	protected:
		virtual Joint<void*, int> operator()(const DataBlock source);
	};

	// Retrieve size of the given type
	template<class Type>
	class SizeRetriever : public Translator<SIGNAL_TYPE, int>
	{
	public:
		SizeRetriever() {};
		virtual ~SizeRetriever() {};
	protected:
		virtual int operator()(const SIGNAL_TYPE signal);
	};

	// Scanning data block for subblock
	class SubblockScanner
		: public Translator<Joint<Joint<DataBlock, long>, DataBlock>, long>
	{
	public:
		SubblockScanner() {};
		virtual ~SubblockScanner() {};
	public:
		virtual long operator()(const Joint<Joint<DataBlock, long>, DataBlock>);
	};

	/*********************************** Implementation *******************************/

	template<class Type>
	DataBlock Untypizer<Type>::operator()(const Type message)
	{
		DataBlock result;
		result.length = sizeof(message);
		result.data = new Type(message);
		return result;
	};

	template<class Type>
	Type Typizer<Type>::operator()(const DataBlock message)
	{
		Type* ptr = reinterpret_cast<Type*>(message.data);
		Type result = *ptr;
		delete ptr;
		return result;
	};

	template<class Type>
	int SizeRetriever<Type>::operator()(const SIGNAL_TYPE signal)
	{
		return sizeof(Type);
	};

};
	
};
#endif
