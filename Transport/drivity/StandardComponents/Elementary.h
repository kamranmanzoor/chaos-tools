/****************************************************************************************************/
/* CST Middleware 2008                        * Elementary.h                                        */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components for CST MiddleWare. Controls to manage elementary data & constructions       */
/****************************************************************************************************/
#ifndef ELEMENTARY_H
#define ELEMENTARY_H

#include "MessageEngine.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;

namespace CSTMiddleWare
{

namespace StandardComponents
{

	// Comparator which does the comparison of two values coming to the input gate and returns
	// true or false via the output gate. Comparison condition (see the list below) must be
	// set up when the component declares.
	template<class Type>
	class Comparison : public Translator<Joint<Type, Type>, bool>
	{
	public:
		enum ComparisonType 
		{
			EQUAL,
			NOT_EQUAL,
			MORE,
			LESS,
			MORE_OR_EQUAL,
			LESS_OR_EQUAL
		};
	public:
		Comparison(ComparisonType = EQUAL);
		virtual ~Comparison();
	protected:
		virtual bool operator()(const Joint<Type, Type> src_msg);
	private:
		ComparisonType type;
	};

	// Branch component converts boolean input to the signal going through the one output gate if
	// input is "false" and through another one if "true".
	class Branch : public Consumer<bool>, 
		           public MultiOut<SIGNAL_TYPE, bool>,
				   public Entity<SIGNAL_TYPE>,
				   public Entity<bool>
	{
	protected:
		TriggeringInputGate<bool> input;
		DirectOutputGate<SIGNAL_TYPE> outputX;
		DirectOutputGate<SIGNAL_TYPE> outputY;
	public:
		Branch();
		virtual ~Branch();
		virtual OutputGate<SIGNAL_TYPE>& GetOutputX() {return outputX;}; // If "false" signal to be sent to X-gate
		virtual OutputGate<SIGNAL_TYPE>& GetOutputY() {return outputY;}; // If "true" signal to be sent to Y-gate
	protected:
		virtual Entity<bool>& GetMessage(InputGate<bool>& source_address, 
														const bool message);
		virtual Entity<SIGNAL_TYPE>& SendMessage(
			OutputGate<SIGNAL_TYPE>& distribution_address, 
			const SIGNAL_TYPE message);
	};

	// The structured component routing given message to the output gate selecting with "Selector".
	// Works like random number of output gates splitter.
	template<class Message, class Selector>
	class MultiSelector : public MultiOut<Message, Selector>,
						  public Entity< Joint<Message, Selector> >,
						  public Entity<Message>,
						  public Consumer< Joint<Message, Selector> >
	{
	private:
		TriggeringInputGate< Joint<Message, Selector> > input;
	public:
		MultiSelector();
		virtual ~MultiSelector();
	public:
		virtual OutputGate<Message>*& operator[](Selector s);
	protected:
		virtual Entity< Joint<Message, Selector> >& GetMessage(
			InputGate< Joint<Message, Selector> >& source_address, 
			const Joint<Message, Selector> message);
		virtual Entity<Message>& SendMessage(
			OutputGate<Message>& distribution_address, 
			const Message message);
		virtual void Lock() {Entity<Message>::Lock(); Entity< Joint<Message, Selector> >::Lock();};
		virtual void Unlock() {Entity<Message>::Unlock(); Entity< Joint<Message, Selector> >::Unlock();};
	};

	// Reference to the input gate (might be transferred as a message)
	template<class Message>
	class InputReference
	{
	private:
		InputGate<Message>* ref;
	public:
		InputReference() : ref(NULL) {};
		InputReference(InputGate<Message>& _ref) : ref(&_ref) {};
		virtual ~InputReference() {};
		virtual operator InputGate<Message>&() { if (NULL == ref) throw int(0); else return *ref;};
	};

	// Reference to the output gate (might be transferred as a message)
	template<class Message>
	class OutputReference
	{
	private:
		OutputGate<Message>* ref;
	public:
		OutputReference() : ref(NULL) {};
		OutputReference(OutputGate<Message>& _ref) : ref(&_ref) {};
		virtual ~OutputReference() {};
		virtual operator OutputGate<Message>&() { if (NULL == ref) throw int(0); else return *ref;};
	};

	// Reference to the entity (lets transfer pointer to the enitiy as a message)
	template<class Type>
	class EntityReference
	{
	private:
		Type* ref;
	public:
		EntityReference(Type& entity) : ref(&entity) {};
		virtual ~EntityReference() {};
		virtual operator Type&() {return *ref;};
		virtual Type& GetObject() {return *ref;};
	};

	// Component to provide reference to the object pointed on construction when signal received on the input
	template<class Type>
	class EntityReferenceProvider : public Translator<SIGNAL_TYPE, EntityReference<Type> >
	{
	private:
		EntityReference<Type>* ref;
	public:
		EntityReferenceProvider(Type& _ref) : ref(new EntityReference<Type>(_ref)) {};
		virtual ~EntityReferenceProvider() {delete ref;};
		virtual EntityReference<Type> operator()(const SIGNAL_TYPE) {return *ref;};
	};

	// Component makes two equivalent messages and transfers it to the joint output
	template<class Message>
	class Duplicate : public Translator<Message, Joint<Message, Message> >
	{
	public:
		Duplicate();
		virtual ~Duplicate();
	protected:
		virtual Joint<Message, Message> operator()(const Message src_msg);
	};

	// Reference provider for the input gate
	template<class Message>
	class InputReferenceProvider : public Translator< EntityReference< Consumer<Message> >, InputReference<Message> >
	{
	public:
		InputReferenceProvider();
		virtual ~InputReferenceProvider();
	protected:
		virtual InputReference<Message> operator()(const EntityReference< Consumer<Message> > src);
	};

	// Reference provider to the output gate
	template<class Message>
	class OutputReferenceProvider : public Translator< EntityReference< Producer<Message> >, OutputReference<Message> >
	{
	public:
		OutputReferenceProvider();
		virtual ~OutputReferenceProvider();
	protected:
		virtual OutputReference<Message> operator()(const EntityReference< Producer<Message> > src);
	};

	// Link to be established in runtime between the output gate defined by output reference
	// and the input gate defined by input reference
	template<class Message>
	class DynamicLink : public Execute< Joint<InputReference<Message>, OutputReference<Message> > >
	{
	public:
		DynamicLink();
		virtual ~DynamicLink();
	protected:
		virtual void Run(const Joint< InputReference<Message>, OutputReference<Message> > msg);
	};

	// Component sending all references to the inputs of given MultiIn object
	template<class Message, class Selector>
	class InputReferencesEnumerator : public Entity< EntityReference< MultiIn<Message, Selector> > >,
									  public Entity< Joint< InputReference<Message>, Selector > >,
									  public Consumer< EntityReference< MultiIn<Message, Selector> > >,
									  public Producer< Joint< InputReference<Message>, Selector > >
	{
	protected:
		TriggeringInputGate< EntityReference< MultiIn<Message, Selector> > >	input;
		DirectOutputGate< Joint< InputReference<Message>, Selector > >			output;
	public:
		InputReferencesEnumerator();
		virtual ~InputReferencesEnumerator();
	protected:
		virtual Entity< EntityReference< MultiIn<Message, Selector> > >& GetMessage(
			InputGate< EntityReference< MultiIn<Message, Selector> > > & source_address, 
			const EntityReference< MultiIn<Message, Selector> > message);
		virtual Entity< Joint< InputReference<Message>, Selector > >& SendMessage(
			OutputGate< Joint< InputReference<Message>, Selector > >& distribution_address, 
			const Joint< InputReference<Message>, Selector > message);
		virtual void Lock() {Entity< EntityReference< MultiIn<Message, Selector> > >::Lock(); 
							 Entity< Joint< InputReference<Message>, Selector > >::Lock();};
		virtual void Unlock() {Entity< EntityReference< MultiIn<Message, Selector> > >::Unlock(); 
						  	   Entity< Joint< InputReference<Message>, Selector > >::Unlock();};
	private:
		static void GatesEnumerate(Selector, void*);
		struct EnumArg
		{
			 MultiIn<Message, Selector> * source_object;
			InputReferencesEnumerator* this_ptr;
		};
	};

	// Component sending all references to the outputs of given MultiOut object
	template<class Message, class Selector>
	class OutputReferencesEnumerator : public Entity< EntityReference< MultiOut<Message, Selector> > >,
									  public Entity< Joint< OutputReference<Message>, Selector > >,
									  public Consumer< EntityReference< MultiOut<Message, Selector> > >,
									  public Producer< Joint< OutputReference<Message>, Selector > >
	{
	protected:
		TriggeringInputGate< EntityReference< MultiOut<Message, Selector> > >						input;
		DirectOutputGate< Joint< OutputReference<Message>, Selector > >		                        output;
	public:
		OutputReferencesEnumerator();
		virtual ~OutputReferencesEnumerator();
	protected:
		virtual Entity< EntityReference< MultiOut<Message, Selector> > >& GetMessage(
			InputGate< EntityReference< MultiOut<Message, Selector> > > & source_address, 
			const EntityReference< MultiOut<Message, Selector> > message);
		virtual Entity< Joint< OutputReference<Message>, Selector > >& SendMessage(
			OutputGate< Joint< OutputReference<Message>, Selector > >& distribution_address, 
			const Joint< OutputReference<Message>, Selector > message);
		virtual void Lock() {Entity< EntityReference< MultiOut<Message, Selector> > >::Lock(); 
							 Entity< Joint< OutputReference<Message>, Selector > >::Lock();};
		virtual void Unlock() {Entity< EntityReference< MultiOut<Message, Selector> > >::Unlock(); 
						  	   Entity< Joint< OutputReference<Message>, Selector > >::Unlock();};
	private:
		static void GatesEnumerate(Selector, void*);
		struct EnumArg
		{
			 MultiOut<Message, Selector> * source_object;
			OutputReferencesEnumerator* this_ptr;
		};
	};

	// Reference to the given Consumer object
	template<class Message>
	class ConsumerReference : public EntityReference< Consumer<Message> > 
	{
	public:
		ConsumerReference(Consumer<Message>& entity) : EntityReference<Message>(entity) {};
		virtual ~ConsumerReference() {};
	};

	// Reference to the given Producer object
	template<class Message>
	class ProducerReference : public EntityReference< Producer<Message> > 
	{
	public:
		ProducerReference(Producer<Message>& entity) : EntityReference<Message>(entity) {};
		virtual ~ProducerReference() {};
	};

	// Provider for Consumer reference
	template<class Type>
	class ConsumerReferenceProvider : public EntityReferenceProvider< Consumer<Type> > 
	{
	public:
		ConsumerReferenceProvider(Consumer<Type>& ref) : EntityReferenceProvider<Type>(ref) {};
		virtual ~ConsumerReferenceProvider() {};
	};

	// Provider for Producer reference
	template<class Type>
	class ProducerReferenceProvider : public EntityReferenceProvider< Producer<Type> > 
	{
	public:
		ProducerReferenceProvider(Producer<Type>& ref) : EntityReferenceProvider<Type>(ref) {};
		virtual ~ProducerReferenceProvider() {};
	};

	// Reference to the given MultiIn object
	template<class Message, class Selector>
	class MultiInReference : public EntityReference< MultiIn<Message, Selector> > 
	{
	public:
		MultiInReference(MultiIn<Message, Selector>& entity) : EntityReference<Message>(entity) {};
		virtual ~MultiInReference() {};
	};

	// Reference to the given MultiOut object
	template<class Message, class Selector>
	class MultiOutReference : public EntityReference< MultiOut<Message, Selector> > 
	{
	public:
		MultiOutReference(MultiOut<Message, Selector>& entity) : EntityReference<Message>(entity) {};
		virtual ~MultiOutReference() {};
	};

	// Provider for MultiIn reference
	template<class Message, class Selector>
	class MultiInReferenceProvider : public EntityReferenceProvider< MultiIn<Message, Selector> > 
	{
	public:
		MultiInReferenceProvider(MultiIn<Message, Selector>& ref) : EntityReferenceProvider<Message>(ref) {};
		virtual ~MultiInReferenceProvider() {};
	};

	// Provider for MultiOut reference
	template<class Message, class Selector>
	class MultiOutReferenceProvider : public EntityReferenceProvider< MultiOut<Message, Selector> > 
	{
	public:
		MultiOutReferenceProvider(MultiOut<Message, Selector>& ref) : EntityReferenceProvider<Message>(ref) {};
		virtual ~MultiOutReferenceProvider() {};
	};
};
	
};

/**********************************************************************************************************/
/************************************** I M P L E M E N T A T I O N ***************************************/
/**********************************************************************************************************/

using namespace CSTMiddleWare::StandardComponents;

template<class Type>
Comparison<Type>::Comparison(ComparisonType _type) : type(_type)
{
};

template<class Type>
Comparison<Type>::~Comparison()
{
};

template<class Type>
bool Comparison<Type>::operator()(const Joint<Type, Type> src_msg)
{
	switch (type)
	{
	case EQUAL:
		return src_msg.x == src_msg.y;
	case NOT_EQUAL:
		return src_msg.x != src_msg.y;
	case MORE:
		return src_msg.x > src_msg.y;
	case LESS:
		return src_msg.x < src_msg.y;
	case MORE_OR_EQUAL:
		return src_msg.x >= src_msg.y;
	case LESS_OR_EQUAL:
		return src_msg.x <= src_msg.y;
	default:
		throw int(0);
	};
};

template<class Message, class Selector>
MultiSelector<Message, Selector>::MultiSelector() : input(*this), Consumer< Joint<Message, Selector> >(input)
{
};

template<class Message, class Selector>
MultiSelector<Message, Selector>::~MultiSelector()
{
	for (typename std::map<Selector, OutputGate<Message>*>::iterator i = this->out_list.begin();
		 i != this->out_list.end(); i++)
		 delete i->second;
};

template<class Message, class Selector>
OutputGate<Message>*& MultiSelector<Message, Selector>::operator [](Selector s)
{
	OutputGate<Message>*& ready_gate = MultiOut<Message, Selector>::operator [](s);
	if (ready_gate == NULL)
	{
		MultiOut<Message, Selector>::operator [](s) = new DirectOutputGate<Message>(*this);
		return MultiOut<Message, Selector>::operator [](s);
	}
	else
		return ready_gate;
};

template<class Message, class Selector>
Entity< Joint<Message, Selector> >& MultiSelector<Message, Selector>::GetMessage(
	InputGate< Joint<Message, Selector> >& source_address, const Joint<Message, Selector> message)
{
	if (&source_address != &this->GetInput())
		throw int(0);
	GetOutputGate(message.y)->operator()(message.x);
	return *this;
};

template<class Message, class Selector>
Entity<Message>& MultiSelector<Message, Selector>::SendMessage(
	OutputGate<Message>& distribution_address, const Message message)
{
	distribution_address.operator()(message);
	return *this;
};

template<class Message>
Duplicate<Message>::Duplicate()
{
};

template<class Message>
Duplicate<Message>::~Duplicate()
{
};

template<class Message>
Joint<Message, Message> Duplicate<Message>::operator ()(const Message src_msg)
{
	Joint<Message, Message> pair;
	pair.x = src_msg;
	pair.y = src_msg;
	return pair;
};

template<class Message>
InputReferenceProvider<Message>::InputReferenceProvider()
{
};

template<class Message>
InputReferenceProvider<Message>::~InputReferenceProvider()
{
};

template<class Message>
InputReference<Message> InputReferenceProvider<Message>::operator ()(const EntityReference< Consumer<Message> > src)
{
	return InputReference<Message>(EntityReference< Consumer<Message> >(src).operator  Consumer<Message> &().GetInput());
};

template<class Message>
OutputReferenceProvider<Message>::OutputReferenceProvider()
{
};

template<class Message>
OutputReferenceProvider<Message>::~OutputReferenceProvider()
{
};

template<class Message>
OutputReference<Message> OutputReferenceProvider<Message>::operator ()(const EntityReference< Producer<Message> > src)
{
	return OutputReference<Message>(EntityReference< Producer<Message> >(src).operator  Producer<Message> &().GetOutput());
};

template<class Message>
DynamicLink<Message>::DynamicLink()
{
};

template<class Message>
DynamicLink<Message>::~DynamicLink()
{
};

template<class Message>
void DynamicLink<Message>::Run(const Joint< InputReference<Message>, OutputReference<Message> > msg)
{
	InputReference<Message> input   = msg.x;
	OutputReference<Message> output = msg.y;

	output.operator OutputGate<Message> &().Link(input.operator InputGate<Message> &());
};

template<class Message, class Selector>
InputReferencesEnumerator<Message, Selector>::InputReferencesEnumerator() : 
									  input(*this),
									  output(*this),
									  Consumer< EntityReference< MultiIn<Message, Selector> > >(input),
									  Producer< Joint< InputReference<Message>, Selector > >(output)
{
}

template<class Message, class Selector>
InputReferencesEnumerator<Message, Selector>::~InputReferencesEnumerator()
{
};

template<class Message, class Selector>
void InputReferencesEnumerator<Message, Selector>::GatesEnumerate(Selector s, void* arg)
{
	EnumArg* arg_ptr = 
		reinterpret_cast< EnumArg* >(arg);
	Joint< InputReference<Message>, Selector > result;
	InputReference<Message> ref = *arg_ptr->source_object->GetInputGate(s);
	result.x = ref;
	result.y = s;
	arg_ptr->this_ptr->SendMessage(arg_ptr->this_ptr->GetOutput(), result);
};

template<class Message, class Selector>
Entity< EntityReference< MultiIn<Message, Selector> > >& InputReferencesEnumerator<Message, Selector>::GetMessage(
	InputGate< EntityReference< MultiIn<Message, Selector> > > & source_address, 
	const EntityReference< MultiIn<Message, Selector> > message)
{
	EnumArg ea;
	ea.source_object = &EntityReference< MultiIn<Message, Selector> >(message).operator  MultiIn<Message, Selector> &();
	ea.this_ptr = this;
	ea.source_object->Enumerate(&GatesEnumerate, &ea);
	return *this;
};

template<class Message, class Selector>
Entity< Joint< InputReference<Message>, Selector > >& InputReferencesEnumerator<Message, Selector>::SendMessage(
	OutputGate< Joint< InputReference<Message>, Selector > >& distribution_address, 
	const Joint< InputReference<Message>, Selector > message)
{
	if (&distribution_address != &this->GetOutput())
		throw int(0);
	distribution_address.operator()(message);
	return *this;
};

template<class Message, class Selector>
OutputReferencesEnumerator<Message, Selector>::OutputReferencesEnumerator() : 
									  input(*this),
									  output(*this),
									  Consumer< EntityReference< MultiOut<Message, Selector> > >(input),
									  Producer< Joint< OutputReference<Message>, Selector > >(output)
{
}

template<class Message, class Selector>
OutputReferencesEnumerator<Message, Selector>::~OutputReferencesEnumerator()
{
};

template<class Message, class Selector>
void OutputReferencesEnumerator<Message, Selector>::GatesEnumerate(Selector s, void* arg)
{
	EnumArg* arg_ptr = 
		reinterpret_cast< EnumArg* >(arg);
	Joint< OutputReference<Message>, Selector > result;
	OutputReference<Message> ref = *arg_ptr->source_object->GetOutputGate(s);
	result.x = ref;
	result.y = s;
	arg_ptr->this_ptr->SendMessage(arg_ptr->this_ptr->GetOutput(), result);
};

template<class Message, class Selector>
Entity< EntityReference< MultiOut<Message, Selector> > >& OutputReferencesEnumerator<Message, Selector>::GetMessage(
	InputGate< EntityReference< MultiOut<Message, Selector> > > & source_address, 
	const EntityReference< MultiOut<Message, Selector> > message)
{
	EnumArg ea;
	ea.source_object = &EntityReference< MultiOut<Message, Selector> >(message).operator  MultiOut<Message, Selector> &();
	ea.this_ptr = this;
	ea.source_object->Enumerate(&GatesEnumerate, &ea);
	return *this;
};

template<class Message, class Selector>
Entity< Joint< OutputReference<Message>, Selector > >& OutputReferencesEnumerator<Message, Selector>::SendMessage(
	OutputGate< Joint< OutputReference<Message>, Selector > >& distribution_address, 
	const Joint< OutputReference<Message>, Selector > message)
{
	if (&distribution_address != &this->GetOutput())
		throw int(0);
	distribution_address.operator()(message);
	return *this;
};

#endif
