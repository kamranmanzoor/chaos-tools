/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* Does encryption and decryption of Manifest + Content records              */
/* shall not be used externally without any real necessity                   */
/*****************************************************************************/
#include "RecordCrypt.h"
#include "records.h"
#include "Base64.h"
#include "RSACrypt.h"

using namespace Componentality::Crypto;
using namespace Componentality::LBS;
using namespace Componentality::Simplicity::Configuration;

const std::string Componentality::LBS::ARG_SIGNATURE	= "sgn"; // Manifest key for signature

RecordCrypt::RecordCrypt(const std::string id, const std::string decryption_key, const std::string signature_key) : mSymmetricCryptor(NULL), 
																													mID(id)
{
	mLocalSignatureKey = signature_key;
	mLocalDecryptionKey = decryption_key;
};

RecordCrypt::~RecordCrypt()
{
	if (mSymmetricCryptor)
		delete mSymmetricCryptor;
};

std::list<std::string> RecordCrypt::listRemoteSides()
{
	std::list<std::string> result;
	for (std::map<std::string, std::string>::iterator iterator = mFingerprints.begin(); iterator != mFingerprints.end(); iterator++)
		result.push_back(iterator->first);
	return result;
}

void RecordCrypt::addRemoteSide(const std::string& id, const std::string& encryption_key, const std::string& signature_key)
{
	mFingerprints[id] = encryption_key;
	mRemoteSignatureKeys[id] = signature_key;
	mKeyList.addKey(encryption_key);
	updateCache();
};

void RecordCrypt::deleteRemoteSide(const std::string& id)
{
	std::map<std::string, std::string>::iterator iterator = mFingerprints.find(id);
	if (iterator != mFingerprints.end())
	{
		mKeyList.removeKey(iterator->second);
		mFingerprints.erase(iterator);
		iterator = mRemoteSignatureKeys.find(id);
		if (iterator != mRemoteSignatureKeys.end())
			mRemoteSignatureKeys.erase(iterator);
	};
};

void RecordCrypt::setup(const std::string& decryption_key, const std::string& signature_key)
{
	mLocalSignatureKey = signature_key;
	mLocalDecryptionKey = decryption_key;
};

RecordCrypt::KEYS_TO_REMOTE RecordCrypt::setup()
{
	KEYS_TO_REMOTE result;
	KEYPAIR encoding_keys = createKeyPair();
	KEYPAIR signature_keys = createKeyPair();
	result.mEncryptionKey = encoding_keys.mEncryptionKey;
	mLocalDecryptionKey = encoding_keys.mDecryptionKey;
	result.mSignatureKey = signature_keys.mEncryptionKey;
	mLocalSignatureKey = signature_keys.mDecryptionKey;
	return result;
};

// To speed up encryption, master key is pre-crypted with public keys of all remote sides
// and applied without consequent encryptions
void RecordCrypt::updateCache()
{
	if (!mSymmetricCryptor)
		mSymmetricCryptor = new AES256();
	if (mSymmetricKey.empty())
		mSymmetricKey = mSymmetricCryptor->getKey();
	std::map<std::string, std::string> cache = mKeyList.encrypt(mSymmetricKey);
	mKeyCache = cache;
};

void RecordCrypt::reset()
{
	if (mSymmetricCryptor)
		delete mSymmetricCryptor;
	mSymmetricCryptor = NULL;
	mSymmetricKey.clear();
	updateCache();
};

std::string RecordCrypt::encryptRecord(const std::string decrypted_record)
{
	std::pair< std::string, std::string > spl = __split_nn(decrypted_record);
	std::string header = spl.first;
	std::string content = spl.second;
	if (!mSymmetricCryptor)
		updateCache();
	// Encrypt content with AES256
	content = mSymmetricCryptor->encrypt(content);
	// Store all pre-encrypted values for master key
	for (std::map<std::string, std::string>::iterator i = mFingerprints.begin(); i != mFingerprints.end(); i++)
		header += std::string("\n") + i->first + '=' + BASE64_encode(mKeyCache[i->second]);
	std::string checksum = __find(header, ARG_CHECKSUM);
	if (mLocalSignatureKey.empty() || checksum.empty())
		return std::string();
	// Sign the checksum (which must be already there)
	std::string signature = ::Crypto::RSA::Sign(checksum, mLocalSignatureKey);
	// And store signature
	header += std::string("\n") + ARG_SIGNATURE + '=' + BASE64_encode(signature);
	return header + "\n\n" + content;
};

std::string RecordCrypt::decryptRecord(const std::string encrypted_record)
{
	std::pair< std::string, std::string > spl = __split_nn(encrypted_record);
	std::string header = spl.first;
	std::string content = spl.second;

	// Retrieve encrypted master key for given ID
	std::string key = __find(header, mID);
	if (key.empty())
		throw std::string("No decryption key for this sender");
	else
		key = BASE64_decode(key);
	// Get sender's ID
	std::string sender = __find(header, ARG_VEHICLE_ID);
	if (sender.empty())
		throw std::string("No sender field in the record");
	// Retrieve signature key for this sender (must be available)
	std::string signKey = mRemoteSignatureKeys[sender];
	if (signKey.empty())
		throw std::string("No signature key for this sender");
	// Decode symmetric key
	key = mKeyList.decrypt(key, mLocalDecryptionKey);

	// Retrieve signature
	std::string signature = BASE64_decode(__find(header, ARG_SIGNATURE));

	// Decode content
	content = AES256(key).decrypt(content);
	std::string checksum = __find(header, ARG_CHECKSUM);
	// Verify signature
	bool valid = ::Crypto::RSA::Verify(checksum, signature, signKey);

	return valid ? content : std::string();
};

std::string RecordCrypt::KEYS_TO_REMOTE::serialize()
{
	return BASE64_encode(mEncryptionKey) + '\n' + BASE64_encode(mSignatureKey);
};

bool RecordCrypt::KEYS_TO_REMOTE::deserialize(const std::string& str)
{
	size_t pos = str.find('\n');
	if (pos == (size_t) -1)
		return false;
	mEncryptionKey = BASE64_decode(str.substr(0, pos));
	mSignatureKey = BASE64_decode(str.substr(pos + 1));
	return true;
};

std::string RecordCrypt::serialize()
{
	std::string result = mID + "\n\n";

	for (std::map<std::string, std::string>::iterator i = mFingerprints.begin(); i != mFingerprints.end(); i++)
	{
		result += BASE64_encode(i->first) + '\n' + BASE64_encode(i->second) + '\n';
	};

	result += "\n";

	for (std::map<std::string, std::string>::iterator i = mRemoteSignatureKeys.begin(); i != mRemoteSignatureKeys.end(); i++)
	{
		result += BASE64_encode(i->first) + '\n' + BASE64_encode(i->second) + '\n';
	};

	result += '\n';

	result += BASE64_encode(mLocalSignatureKey) + '\n';
	result += BASE64_encode(mLocalDecryptionKey) + '\n';

	return result;
};

std::pair<std::string, std::string> __getline(const std::string s)
{
	std::pair<std::string, std::string> result;
	size_t pos = s.find('\n');
	if (pos == (size_t) -1)
		return result;
	result.first = s.substr(0, pos);
	result.second = s.substr(pos + 1);
	return result;
};

bool __checkpair(const std::pair<std::string, std::string> pair)
{
	return pair.first.empty() && pair.second.empty();
};

bool RecordCrypt::deserialize(const std::string& str)
{
	std::pair<std::string, std::string> pair = __split_nn(str);

	mID = pair.first;
	std::string s = pair.second;
	if (s.empty())
		return false;
	std::string x, y;
	mFingerprints.clear();
	mKeyList.clear();
	while (s[0] != '\n')
	{
		pair = __getline(s);
		if (__checkpair(pair))
			return false;
		s = pair.second;
		x = pair.first;
		pair = __getline(s);
		if (__checkpair(pair))
			return false;
		s = pair.second;
		y = pair.first;
		mFingerprints[BASE64_decode(x)] = BASE64_decode(y);
		mKeyList.addKey(BASE64_decode(y));
	};
	s = s.substr(1);
	mRemoteSignatureKeys.clear();
	while (s[0] != '\n')
	{
		pair = __getline(s);
		if (__checkpair(pair))
			return false;
		s = pair.second;
		x = pair.first;
		pair = __getline(s);
		if (__checkpair(pair))
			return false;
		s = pair.second;
		y = pair.first;
		mRemoteSignatureKeys[BASE64_decode(x)] = BASE64_decode(y);
	};
	s = s.substr(1);
	pair = __getline(s);
	mLocalSignatureKey = BASE64_decode(pair.first);
	mLocalDecryptionKey = BASE64_decode(pair.second);
	reset();
	return true;
};

RecordCrypt::KEYS_TO_REMOTE RecordCrypt::getKeys(const std::string& id)
{
	KEYS_TO_REMOTE ktr;
	ktr.mEncryptionKey = this->mFingerprints[id];
	ktr.mSignatureKey = this->mRemoteSignatureKeys[id];
	return ktr;
}