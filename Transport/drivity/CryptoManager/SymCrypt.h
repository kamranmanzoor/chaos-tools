/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* Symmetric encryption adapter. Very thin and easy                          */
/* The only reason of this module is to hide CSTMiddleWare classes, which    */
/* are using rather special programming style and API                        */
/*****************************************************************************/
#ifndef SYMCRYPT_H
#define SYMCRYPT_H

#include <string>

namespace Componentality
{

	namespace Crypto
	{

		class AES256
		{
		protected:
			std::string mKey;
		public:
			AES256(const std::string = "");
			virtual ~AES256();
		public:
			virtual void setKey(const std::string);
			virtual std::string getKey();
			virtual void renewKey();
		public:
			virtual std::string encrypt(const std::string);
			virtual std::string decrypt(const std::string);
		};

	}; // namespace Crypto

}; // namespace Componentality

#endif