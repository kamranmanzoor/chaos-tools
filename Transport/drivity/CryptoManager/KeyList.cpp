/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* List of registered public keys. This class is a little bit redundant, but */
/* still widely used for encryption                                          */
/*****************************************************************************/

#include "KeyList.h"
#include "RSACrypt.h"

using namespace Componentality::Crypto;
using namespace ::Crypto::RSA;

KeyList::KeyList()
{
};

KeyList::~KeyList()
{
};

void KeyList::addKey(const std::string& key)
{
	mKeyList[key] = key;
};

void KeyList::removeKey(const std::string& key)
{
	if (mKeyList.find(key) != mKeyList.end())
		mKeyList.erase(mKeyList.find(key));
};

std::string KeyList::Serialize()
{
	std::string result;

	for (std::map<std::string, std::string>::iterator i = mKeyList.begin(); i != mKeyList.end(); i++)
	{
		if (!result.empty())
			result += '\n';
		result += i->first;
	};

	return result;
};

void KeyList::Deserialize(const std::string& str)
{
	std::string remain = str;
	while (!remain.empty())
	{
		size_t pos = remain.find('\n');
		std::string key;
		if (pos != (size_t) -1)
		{
			key = remain.substr(0, pos);
			remain = remain.substr(pos + 1);
		}
		else
		{
			key = remain;
			remain.clear();
		};
		addKey(key);
	};
};

std::map<std::string, std::string> KeyList::encrypt(const std::string& decrypted_content)
{
	std::map<std::string, std::string> result;
	CSTMiddleWare::StandardComponents::StringToBlock stb;
	::Encryptor ec;
	CSTMiddleWare::StandardComponents::BlockToString bts;
	CSTMiddleWare::StandardComponents::BlockUtilizer bu;
	CSTMiddleWare::StandardComponents::Cell<std::string> receiver; 
	stb.GetOutput().Link(ec.GetDataInput());
	stb.GetOutput().Link(bu.GetInput());
	ec.GetOutput().Link(bts.GetInput());
	ec.GetOutput().Link(bu.GetInput());
	bts.GetOutput().Link(receiver.GetDataInput());
	CSTMiddleWare::StandardComponents::StringToBlock key_stb;
	key_stb.GetOutput().Link(ec.GetKeyInput());
	CSTMiddleWare::StandardComponents::Cell<DataBlock> db;
	key_stb.GetOutput().Link(db.GetDataInput());
	for (std::map<std::string, std::string>::iterator i = mKeyList.begin(); i != mKeyList.end(); i++)
	{
		key_stb.GetInput().operator()(i->first);
		stb.GetInput().operator()(decrypted_content);
		bu.GetInput().operator()(db.GetValue());
		result[i->first] = receiver.GetValue();
	};
	return result;
};

std::string KeyList::encrypt(const std::string& decrypted_content, const std::string& pkey)
{
	std::string result;
	CSTMiddleWare::StandardComponents::StringToBlock stb;
	::Encryptor ec;
	CSTMiddleWare::StandardComponents::BlockToString bts;
	CSTMiddleWare::StandardComponents::BlockUtilizer bu;
	CSTMiddleWare::StandardComponents::Cell<std::string> receiver; 
	stb.GetOutput().Link(ec.GetDataInput());
	stb.GetOutput().Link(bu.GetInput());
	ec.GetOutput().Link(bts.GetInput());
	ec.GetOutput().Link(bu.GetInput());
	bts.GetOutput().Link(receiver.GetDataInput());
	CSTMiddleWare::StandardComponents::StringToBlock key_stb;
	key_stb.GetOutput().Link(ec.GetKeyInput());
	CSTMiddleWare::StandardComponents::Cell<DataBlock> db;
	key_stb.GetOutput().Link(db.GetDataInput());
	key_stb.GetInput().operator()(pkey);
	stb.GetInput().operator()(decrypted_content);
	bu.GetInput().operator()(db.GetValue());
	result = receiver.GetValue();
	return result;
};

std::string KeyList::decrypt(const std::string& encrypted_content, const std::string& secure_key)
{
	::Decryptor dc;
	CSTMiddleWare::StandardComponents::StringToBlock stb;
	CSTMiddleWare::StandardComponents::BlockToString bts;
	CSTMiddleWare::StandardComponents::BlockUtilizer bu;
	CSTMiddleWare::StandardComponents::Cell<std::string> receiver; 
	stb.GetOutput().Link(dc.GetDataInput());
	stb.GetOutput().Link(bu.GetInput());
	dc.GetOutput().Link(bts.GetInput());
	dc.GetOutput().Link(bu.GetInput());
	bts.GetOutput().Link(receiver.GetDataInput());
	CSTMiddleWare::StandardComponents::StringToBlock key_stb;
	key_stb.GetOutput().Link(dc.GetKeyInput());
	CSTMiddleWare::StandardComponents::Cell<DataBlock> db;
	key_stb.GetOutput().Link(db.GetDataInput());

	key_stb.GetInput().operator()(secure_key);
	stb.GetInput().operator()(encrypted_content);
	bu.GetInput().operator()(db.GetValue());

	std::string s = receiver.GetValue();
	return s;
};

KEYPAIR Componentality::Crypto::createKeyPair()
{
	::KeyGen kg;
	CSTMiddleWare::StandardComponents::Cell<KeyPair> receiver;
	kg.GetOutput().Link(receiver.GetDataInput());
	CSTMiddleWare::StandardComponents::BlockUtilizer bu;
	CSTMiddleWare::StandardComponents::Cell<std::string> strholder;
	CSTMiddleWare::StandardComponents::BlockToString bts;
	bts.GetOutput().Link(strholder.GetDataInput());
	kg.GetInput().operator()(SIGNAL);
	KEYPAIR result;
	bts.GetInput().operator()(receiver.GetValue().x);
	result.mDecryptionKey = strholder.GetValue();
	bts.GetInput().operator()(receiver.GetValue().y);
	result.mEncryptionKey = strholder.GetValue();
	bu.GetInput().operator()(receiver.GetValue().x);
	bu.GetInput().operator()(receiver.GetValue().y);
	return result;
};
