/*****************************************************************************/
/* Componentality NMEA parser             * (C) Componentality Oy, 2011-2013 */
/*****************************************************************************/

#include "nmeaparser.h"

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <set>
#include <map>
#include <exception>
#include "nmeastatemachine.h"

using namespace Componentality;
using namespace Componentality::NMEA;


//////////////////////////////////////////////////////////////////
//   Parser implementation
//////////////////////////////////////////////////////////////////

Parser::Parser()
{
	// Init TalkersIDs
	Talkers["AG"] = "Autopilot - General";
	Talkers["AP"] = "Autopilot - Magnetic";
	Talkers["CD"] = "Communications � Digital Selective Calling (DSC)";
	Talkers["CR"] = "Communications � Receiver / Beacon Receiver";
	Talkers["CS"] = "Communications � Satellite";
	Talkers["CT"] = "Communications � Radio-Telephone (MF/HF)";
	Talkers["CV"] = "Communications � Radio-Telephone (VHF)";
	Talkers["CX"] = "Communications � Scanning Receiver";
	Talkers["DF"] = "Direction Finder";
	Talkers["EC"] = "Electronic Chart Display & Information System (ECDIS)";
	Talkers["EP"] = "Emergency Position Indicating Beacon (EPIRB)";
	Talkers["ER"] = "Engine Room Monitoring Systems";
	Talkers["GP"] = "Global Positioning System (GPS)";
	Talkers["HC"] = "Heading � Magnetic Compass";
	Talkers["HE"] = "Heading � North Seeking Gyro";
	Talkers["HN"] = "Heading � Non North Seeking Gyro";
	Talkers["II"] = "Integrated Instrumentation";
	Talkers["IN"] = "Integrated Navigation";
	Talkers["LC"] = "Loran C";
	Talkers["RA"] = "RADAR and/or ARPA";
	Talkers["SD"] = "Sounder, Depth";
	Talkers["SN"] = "Electronic Positioning System, other/general";
	Talkers["SS"] = "Sounder, Scanning";
	Talkers["TI"] = "Turn Rate Indicator";
	Talkers["VD"] = "Velocity Sensor, Doppler, other/general";
	Talkers["DM"] = "Velocity Sensor, Speed Log, Water, Magnetic";
	Talkers["VW"] = "Velocity Sensor, Speed Log, Water, Mechanical";
	Talkers["WI"] = "Weather Instruments";
	Talkers["YX"] = "Transducer";
	Talkers["ZA"] = "Timekeeper � Atomic Clock";
	Talkers["ZC"] = "Timekeeper � Chronometer";
	Talkers["ZQ"] = "Timekeeper � Quartz";
	Talkers["ZV"] = "Timekeeper � Radio Update, WWV or WWVH";

	// Init Sentencies
	Sentencies["GSA"] = "a,a,x,x,x,x,x,x,x,x,x,x,x,x,x.x,x.x,x.x";
	Sentencies["GSV"] = "x,x,x,(,x,x,x,x,)"; // ... - (4..7)[3]
	Sentencies["RMC"] = "hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,xxxx,x.x,a";
	Sentencies["VTG"] = "x.x,T,x.x,M,x.x,N,x.x,K";
	Sentencies["ZDA"] = "hhmmss.ss,xx,xx,xxxx,xx,xx";
	Sentencies["GGA"] = "hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx";
	Sentencies["GLL"] = "llll.ll,a,yyyyy.yy,a,hhmmss.ss,A";


	// Init param parsers holder
	m_paramParsersHolder.Construct(Sentencies);

	// Init State machine
	m_pStateMachine = new ParserStateMachine(Talkers, Sentencies, &m_paramParsersHolder);

	if (!m_pStateMachine)
	{
		throw "Could not create state machine."; // this is enought reason to die.
	}

}

Parser::~Parser()
{
	delete m_pStateMachine;
}


Error Parser::Parse(Blob& data, Callback* client)
{
	for (size_t i = 0; i < data.mLength; ++i)
	{
		m_pStateMachine->Process(((const char*) data.mData)[i], client);
	}

	if (client)
    		client->onBlobProcessed(data);

	return ERROR_NONE;
}



