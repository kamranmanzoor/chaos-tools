/*****************************************************************************/
/* Componentality NMEA parser             * (C) Componentality Oy, 2011-2013 */
/*****************************************************************************/
/* Parsers for specific sentences                                            */
/*****************************************************************************/

#ifndef NMEA_API_FIELD_PARSER
#define NMEA_API_FIELD_PARSER

#include "nmeacommon.h"

namespace Componentality
{
	namespace NMEA
	{

		//////////////////////////////////////////////////////////////////
		//   Field parsers interface
		//////////////////////////////////////////////////////////////////
		class ConcreeteFieldParser
		{
		public:
			virtual ~ConcreeteFieldParser(){};
			virtual int Process(const std::string& fparam) = 0;
			virtual bool WasError() = 0;
			virtual void Reset() = 0;
			virtual void Report() = 0;
			virtual size_t WriteData(void* pData) = 0;
		};
		
		//////////////////////////////////////////////////////////////////
		//   Field parsers fabric.
		//////////////////////////////////////////////////////////////////
		ConcreeteFieldParser* GenerateConcreeteFieldParser(const std::string& stTemplate, const std::string& stSubTemplate = "");

	}; // namespace NMEA

}; // namespace Componentality

#endif // NMEA_API_FIELD_PARSER