/*****************************************************************************/
/* Componentality NMEA parser             * (C) Componentality Oy, 2011-2013 */
/*****************************************************************************/

#include "nmeaconcreetefieldparsers.h"

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <set>
#include <map>
#include <exception>
#include <stdlib.h>

using namespace Componentality;
using namespace Componentality::NMEA;

//////////////////////////////////////////////////////////////////
//   Auxillary functions
//////////////////////////////////////////////////////////////////
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss(s);
	std::string item;
	while(std::getline(ss, item, delim)) 
	{
		elems.push_back(item);
	}
	return elems;
}


//////////////////////////////////////////////////////////////////
//   Field parsers
//////////////////////////////////////////////////////////////////

// NOP parser, for do nothing 
class ConcreeteFieldParserEmpty : public ConcreeteFieldParser
{
public:
	ConcreeteFieldParserEmpty():m_bError(false){};
	~ConcreeteFieldParserEmpty(){}
	int Process(const std::string& fparam){return 1;};
	bool WasError(){return m_bError;};
	void Reset(){m_bError = false;};
	void Report(){std::cout << "Empty field parser." << std::endl;};
	size_t WriteData(void* pData)
	{
		return 0;
	};
protected:
	bool m_bError;
};

// "a" field, single char
class ConcreeteFieldParserChar : public ConcreeteFieldParserEmpty
{
public:
	~ConcreeteFieldParserChar(){};
	int Process(const std::string& fparam)
	{
		if (fparam.length() == 0)
		{
			m_char = ' ';
			return 1;
		}

		if (fparam.length() == 1)
		{
			m_char = fparam[0];
			return 1;
		}

		m_bError = true;
		return 1;
	};
	void Report()
	{
		std::cout << "Char field: " << m_char << std::endl;
	};
	size_t WriteData(void* pData)
	{
		if (pData)
		{
			*(reinterpret_cast<char*>(pData)) = m_char;
		}
		return 1; // sizeof char
	};
protected:
	char m_char;
};

// "A" field, flag, equalient to single char
class ConcreeteFieldParserFlagChar : public ConcreeteFieldParserChar
{
public:
	~ConcreeteFieldParserFlagChar(){};

	void Report()
	{
		std::cout << "Flag field: " << m_char << std::endl;
	};
};

// "M" field, magnetic boolean, 
class ConcreeteFieldParserMagnetic : public ConcreeteFieldParserChar
{
public:
	~ConcreeteFieldParserMagnetic(){};

	void Report()
	{
		std::cout << "Magnetic: " << ((m_char == 'T')?"true":"false") << std::endl;
	};
};


// "x.x" field, doubleing point value
class ConcreeteFieldParserdouble : public ConcreeteFieldParserEmpty
{
public:
	~ConcreeteFieldParserdouble(){};
	int Process(const std::string& fparam)
	{
		if (fparam.length() == 0)
		{
			m_data = 0.f;
			return 1;
		}

		m_data = strtod(fparam.c_str(), NULL);  // [TODO] - add conversion error handling (check second ptr)
		return 1;
	};
	void Report()
	{
		std::cout << "double field: " << m_data << std::endl;
	};
	size_t WriteData(void* pData)
	{
		if (pData)
		{
			*(reinterpret_cast<double*>(pData)) = m_data;
		}
		return sizeof(m_data);
	};
protected:
	double m_data;
};


// "x" field, integer
class ConcreeteFieldParserInt : public ConcreeteFieldParserEmpty
{
public:
	~ConcreeteFieldParserInt(){};
	int Process(const std::string& fparam)
	{
		if (fparam.length() == 0)
		{
			m_data = 0;
			return 1;
		}

		m_data = atoi(fparam.c_str());  // [TODO] - add conversion error handling
		return 1;
	};
	void Report()
	{
		std::cout << "Int field: " << m_data << std::endl;
	};
	size_t WriteData(void* pData)
	{
		if (pData)
		{
			*(reinterpret_cast<int*>(pData)) = m_data;
		}
		return sizeof(m_data);
	};
protected:
	int m_data;
};


// "(%,x,x,x,x,) field, defined length repeatedable group
class ConcreeteFieldParserRepeated : public ConcreeteFieldParserEmpty
{
public:
	ConcreeteFieldParserRepeated(const std::string& groupTemplate):stGroupTemplate(groupTemplate)
	{
		std::vector<std::string> splittedParams;
		
		split(groupTemplate, ',', splittedParams);

		for (size_t i = 0; i  < splittedParams.size(); ++i)
		{
			m_commandStack.push_back(GenerateConcreeteFieldParser(splittedParams[i]));
		}
		
		Reset();
	};

	~ConcreeteFieldParserRepeated()
	{
		for (size_t i = 0; i < m_commandStack.size(); ++i)
		{
			delete m_commandStack[i];
		}
	}

	int Process(const std::string& fparam)
	{
		// read group count for first call
		if (m_iIterationCounter + m_iIterationCount + m_iCurrentCmd == 0)
		{
			m_iIterationCount = atoi(fparam.c_str());

			if (m_iIterationCount < 0)
			{
				m_bError = true;
				return 1;
			}

			return 0;
		}

		m_iCurrentCmd++;
		m_iIterationCounter += m_iCurrentCmd / m_commandStack.size();
		m_iCurrentCmd %= m_commandStack.size();

		m_commandStack[m_iCurrentCmd]->Process(fparam);

		// check end
		if (m_iIterationCounter == m_iIterationCount /*last iteration*/ &&
			m_iCurrentCmd == m_commandStack.size() - 1 /*last command in current group*/)
			return 1;

		return 0;
	};

	void Reset()
	{
		ConcreeteFieldParserEmpty::Reset();
		m_iIterationCounter = 0;
		m_iIterationCount = 0;
		m_iCurrentCmd = 0;
	}
	void Report()
	{
		std::cout << "	: ";
		m_commandStack[m_iCurrentCmd]->Report();
	};
	size_t WriteData(void* pData)
	{
		if (m_iCurrentCmd != 0)
			return 0;

		size_t offset = 0;
		if (pData)
		{

			for(size_t i = 0; i < m_commandStack.size(); ++i)
			{
				offset += m_commandStack[i]->WriteData((void*)(reinterpret_cast<char*>(pData) + offset));
			}

		}
		return offset;
	};
protected:

	std::string stGroupTemplate;

	std::vector<ConcreeteFieldParser*> m_commandStack;

	int m_iCurrentCmd;
	int m_iIterationCounter;
	int m_iIterationCount;
};


// "(,x,x,x,x,) field, defined rolled group
class ConcreeteFieldParserRepeateRolled : public ConcreeteFieldParserEmpty
{
public:
	ConcreeteFieldParserRepeateRolled(const std::string& groupTemplate):stGroupTemplate(groupTemplate)
	{
		std::vector<std::string> splittedParams;
		
		split(groupTemplate, ',', splittedParams);

		for (size_t i = 0; i  < splittedParams.size(); ++i)
		{
			m_commandStack.push_back(GenerateConcreeteFieldParser(splittedParams[i]));
		}
		
		Reset();
	};

	~ConcreeteFieldParserRepeateRolled()
	{
		for (size_t i = 0; i < m_commandStack.size(); ++i)
		{
			delete m_commandStack[i];
		}
	}

	int Process(const std::string& fparam)
	{
		m_commandStack[m_iCurrentCmd]->Process(fparam);

		m_iCurrentCmd++;
		m_iCurrentCmd %= m_commandStack.size();

		return 0;
	};

	void Reset()
	{
		ConcreeteFieldParserEmpty::Reset();
		m_iIterationCount = 0;
		m_iCurrentCmd = 0;
	}

	void Report()
	{
		std::cout << "	: ";
		m_commandStack[m_iCurrentCmd]->Report();
	};

	size_t WriteData(void* pData)
	{
		if (m_iCurrentCmd != 0)
			return 0; // no reasons to get uncomplete data

		size_t offset = 0;
		if (pData)
		{

			for(size_t i = 0; i < m_commandStack.size(); ++i)
			{
				offset += m_commandStack[i]->WriteData((void*)(reinterpret_cast<char*>(pData) + offset));
			}
		}
		return offset;
	};
protected:

	std::string stGroupTemplate;

	std::vector<ConcreeteFieldParser*> m_commandStack;

	int m_iCurrentCmd;
	int m_iIterationCount;
};

// "hhmmss.ss field, defined UTC time 
class ConcreeteFieldParserTime : public ConcreeteFieldParserEmpty
{
public:
	ConcreeteFieldParserTime()
	{
		Reset();
	};

	~ConcreeteFieldParserTime(){}

	int Process(const std::string& fparam)
	{
		if (fparam.length() < 6) // minimal time record can be parsed
		{
			// empty

			return 1;
		}

		std::string m_stbuf;
		m_stbuf.append(&fparam[0],2);
		m_hh = atoi(m_stbuf.c_str());
		m_stbuf.assign(&fparam[2],2);
		m_mm = atoi(m_stbuf.c_str());
		m_stbuf.assign(&fparam[4],2);
		m_ss = atoi(m_stbuf.c_str());
		if (fparam.length() > 9)
		{
			m_stbuf.assign(&fparam[7],3);
			m_mss = atoi(m_stbuf.c_str());
		}
		else
			m_mss = 0;

		return 1;
	};

	void Reset()
	{
		ConcreeteFieldParserEmpty::Reset();
	}
	void Report()
	{
		std::cout << "Time(UTC): hh=" << m_hh << " mm=" << m_mm << " ss = " << m_ss << " mss=" << m_mss << std::endl;
	};

	size_t WriteData(void* pData)
	{
		if (pData)
		{
			Time tmw;
			tmw.m_hh = m_hh;
			tmw.m_mm = m_mm;
			tmw.m_ss = m_ss;
			tmw.m_mss = m_mss;

			*reinterpret_cast<Time*>(pData) = tmw;

			return sizeof(tmw);
		}
		return 0;
	}

protected:
	int m_hh;
	int m_mm;
	int m_ss;
	int m_mss;
};


// "llll.ll field, defined latitude 
class ConcreeteFieldParserLatitude : public ConcreeteFieldParserEmpty
{
public:
	ConcreeteFieldParserLatitude()
	{
	};

	~ConcreeteFieldParserLatitude(){}

	int Process(const std::string& fparam)
	{
		if (fparam.length() < 4)
		{
			// empty

			return 1;
		}
		std::string m_stbuf;
		m_stbuf.append(&fparam[0],2);
		m_latR = atoi(m_stbuf.c_str());
		m_stbuf.assign(&fparam[2],2);
		m_latM = atoi(m_stbuf.c_str());
		if (fparam.length() >= 6)
		{
			m_stbuf.assign(&fparam[5],2);
			m_latS = atoi(m_stbuf.c_str());
		}
		else
			m_latS = 0;

		return 1;
	};

	void Report()
	{
		std::cout << "Latitude: R=" << m_latR << " M=" << m_latM << " S = " << m_latS << std::endl;
	};

	size_t WriteData(void* pData)
	{
		if (pData)
		{
			double converted = m_latR;
			if (m_latM != 0)
				converted += 1.f/m_latM;
			if (m_latS != 0)
				converted += 1.f / (60 * m_latS);

			*reinterpret_cast<double*>(pData) = converted;

			return sizeof(converted);
		}
		return 0;
	}
protected:
	int m_latR;
	int m_latM;
	int m_latS;
};

// "yyyyy.yy field, defined longitude
class ConcreeteFieldParserLongitude : public ConcreeteFieldParserLatitude
{
public:
	~ConcreeteFieldParserLongitude(){}

	int Process(const std::string& fparam)
	{
		if (fparam.length() < 5)
		{
			// empty

			return 1;
		}
		std::string m_stbuf;
		m_stbuf.append(&fparam[0],3);
		m_latR = atoi(m_stbuf.c_str());
		m_stbuf.assign(&fparam[3],2);
		m_latM = atoi(m_stbuf.c_str());
		if (fparam.length() >= 7)
		{
			m_stbuf.assign(&fparam[6],2);
			m_latS = atoi(m_stbuf.c_str());
		}
		else
			m_latS = 0;

		return 1;
	};

	void Report()
	{
		std::cout << "Longitude: R=" << m_latR << " M=" << m_latM << " S = " << m_latS << std::endl;
	};
};

//////////////////////////////////////////////////////////////////
//   FieldsParserMachine implementation
//////////////////////////////////////////////////////////////////
namespace Componentality
{
	namespace NMEA
	{

		ConcreeteFieldParser* GenerateConcreeteFieldParser(const std::string& stTemplate, const std::string& stSubTemplate /*= ""*/)
		{
			if (stTemplate == "a")
				return new ConcreeteFieldParserChar();
			if (stTemplate == "A")
				return new ConcreeteFieldParserFlagChar();
			if (stTemplate == "M")
				return new ConcreeteFieldParserMagnetic();
			if ((stTemplate == "x") ||
				(stTemplate == "xx") ||
				(stTemplate == "xxxx"))
				return new ConcreeteFieldParserInt();
			/*if (stTemplate == "x")
				return new ConcreeteFieldParserInt();*/
			if (stTemplate == "x.x")
				return new ConcreeteFieldParserdouble();
			if (stTemplate.find("(%") != stTemplate.npos  /*"(x.count"*/)
			{
				return new ConcreeteFieldParserRepeated(stSubTemplate);
			}
			if (stTemplate.find("(") != stTemplate.npos  /*"()*/)
			{
				return new ConcreeteFieldParserRepeateRolled(stSubTemplate);
			}
			if (stTemplate == "hhmmss.ss")
				return new ConcreeteFieldParserTime();
			if (stTemplate == "llll.ll")
				return new ConcreeteFieldParserLatitude();
			if (stTemplate == "yyyyy.yy")
				return new ConcreeteFieldParserLongitude();


		/*	switch (stTemplate)
			{
			case "xxxx":*/
			return new ConcreeteFieldParserEmpty();


			return NULL;
		}
	}
}


