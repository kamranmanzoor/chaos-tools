/*****************************************************************************/
/* Componentality NMEA parser             * (C) Componentality Oy, 2011-2013 */
/*****************************************************************************/
/* Parser API                                                                */
/*****************************************************************************/

#ifndef NMEA_API
#define NMEA_API

#include <list>
#include <vector>
#include <string>
#include <set>
#include <map>
#include "nmeacommon.h"
#include "nmeastatemachine.h"

namespace Componentality
{
	namespace NMEA
	{
		class ParserStateMachine;

		class Parser
		{
		public:
			Parser();
			~Parser();

			Error Parse(Blob& data, Callback* client);			
			//Error Parse(const void* data, size_t length/*, Callback* callback*/);
		protected:
			// first member of the pair is talkerID, second is description from reference manual.
			std::map<std::string, std::string> Talkers;
			// first member of the pair is talkerID, second is description from reference manual.
			std::map<std::string, std::string> Sentencies;
			// Instance of the state machine
			ParserStateMachine* m_pStateMachine;
			// Keeps set of concreete param parsers
			ParamParser m_paramParsersHolder;
		};

	}; // namespace NMEA

}; // namespace Componentality

#endif