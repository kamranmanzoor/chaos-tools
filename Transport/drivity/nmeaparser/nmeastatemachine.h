/*****************************************************************************/
/* Componentality NMEA parser             * (C) Componentality Oy, 2011-2013 */
/*****************************************************************************/
/* State machine                                                             */
/*****************************************************************************/

#ifndef NMEA_API_STATE
#define NMEA_API_STATE

#include "nmeacommon.h"
#include <map>
#include <vector>
#include "nmeaconcreetefieldparsers.h"

namespace Componentality
{
	namespace NMEA
	{
		//////////////////////////////////////////////////////////////////
		//   state machine
		//////////////////////////////////////////////////////////////////
		enum ParserStates
		{
			STATE_READY,
			STATE_ERROR,
			STATE_TALKER_IDENTIFICATION,
			STATE_TALKER_IDENTIFICATION_CUSTOM,
			STATE_SENTENCE_IDENTIFICATION,
			STATE_PARAM_IDENTIFICATION,
			STATE_CHECKSUM_READ,
			STATE_SENTENCE_PARSED
		};

		// Base state
		class ParserStateBase
		{
		public:
			virtual ParserStates Enter(Callback* client){return current;};
			virtual ParserStates Process(const char symbol);
			virtual void Reset(){};
			virtual ParserStates GetCurrent();
			
			// debug method
			virtual std::string Report(){return "";} 
		protected:
			ParserStates current;
		};

		// Parser is ready to process new message
		class ParserStateReady : public ParserStateBase
		{
		public:
			ParserStateReady(){current=STATE_READY;};

			virtual ParserStates Process(const char symbol);
			virtual std::string Report();
		};

		// Error state. This is pass-throw state to mark error occured.
		class ParserStateError : public ParserStateBase
		{
		public:
			ParserStateError(){current=STATE_ERROR;};

			virtual ParserStates Enter(Callback* client);
			virtual ParserStates Process(const char symbol);
			virtual void Reset();
			virtual std::string Report();

		protected:
			std::string stError;
		};

		// State processses identification of the custom manufacture. Tractated as error at this moment.
		// This is pass-throw state to check errror occured.
		class ParserStateTalkerIdentificationCustomVendor : public ParserStateError
		{
		public:
			ParserStateTalkerIdentificationCustomVendor();

			virtual std::string Report();
		};

		// Parser is scanning talker identification
		class ParserStateTalkerIdentification : public ParserStateBase
		{
		public:
			const static size_t MAX_TALKER_LENGTH = 5;  // max length variant - PXXX\0 (for custom manufactures)

			ParserStateTalkerIdentification(std::map<std::string, std::string>& talkers);
			virtual ParserStates Process(const char symbol);
			virtual void Reset();
			virtual std::string Report();

			const std::string& GetTalker(){return talkerID;}
		protected:
			std::string talkerID; 
			std::map<std::string, std::string>& Talkers;
		};

		// Parser is scanning sentence identification
		class ParserStateSentenceIdentification : public ParserStateBase
		{
		public:
			const static size_t MAX_SENTENCE_LENGTH = 3;  // mandatory from standart

			ParserStateSentenceIdentification(std::map<std::string, std::string>& sentencies);
			virtual ParserStates Process(const char symbol);
			virtual void Reset();
			virtual std::string Report();

			const std::string& GetSentence(){return sentenceID;}
		protected:
			std::string sentenceID; 
			std::map<std::string, std::string>& Sentencies;
		};

		class ParamParser;
		class FieldsParserMachine;
		// Parser is scanning param identification
		class ParserStateParamIdentification : public ParserStateBase
		{
		public:
			ParserStateParamIdentification(std::map<std::string, std::string>& sentencies, ParamParser* paramParser);
			virtual ParserStates Process(const char symbol);
			virtual void Reset();
			virtual std::string Report();
			virtual ParserStates Enter(Callback* client);

			const std::string& GetLastParam(){return lastParam;}
			void SetCurrentSentence(const std::string& sentence);
		protected:
			std::string lastParam; 
			std::map<std::string, std::string>& Sentencies;
			ParamParser* m_paramParser;
			std::string currrentSentence;
			FieldsParserMachine* m_pCurrentProcessor;
			Callback* m_client;
		};


		//////////////////////////////////////////////////////////////////
		//   state machine
		//////////////////////////////////////////////////////////////////
		class ParserStateMachine
		{
		public:
			ParserStateMachine(std::map<std::string, std::string>& talkers, std::map<std::string, std::string>& sentencies, ParamParser* paramParser);
			virtual ~ParserStateMachine();

			virtual void Process(const char data, Callback* client);

		protected:
			// States
			ParserStateSentenceIdentification m_ParserStateSentenceIdentification;
			ParserStateTalkerIdentification m_ParserStateTalkerIdentification;
			ParserStateReady m_ParserStateReady;
			ParserStateError m_ParserStateError;
			ParserStateTalkerIdentificationCustomVendor m_ParserStateTalkerIdentificationCustomVendor;
			ParserStateParamIdentification m_ParserStateParamIdentification;

			// Current states is placed here.
			ParserStateBase* m_pCurrentState;

			// Auxillary method translates states enum into concreete instance of the state machine
			ParserStateBase* TranslateToInstance(ParserStates state);
			// resets all states
			void Reset();
		};

		//////////////////////////////////////////////////////////////////
		//   Field parsers machine
		//////////////////////////////////////////////////////////////////
		class FieldsParserMachine
		{
		public:
			FieldsParserMachine(const std::string& stTemplate);
			virtual ~FieldsParserMachine();

			virtual bool ProcessParam(const std::string& param, Callback* client);
			void Reset();
			bool WasError(){return m_bError;}
		protected:
			std::vector<ConcreeteFieldParser*> m_ParsersStack;
			size_t m_currentCmd;
			bool m_bError; 
		};

		// Recommended minimum specific GPS/TRANSIT data
		class RMC_Processor : public FieldsParserMachine
		{
		public:
			RMC_Processor(const std::string& stTemplate):FieldsParserMachine(stTemplate){};
			virtual ~RMC_Processor(){};

			virtual bool ProcessParam(const std::string& param, Callback* client);
		};
		
		// GPS DOP and active satellites
		class GSA_Processor : public FieldsParserMachine
		{
		public:
			GSA_Processor(const std::string& stTemplate):FieldsParserMachine(stTemplate){};
			virtual ~GSA_Processor(){};

			virtual bool ProcessParam(const std::string& param, Callback* client);
		};

		// GPS Satellites in view
		class GSV_Processor : public FieldsParserMachine
		{
		public:
			GSV_Processor(const std::string& stTemplate):FieldsParserMachine(stTemplate){};
			virtual ~GSV_Processor(){};

			virtual bool ProcessParam(const std::string& param, Callback* client);
		};

		// Track Made Good and Ground Speed
		class VTG_Processor : public FieldsParserMachine
		{
		public:
			VTG_Processor(const std::string& stTemplate):FieldsParserMachine(stTemplate){};
			virtual ~VTG_Processor(){};

			virtual bool ProcessParam(const std::string& param, Callback* client);
		};

		//Geographic Position, Latitude / Longitude and time.
		class GLL_Processor : public FieldsParserMachine
		{
		public:
			GLL_Processor(const std::string& stTemplate):FieldsParserMachine(stTemplate){};
			virtual ~GLL_Processor(){};

			virtual bool ProcessParam(const std::string& param, Callback* client);
		};

		// Global Positioning System Fix Data
		class GGA_Processor : public FieldsParserMachine
		{
		public:
			GGA_Processor(const std::string& stTemplate):FieldsParserMachine(stTemplate){};
			virtual ~GGA_Processor(){};

			virtual bool ProcessParam(const std::string& param, Callback* client);
		};

		class ParamParser
		{
		protected:
			typedef std::map<std::string, FieldsParserMachine*>  fieldsParsersMap;
			typedef fieldsParsersMap::iterator					 fieldsParsersIterator;

		public:
			virtual ~ParamParser();

			void Construct(std::map<std::string, std::string>& sentencies);
			FieldsParserMachine* SelectMachine(const std::string& sentence); // can throw exception.

		protected:
			fieldsParsersMap m_parsers;
		};
	}; // namespace NMEA

}; // namespace Componentality

#endif // NMEA_API_STATE