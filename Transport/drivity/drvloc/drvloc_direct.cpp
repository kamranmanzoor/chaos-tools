/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2012-2013 */
/*****************************************************************************/
/* Device independent location information provisioning and encryption       */
/* Implemented with direct using of Componentality NMEA parser and RSA with  */
/* no usage of external providers like GPS                                   */
/*****************************************************************************/
/* Attention! No thread safety!                                              */
/*****************************************************************************/

#include "drvloc.h"
#include "nmeacommon.h"
#include "nmeaparser.h"
#include "RSACrypt.h"
#include <string.h>
#include <fstream>

// Vehicle ID must be provided externally
static VEHICLE_ID __vehicle_id = {0};

using namespace Componentality::NMEA;
using namespace Crypto::RSA;

class NMEAProcessor : public Componentality::NMEA::Callback
{
private:
	VEHICLE_INFO mInfo;									// Storage for parsed vehicle information
	Componentality::NMEA::Parser mParser;				// Parser itself
	Coordinates  mCoordinates;							//   NMEA data for provisioning via regular Componentality NMEA
	Precision    mPrecision;							//   interface. This interface is not used if DRVLOC is provided
	Fix			 mFix;
	double		 mHeading;
	double		 mSpeed;
	size_t		 mSattelitesInView;
	Time		 mTime;
	std::list<Satellite> mSatelliteInfo;
public:
	NMEAProcessor()
	{
		mInfo.mLocation.mAvailable = False;
		mInfo.mLocation.hDOP.mPrecision = -1.0;
		mInfo.mLocation.vDOP.mPrecision = -1.0;
		mInfo.mHeading.mHeading = -1.0;
		mInfo.mVelocity.mVelocity = -1.0;
	};
	virtual ~NMEAProcessor()
	{
	};
public:
	// Setters (used in paraser side to SET values to be getted methods above)
	virtual void setPosition(const Coordinates& position)
	{
		mInfo.mLocation.mPosition.mLatitude.mCoordinate = position.mLatitude;
		mInfo.mLocation.mPosition.mLongitude.mCoordinate = position.mLongitude;
		if (position.mAltitude != 0.0)
			mInfo.mLocation.mPosition.mAltitude.mCoordinate = position.mAltitude;
		mInfo.mLocation.mAvailable = False;
		mCoordinates = position;
	}
	virtual void setPrecision(const Precision& precision)
	{
		mPrecision = precision;
		mInfo.mLocation.hDOP.mPrecision = precision.mHDOP;
		mInfo.mLocation.vDOP.mPrecision = precision.mVDOP;
	};
	virtual void setFix(Fix fix)
	{
		mFix = fix;
		mInfo.mLocation.mAvailable = ((fix != Componentality::NMEA::FIX_NO) ? True : False);
	};
	virtual void setHeading(double heading)
	{
		mHeading = heading;
		mInfo.mHeading.mHeading = heading;
	};
	virtual void setSpeed(double speed)
	{
		mSpeed = speed;
		mInfo.mVelocity.mVelocity = speed;
	};
	virtual void setSatelliteInfo(const std::list<Satellite>& satellites)
	{
		mSatelliteInfo = satellites;
	};
	virtual void setSattelitesInView(size_t count)
	{
		mSattelitesInView = count;
	};
	virtual void setTime(const Time& gmt)
	{
		mTime = gmt;
	};
	virtual void updateSatelliteInfo(const Satellite& info)
	{
	};

	// Getters (used in client side to GET values)
	virtual Coordinates getPosition() const
	{
		return mCoordinates;
	};
	virtual Precision getPrecision() const
	{
		return mPrecision;
	};
	virtual Fix	getFix() const
	{
		return mFix;
	};
	virtual double getHeading() const 
	{
		return mHeading;
	};
	virtual double getSpeed() const
	{
		return mSpeed;
	};
	virtual size_t getSattelitesInView() const
	{
		return mSattelitesInView;
	};
	virtual Time getTime() const
	{
		return mTime;
	};
	virtual std::list<Satellite> getSatelliteInfo() const
	{
		return mSatelliteInfo;
	};
public:
	// event
	virtual void onBlobProcessed(Blob&) {};
	virtual void onSentenceProcessed() {};
public:
	VEHICLE_INFO& getVehicleInfo() {return mInfo;};
	void parseFile(const std::string filename)
	{
		std::ifstream file(filename.c_str(), std::ios::binary);
		file.seekg(0, std::ios::end);
		size_t size = file.tellg();
		file.seekg(0);
		Componentality::NMEA::Blob data;
		data.mLength = size;
		data.mData = new char[size];
		file.read((char*)data.mData, size);
		file.close();
		mParser.Parse(data, this);
		delete[] data.mData;
	};
	void parseBlob(Componentality::NMEA::Blob data)
	{
		mParser.Parse(data, this);
	}
} static nmea_processor;

// Retrieve current vehicle location
extern "C" VEHICLE_INFO getVehicleInfo(void)
{
	return nmea_processor.getVehicleInfo();
};

// Retrieve current time
extern "C" TIMESTAMP    getCurrentTime(void)
{
	TIMESTAMP result;
	time(&result.mTime);
	return result;
};

// Retrieve current vehicle ID
extern "C" VEHICLE_ID getVehicleID(void)
{
	return __vehicle_id;
};

// Change vehicle ID
extern "C" void setVehicleID(VEHICLE_ID vid)
{
	__vehicle_id = vid;
}

// Provide NMEA data for parsing
extern "C" VEHICLE_INFO parseNMEA(const char* nmea)
{
	if (nmea)
	{
		Componentality::NMEA::Blob blob;
		blob.mData = (void*)nmea;
		blob.mLength = strlen(nmea);
		nmea_processor.parseBlob(blob);
	}
	return getVehicleInfo();
};

// Provide NMEA data for parsing in a file
extern "C" VEHICLE_INFO parseNMEAFile(const char* nmea_file)
{
	nmea_processor.parseFile(nmea_file);
	return getVehicleInfo();
};


// Generate key pair using Componentality CST engine RSA adapter
extern "C" KEY_PAIR generateKeyPair(void)
{
	KEY_PAIR keypair;
	Cell<KeyPair> storage;
	KeyGen keygen;
	keygen.GetOutput().Link(storage.GetDataInput());
	keygen.GetInput().operator()(SIGNAL);
	keypair.mSecureKey.mSecureKey.mData = storage.GetValue().x.data;
	keypair.mSecureKey.mSecureKey.mSize = storage.GetValue().x.length;
	keypair.mPublicKey.mPublicKey.mData = storage.GetValue().y.data;
	keypair.mPublicKey.mPublicKey.mSize = storage.GetValue().y.length;
	return keypair;
};

// Encrypt data block
extern "C" BLOB rsa_encrypt(BLOB decrypted_data, PUBLIC_KEY key)
{
	BLOB result;
	DataBlock pkey;
	DataBlock source;
	Encryptor encryptor;
	Cell<DataBlock> storage;
	pkey.data = key.mPublicKey.mData;
	pkey.length = key.mPublicKey.mSize;
	source.data = decrypted_data.mData;
	source.length = decrypted_data.mSize;
	encryptor.GetOutput().Link(storage.GetDataInput());
	encryptor.GetKeyInput().operator()(pkey);
	encryptor.GetDataInput().operator()(source);
	result.mData = storage.GetValue().data;
	result.mSize = storage.GetValue().length;
	return result;
}

// Decrypt data block
extern "C" BLOB rsa_decrypt(BLOB encrypted_data, SECURE_KEY key)
{
	BLOB result;
	DataBlock skey;
	DataBlock source;
	Decryptor decryptor;
	Cell<DataBlock> storage;
	skey.data = key.mSecureKey.mData;
	skey.length = key.mSecureKey.mSize;
	source.data = encrypted_data.mData;
	source.length = encrypted_data.mSize;
	decryptor.GetOutput().Link(storage.GetDataInput());
	decryptor.GetKeyInput().operator()(skey);
	decryptor.GetDataInput().operator()(source);
	result.mData = storage.GetValue().data;
	result.mSize = storage.GetValue().length;
	return result;
};

extern "C" void initVehicleInfo(VEHICLE_INFO* vi)
{
	if (!vi)
		return;
	vi->mLocation.mPosition.mLatitude.mCoordinate = -1.0;
	vi->mLocation.mPosition.mLongitude.mCoordinate = -1.0;
	vi->mLocation.mPosition.mAltitude.mCoordinate = -100000.0;
	vi->mLocation.mAvailable = False;
	vi->mLocation.hDOP.mPrecision = -1.0;
	vi->mLocation.vDOP.mPrecision = -1.0;
	vi->mVelocity.mVelocity = -1.0;
	vi->mHeading.mHeading = -1.0;
};
