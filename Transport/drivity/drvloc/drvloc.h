/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* Device independent location information provisioning and encryption       */
/*****************************************************************************/

#ifndef DRVLOC_H
#define DRVLOC_H

#include <time.h>

#if defined(__cplusplus) || defined(__GNUG__)
extern "C"
{
#endif

// Type definition for boolean
typedef enum __BOOLEAN
{
	False = 0,
	True  = 1
} BOOLEAN;
// Type definition for a single coordinate
typedef struct __COORDINATE
{
	double mCoordinate;
} COORDINATE;
// Type definition for 3D position (three coordinates)
typedef struct __POSITION
{
	COORDINATE mLatitude;
	COORDINATE mLongitude;
	COORDINATE mAltitude;
} POSITION;
// Type definition for location precision in GPS DOP format
typedef struct __PRECISION
{
	double mPrecision;
} PRECISION;
// Location informaion: coordinates and precision
typedef struct __LOCATION
{
	BOOLEAN mAvailable;		// True if location available (fix present)
	POSITION mPosition;		// Positioning information
	PRECISION hDOP;			// Horizontal precision
	PRECISION vDOP;			// Vertical precision
} LOCATION;
// Velocity information
typedef struct __VELOCITY
{
	double mVelocity;
} VELOCITY;
// Heading (course) information
typedef struct __HEADING
{
	double mHeading;
} HEADING;
// Full vehicle movement information
typedef struct __VEHICLE_INFO
{
	LOCATION mLocation;		// Location
	VELOCITY mVelocity;		// Velocity
	HEADING  mHeading;		// Heading
} VEHICLE_INFO;
// Type for vehicle ID
typedef struct __VEHICLE_ID
{
	char* mVehicleID;
} VEHICLE_ID;
// Type for time stamp
typedef struct __TIMESTAMP
{
	time_t mTime;
} TIMESTAMP;

// *** Requests to the operating environment *** //

// Retrieve current vehicle location
VEHICLE_INFO getVehicleInfo(void);
// Retrieve current time
TIMESTAMP    getCurrentTime(void);
// Retrieve current vehicle ID
VEHICLE_ID	 getVehicleID(void);
// Change vehicle ID
void		 setVehicleID(VEHICLE_ID vid);
// Provide NMEA data for parsing in a string format
VEHICLE_INFO parseNMEA(const char* nmea);
// Provide NMEA data for parsing in a file
VEHICLE_INFO parseNMEAFile(const char* nmea_file);
// Initialize vehicle info structure
void initVehicleInfo(VEHICLE_INFO*);

// *************************************************************************** //

// Block of bytes
typedef struct __BLOB
{
	void* mData;
	long  mSize;
} BLOB;

// Secure key
typedef struct __SECURE_KEY
{
	BLOB mSecureKey;
} SECURE_KEY;

// Public key
typedef struct __PUBLIC_KEY
{
	BLOB mPublicKey;
} PUBLIC_KEY;

// Key pair
typedef struct __KEY_PAIR
{
	BOOLEAN    mAvailable;		// Pair is generated
	SECURE_KEY mSecureKey;		// Secure key
	PUBLIC_KEY mPublicKey;		// Public key
} KEY_PAIR;

// *** Encryption block *** //

// Generate key pair
KEY_PAIR generateKeyPair(void);
// Encrypt data block
BLOB     rsa_encrypt(BLOB decrypted_data, PUBLIC_KEY key);
// Decrypt data block
BLOB	 rsa_decrypt(BLOB encrypted_data, SECURE_KEY key);

#if defined(__cplusplus) || defined(__GNUG__)
}; // extern "C"
#endif

#endif