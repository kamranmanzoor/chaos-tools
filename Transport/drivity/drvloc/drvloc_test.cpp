/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2012-2013 */
/*****************************************************************************/
/* DRVLOC API test code                                                      */
/*****************************************************************************/

#include "drvloc.h"
#include <fstream>

std::string TESTLINE = "0123456789";

int main(int argc, char** argv)
{
	if (argc > 1)
	{
		std::string fname = argv[1];
		printf(fname.c_str());
		VEHICLE_INFO vinfo = parseNMEAFile(fname.c_str());
	};

	KEY_PAIR kp = generateKeyPair();
	BLOB line; line.mData = (void*)TESTLINE.c_str(); line.mSize = TESTLINE.size() + 1;
	BLOB encoded = encrypt(line, kp.mPublicKey);
	BLOB decoded = decrypt(encoded, kp.mSecureKey);
	std::string result = (char*) decoded.mData;
	return 0;
};
