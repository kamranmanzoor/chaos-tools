#include "records.h"
#include "../../CommonLibs/libjson/Json.h"
#include "../../CommonLibs/sha1/CompoSHA.h"
#include "MessageEngine.h"
#include "StandardComponents.h"
#include "Base64.h"
#include "drvloc.h"
#include "RecordCrypt.h"
#include <time.h>

using namespace Componentality::LBS;
using namespace Componentality::Simplicity::Configuration;
using namespace CST::Common::LibJSON;

std::string Componentality::LBS::ARG_LATITUDE		= "lat";
std::string Componentality::LBS::ARG_LONGITUDE		= "lon";
std::string Componentality::LBS::ARG_ALTITUDE		= "alt";
std::string Componentality::LBS::ARG_AVAILABILITY	= "avl";
std::string Componentality::LBS::ARG_VEHICLE_ID		= "vid";
std::string Componentality::LBS::ARG_HEADING		= "hdg";
std::string Componentality::LBS::ARG_VELOCITY		= "vel";
std::string Componentality::LBS::ARG_TIMESTAMP		= "tms";
std::string Componentality::LBS::ARG_HDOP			= "hdp";
std::string Componentality::LBS::ARG_VDOP			= "vdp";
std::string Componentality::LBS::ARG_CHECKSUM		= "sha";

std::string makeSHA1(BLOB blob)
{
	CST::Common::SHA_Converter sha;
	CST::Common::blob _blob; _blob.mData = (char*) blob.mData; _blob.mSize = blob.mSize;
	CST::Common::DIGEST digest = sha(_blob);

	std::string result = CST::Common::SHA_serialize(digest);
	return Componentality::Simplicity::Configuration::BASE64_encode(result);
};

std::string Componentality::LBS::doubleToString(double x)
{
	std::string result;

	if (x < 0)
	{
		result += '-';
		x = -x;
	};

	unsigned int intpart = (unsigned int) x;

	x -= intpart;

	if (!intpart)
	{
		result += '0';
	}
	else
	{
		std::string tmp;
		while (intpart)
		{
			char c = (char) (intpart % 10) + '0';
			tmp = std::string() + c + tmp;
			intpart /= 10;
		};
		result += tmp;
	};

	if (x != 0.0)
		result += '.';

	int sgns = 0;

	while ((sgns++ < 10) && (x > 0.0))
	{
		x *= 10.0;
		intpart = (unsigned int) x;
		x -= intpart;
		result += (char) intpart + '0';
	};

	return result;
};

double Componentality::LBS::stringToDouble(std::string str)
{
	double result = 0.0;
	bool negative = false;
	unsigned int idx = 0;
	int intpart = 0;
	unsigned long divider = 10;
	bool intpart_parsing = true;

	if (str[idx] == '-')
	{
		negative = true;
		idx += 1;
	}

	for (;idx < str.size();idx++)
	{
		if (intpart_parsing)
		{
			if ((str[idx] <= '9') && (str[idx] >= '0'))
				intpart = intpart * 10 + (str[idx] - '0');
			if (str[idx] == '.')
				intpart_parsing = false;
		}
		else
		{
			if ((str[idx] <= '9') && (str[idx] >= '0'))
			{
				double tmp = ((double) (str[idx] - '0')) / divider;
				result += tmp;
				divider *= 10;
			};
		};
	};

	result += intpart;

	if (negative)
		result = -result;
	return result;
};

std::string Componentality::LBS::packVehicleInfo(const VEHICLE_INFO& vi)
{
	std::string latitude		= doubleToString(vi.mLocation.mPosition.mLatitude.mCoordinate);
	std::string longitude		= doubleToString(vi.mLocation.mPosition.mLongitude.mCoordinate);
	std::string altitude		= doubleToString(vi.mLocation.mPosition.mAltitude.mCoordinate);
	std::string availability	= (vi.mLocation.mAvailable == True) ? "1" : "0";
	std::string hdop			= doubleToString(vi.mLocation.hDOP.mPrecision);
	std::string vdop			= doubleToString(vi.mLocation.vDOP.mPrecision);
	std::string velocity		= doubleToString(vi.mVelocity.mVelocity);
	std::string heading			= doubleToString(vi.mHeading.mHeading);

	std::string result = std::string() + "{\n"
		+ "\t\"" + ARG_LATITUDE + "\":" + latitude + ",\n"
		+ "\t\"" + ARG_LONGITUDE + "\":" + longitude + ",\n"
		+ "\t\"" + ARG_ALTITUDE + "\":" + altitude + ",\n" 
		+ "\t\"" + ARG_AVAILABILITY + "\":" + availability + ",\n"
		+ "\t\"" + ARG_HEADING + "\":" + heading + ",\n"
		+ "\t\"" + ARG_VELOCITY + "\":" + velocity + ",\n"
		+ "\t\"" + ARG_HDOP + "\":" + hdop + ",\n"
		+ "\t\"" + ARG_VDOP + "\":" + vdop + "\n}\n";

	return result;
};

double __get_json_double(JsonObject* jobj)
{
	if (!jobj)
		return 0.0;
	if (jobj->GetObjectType() == JsonObject::NUMBER)
	{
		if (((Number*) jobj)->GetType() == Number::FLOAT)
			return ((Number*) jobj)->GetValue().float_value;
		if (((Number*) jobj)->GetType() == Number::INTEGER)
			return ((Number*) jobj)->GetValue().integer_value;
	}
	return 0.0;
}

VEHICLE_INFO Componentality::LBS::unpackVehicleInfo(const std::string str)
{
	Object jobj;
	std::pair<size_t, size_t> result = jobj.Parse(str, 0, str.size());

	VEHICLE_INFO vi;
	initVehicleInfo(&vi);

	vi.mLocation.mPosition.mLatitude.mCoordinate = __get_json_double(jobj[ARG_LATITUDE]);
	vi.mLocation.mPosition.mLongitude.mCoordinate = __get_json_double(jobj[ARG_LONGITUDE]);
	vi.mLocation.mPosition.mAltitude.mCoordinate = __get_json_double(jobj[ARG_ALTITUDE]);
	vi.mLocation.mAvailable = (__get_json_double(jobj[ARG_AVAILABILITY]) == 0.0) ? False : True;
	vi.mLocation.hDOP.mPrecision = __get_json_double(jobj[ARG_HDOP]);
	vi.mLocation.vDOP.mPrecision = __get_json_double(jobj[ARG_VDOP]);
	vi.mVelocity.mVelocity = __get_json_double(jobj[ARG_VELOCITY]);
	vi.mHeading.mHeading = __get_json_double(jobj[ARG_HEADING]);

	return vi;
};

Componentality::LBS::LBRecord::LBRecord(const VEHICLE_ID vid) : mId(vid)
{
	initVehicleInfo(&mInfo);
	mTime.mTime = time(0);
	mFreeNeeded = false;
};

Componentality::LBS::LBRecord::LBRecord(const VEHICLE_ID vid, const VEHICLE_INFO vi) : mId(vid), mInfo(vi)
{
	mTime.mTime = time(0);
	mFreeNeeded = false;
};

Componentality::LBS::LBRecord::LBRecord(const VEHICLE_ID vid, const VEHICLE_INFO vi, const TIMESTAMP time) : mId(vid), mInfo(vi), mTime(time)
{
	mFreeNeeded = false;
};

Componentality::LBS::LBRecord::~LBRecord()
{
	if (mFreeNeeded)
		if (mId.mVehicleID)
			delete[] mId.mVehicleID;
};

std::string Componentality::LBS::LBRecord::Serialize(const bool add_checksum)
{
	std::string vi = packVehicleInfo(mInfo);
	double _time1 = (double)((mTime.mTime >> 32) & 0xFFFFFFFF);
	std::string __time1 = doubleToString(_time1);
	double _time2 = (double) ((mTime.mTime >> 0) & 0xFFFFFFFF);
	std::string __time2 = doubleToString(_time2);
	std::string vid = mId.mVehicleID ? mId.mVehicleID : "";
	std::string result = std::string(ARG_VEHICLE_ID) + "=" + vid + '\n' + ARG_TIMESTAMP + "=" + __time1 + ":" + __time2 + "\n\n" + vi;
	if (add_checksum)
		result = addChecksum(result);
	return result;
}

std::pair< std::string, std::string > Componentality::LBS::__split_nn(const std::string str)
{
	std::pair< std::string, std::string > result;
	size_t pos = str.find("\n\n");
	if (pos == (size_t) -1)
	{
		return result;
	}
	std::string header = str.substr(0, pos);
	std::string content = str.substr(pos + 2);

	result.first = header;
	result.second = content;

	return result;
};

std::string Componentality::LBS::__find(const std::string _header, const std::string keyval)
{
	std::string header = _header;
	do
	{
		size_t pos = header.find('\n');
		std::string clause;
		if (pos != (size_t) -1)
		{
			clause = header.substr(0, pos);
			header = header.substr(pos + 1);
		}
		else
		{
			clause = header;
			header = "";
		};
		size_t splitter = clause.find('=');
		if (splitter != (size_t) -1)
		{
			std::string key = clause.substr(0, splitter);
			std::string value = clause.substr(splitter + 1);
			if (key == keyval)
			{
				return value;
			}
		}
	}
	while (!header.empty());
	return std::string();
};

bool Componentality::LBS::LBRecord::Deserialize(const std::string str)
{
	std::pair< std::string, std::string > s = __split_nn(str);
	std::string header = s.first;
	std::string content = s.second;
	mInfo = unpackVehicleInfo(content);
	std::string value = __find(header, ARG_VEHICLE_ID);
	if (!value.empty())
	{
		if (mFreeNeeded)
			if (mId.mVehicleID)
				delete[] mId.mVehicleID;
		mId.mVehicleID = new char[value.size() + 1];
		strcpy(mId.mVehicleID, value.c_str());
		mFreeNeeded = true;
	};
    value = __find(header, ARG_TIMESTAMP);
	if (!value.empty())
	{
		size_t splitter = value.find(':');
		if (splitter != (size_t) -1)
		{
			std::string time1 = value.substr(0, splitter);
			std::string time2 = value.substr(splitter + 1);
			double _time1 = stringToDouble(time1);
			double _time2 = stringToDouble(time2);
			mTime.mTime = ((unsigned long) _time1) << 32 | ((unsigned long) _time2);
		};
	}
	value = __find(header, ARG_CHECKSUM);
	if (!value.empty())
		return verifyChecksum(str);
	return true;
};

std::string Componentality::LBS::addChecksum(const std::string& str)
{
	std::pair< std::string, std::string > s = __split_nn(str);
	std::string header = s.first;
	std::string content = s.second;
	BLOB _content;
	_content.mData = (void*) content.c_str();
	_content.mSize = content.size();
	std::string sha = makeSHA1(_content);
	std::string result = header + '\n' + ARG_CHECKSUM + '=' + sha + "\n\n" + content;
	return result;
};

bool Componentality::LBS::verifyChecksum(const std::string& str)
{
	std::pair< std::string, std::string > s = __split_nn(str);
	std::string header = s.first;
	std::string content = s.second;
	BLOB _content;
	_content.mData = (void*) content.c_str();
	_content.mSize = content.size();
	std::string sha = makeSHA1(_content);
	std::string value = __find(header, ARG_CHECKSUM);
	return value == sha;
};

////////////////////////////////////////////////////////////////////////////////////////////
LBSecurityProvider::LBSecurityProvider(const VEHICLE_ID id)
	: mRecordEncryptor(new Componentality::Crypto::RecordCrypt(id.mVehicleID ? id.mVehicleID : ""))
{
};

LBSecurityProvider::~LBSecurityProvider()
{
	delete (Componentality::Crypto::RecordCrypt*) mRecordEncryptor;
};

LBSecureRecord::LBSecureRecord(const VEHICLE_ID id, LBSecurityProvider& sp)
	: LBRecord(id),
	  mSecurityProvider(sp)
{
};

LBSecureRecord::LBSecureRecord(const VEHICLE_ID id, const VEHICLE_INFO info, LBSecurityProvider& sp)
	: LBRecord(id, info),
	  mSecurityProvider(sp)
{
};

LBSecureRecord::LBSecureRecord(const VEHICLE_ID id, const VEHICLE_INFO info, const TIMESTAMP time, LBSecurityProvider& sp)
	: LBRecord(id, info, time),
	  mSecurityProvider(sp)
{
};

LBSecureRecord::~LBSecureRecord()
{
};

std::string LBSecureRecord::Serialize(const bool add_checksum)
{
	std::string raw = LBRecord::Serialize(true);
	return mSecurityProvider.encrypt(raw);
};

bool LBSecureRecord::Deserialize(const std::string str)
{
	std::pair< std::string, std::string > s = __split_nn(str);
	std::string raw = mSecurityProvider.decrypt(str);
	return LBRecord::Deserialize(s.first + "\n\n" + raw);
};

std::string LBSecurityProvider::init()
{
	return ((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->setup().serialize();
};

std::list<std::string> LBSecurityProvider::listRemoteSides()
{
	return ((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->listRemoteSides();
};

void LBSecurityProvider::registerRemoteSide(const VEHICLE_ID id, const std::string keys)
{
	Componentality::Crypto::RecordCrypt::KEYS_TO_REMOTE ktr;
	ktr.deserialize(keys);
	((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->addRemoteSide(id.mVehicleID, ktr.mEncryptionKey, ktr.mSignatureKey);
};

void LBSecurityProvider::unregisterRemoteSide(const VEHICLE_ID id)
{
	((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->deleteRemoteSide(id.mVehicleID);
};

std::string LBSecurityProvider::serialize()
{
	return ((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->serialize();
};

bool LBSecurityProvider::deserialize(const std::string str)
{
	return ((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->deserialize(str);
};

std::string LBSecurityProvider::encrypt(const std::string& s)
{
	return ((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->encryptRecord(s);
};

std::string LBSecurityProvider::decrypt(const std::string& s)
{
	return ((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->decryptRecord(s);
};

std::string LBSecurityProvider::getID()
{
	return ((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->getID();
};

std::string LBSecurityProvider::getKeys(const std::string& id)
{
	return ((Componentality::Crypto::RecordCrypt*) mRecordEncryptor)->getKeys(id).serialize();
}
