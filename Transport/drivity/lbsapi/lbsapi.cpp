/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/

#include "lbsapi.h"
#include "records.h"
#include <string.h>
#include <fstream>
#include <map>

using namespace Componentality::LBS;

// Default configuration directories for Windows and Linux
#ifndef WIN32
#define CONFIG_DIR "/etc/drivity/"
#else
#define CONFIG_DIR "C:\\Drivity\\"
#endif

// Default configuration files
const std::string INI_VEHICLE_ID = "vehicle.ini";
const std::string INI_GATES = "gates.ini";

// Maximum size of configuration file
const size_t CONFIG_SIZE_LIMIT = 16 * 1024 * 1024;

LBSAPI::LBSAPI(const std::string config_dir) : mLogger(NULL), mPeriodicLogger(NULL), mGateLogger(NULL), mVIDLine(NULL)
{
	mConfigDirectory = config_dir.empty() ? CONFIG_DIR : config_dir;
	load();
};

LBSAPI::~LBSAPI()
{
	dispose();
};

void LBSAPI::dispose()
{
	if (mLogger)
	{
		if (mPeriodicLogger)
		{
			mLogger->unsubscribe(LBSEVT_LOCATION, *mPeriodicLogger);
		};
		if (mGateLogger)
		{
			mLogger->unsubscribe(LBSEVT_GATEPASS, *mGateLogger);
		};
	}
	if (mPeriodicLogger)
		delete mPeriodicLogger;
	if (mGateLogger)
		delete mGateLogger;
	if (mLogger)
		delete mLogger;

	if (mVIDLine)
		delete[] mVIDLine;

	mLogger = NULL;
	mPeriodicLogger = NULL;
	mGateLogger = NULL;
	mVIDLine = NULL;
};

std::string LBSAPI::init(const std::string vehicle_id, const std::string output_folder)
{
	dispose();

	VEHICLE_ID vid;
	mVIDLine = vid.mVehicleID = new char[vehicle_id.size() + 1];
	strcpy(vid.mVehicleID, vehicle_id.c_str());

	mLogger = new FileLogger(vid, FileLogger::TIME, output_folder);
	std::string result = mLogger->init();
	setVehicleID(mLogger->getVehicleID());
	if (!result.empty())
		save();
	return result;
};

void LBSAPI::save()
{
	if (!mLogger)
		return;
	std::ofstream file((mConfigDirectory + INI_VEHICLE_ID).c_str(), std::ios::binary);
	std::string provider = ((LBSecurityProvider*) (mLogger))->serialize();
	file.write(provider.c_str(), provider.size());
	file.close();
	file.open((mConfigDirectory + INI_GATES).c_str(), std::ios::binary);
	std::string gates = ((GateSet*) (mLogger))->serialize();
	file.write(gates.c_str(), gates.size());
	file.close();
};

std::string Componentality::LBS::fread(const std::string& fname)
{
	std::ifstream file(fname.c_str(), std::ios::binary);
	file.seekg(0, std::ios::end);
	size_t size = file.tellg();
	if (size > CONFIG_SIZE_LIMIT)
	{
		file.close();
		return std::string();
	};
	file.seekg(0);
	char* str = new char[size];
	file.read(str, size);
	std::string content;
	for (size_t i = 0; i < size; i++)
		content += str[i];
	delete[] str;
	file.close();
	return content;
};

void LBSAPI::load()
{
	dispose();
	std::string provider = fread(mConfigDirectory + INI_VEHICLE_ID);
	std::string gates = fread(mConfigDirectory + INI_GATES);
	VEHICLE_ID id;
	id.mVehicleID = NULL;
	mLogger = new FileLogger(id, FileLogger::TIME, "");
	((LBSecurityProvider*) mLogger)->deserialize(provider);
	((GateSet*) mLogger)->deserialize(gates);
	mVIDLine = mLogger->getVehicleID().mVehicleID;
	setVehicleID(mLogger->getVehicleID());
};

// TBD: "Key string:" shall be replaced with more reliable signature or this feature moved to Drivity utility
bool LBSAPI::pair(const std::string vehicle_id, const std::string infile)
{
	if (!mLogger)
		return false;
	std::string keys = fread(infile);
	std::string findline = "Key string:\n";
	size_t pos = keys.find(findline);
	if (pos != (size_t) -1)
		keys = keys.substr(pos + findline.size());
	findline = "Key string:\r\n";
	pos = keys.find(findline);
	if (pos != (size_t) -1)
		keys = keys.substr(pos + findline.size());	
	if (keys.empty())
		return false;
	VEHICLE_ID vid;
	vid.mVehicleID = (char*) vehicle_id.c_str();
	std::list<std::string> remote_sides = mLogger->listRemoteSides();
	for (std::list<std::string>::iterator i = remote_sides.begin(); i != remote_sides.end(); i++)
		if (*i == vehicle_id)
			return false;
	mLogger->registerRemoteSide(vid, keys);
	save();
	return true;
};

std::string LBSAPI::list()
{
	std::string result;
	if (!mLogger)
		return result;
	std::list<std::string> remote_sides = mLogger->listRemoteSides();
	for (std::list<std::string>::iterator i = remote_sides.begin(); i != remote_sides.end(); i++)
		result += *i + '\n';
	return result;
};

bool LBSAPI::forget(const std::string vehicle_id)
{
	VEHICLE_ID vid;
	if (!mLogger)
		return false;
	vid.mVehicleID = (char*) vehicle_id.c_str();
	bool exist = false;
	std::list<std::string> remote_sides = mLogger->listRemoteSides();
	for (std::list<std::string>::iterator i = remote_sides.begin(); i != remote_sides.end(); i++)
		if (*i == vehicle_id)
			return true;
	if (!exist)
		return false;
	mLogger->unregisterRemoteSide(vid);
	save();
	return true;
};

void LBSAPI::track(const long period, const std::string output_folder, const std::string input_stream)
{
	if (!mLogger)
		return;
	if (!mPeriodicLogger)
		mPeriodicLogger = new PeriodicalLogger(period, *mLogger);
	if (!mGateLogger)
		mGateLogger = new GateLogger(*mLogger);
	mLogger->setFolder(output_folder);
	if (!input_stream.empty())
	{
		std::ifstream file(input_stream.c_str());
		mLogger->parse(file);
		file.close();
	}
	else
		mLogger->parse(std::cin);
	delete mPeriodicLogger;
	delete mGateLogger;
	mPeriodicLogger = NULL;
	mGateLogger = NULL;
};

std::pair<std::string, std::string> LBSAPI::parse(const std::string filename)
{
	std::pair<std::string, std::string> result;
	if (!mLogger)
		return result;
	std::string content = fread(filename);
	LBSecureRecord record(mLogger->getVehicleID(), *mLogger);
	if (!record.Deserialize(content))
		return result;
	result.second = packVehicleInfo(record.getVehicleInfo());
	result.first = record.getVehicleId().mVehicleID;
	return result;
};

bool LBSAPI::parse(VEHICLE_INFO& result, const std::string filename)
{
	if (!mLogger)
		return false;
	std::string content = fread(filename);
	LBSecureRecord record(mLogger->getVehicleID(), *mLogger);
	if (!record.Deserialize(content))
		return false;
	result = record.getVehicleInfo();
	return true;
};

bool LBSAPI::addGates(const std::string gates)
{
	if (!mLogger)
		return false;
	GateSet gset;
	if (!gset.deserialize(gates))
		return false;
	*mLogger += gset;
	save();
	return true;
};

bool LBSAPI::removeGate(const std::string gate_name)
{
	if (!mLogger)
		return false;
	Gate* gate = mLogger->find(gate_name);
	if (gate)
		mLogger->removeGate(*gate);
	else
		return false;
	save();
	return true;
};

std::string LBSAPI::getGates()
{
	return fread(mConfigDirectory + INI_GATES);
};

std::string LBSAPI::encrypt(const std::string decrypted)
{
	if (!mLogger)
		return std::string();
	std::string record = ARG_VEHICLE_ID + '=';
	record += std::string(getVehicleID().mVehicleID);
	record += "\n\n" + decrypted;
	record = addChecksum(record);
	return mLogger->encrypt(record);
};

// Do data encryption for one given target specified by vehicle ID
std::string LBSAPI::encrypt(const std::string decrypted, const std::string vehicle_id)
{
	if (!mLogger)
		throw "Internal error";
	// Remove all destinations except given one
	std::list<std::string> remote_sides = mLogger->listRemoteSides();
	std::map<std::string, std::string> keys;
	for (std::list<std::string>::iterator i = remote_sides.begin(); i != remote_sides.end(); i++)
		if (*i != vehicle_id)
		{
			VEHICLE_ID vid;
			vid.mVehicleID = (char*)i->c_str();
			keys[*i] = mLogger->getKeys(*i);
			mLogger->unregisterRemoteSide(vid);
		}

	// Prepare data for encryption
	if (!mLogger)
		return std::string();
	std::string record = ARG_VEHICLE_ID + '=';
	record += std::string(getVehicleID().mVehicleID);
	record += "\n\n" + decrypted;
	record = addChecksum(record);

	// Do encryption
	std::string result = mLogger->encrypt(record);

	// Restore list of paired items back
	for (std::list<std::string>::iterator i = remote_sides.begin(); i != remote_sides.end(); i++)
		if (*i != vehicle_id)
		{
			VEHICLE_ID vid;
			vid.mVehicleID = (char*)i->c_str();
			mLogger->registerRemoteSide(vid, keys[*i]);
		}
	return result;
};

std::string LBSAPI::decrypt(const std::string encrypted)
{
	if (!mLogger)
		return std::string();
	std::string header = __split_nn(encrypted).first;
	std::string decrypted = mLogger->decrypt(encrypted);
	std::string full = header + "\n\n" + decrypted;
	if (!verifyChecksum(full))
		return std::string();
	else
		return decrypted;
};
