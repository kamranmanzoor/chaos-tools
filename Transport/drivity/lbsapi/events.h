/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2012-2013 */
/*****************************************************************************/
/* Location based events processing. Describes basic components to           */
/* handle events like passing given geographical area                        */
/* Shall be used as a callback-based OO interface                            */
/*****************************************************************************/
/* Attention! No thread safety!                                              */
/*****************************************************************************/

#ifndef EVENTS_H
#define EVENTS_H

#include <string>
#include <map>

namespace Componentality
{

	namespace EventManagement
	{

		// Event like a callback structure provided to subscribers in case of processor generates an event
		class Event
		{
		private:
			const std::string mType;						// Type of the event
		public:
			Event(const std::string type) : mType(type) {};
			virtual ~Event() {};
			std::string getType() const {return mType;};
			virtual std::string getInfo() const {return std::string();};
		};

		// Event subscriber any class receiving LBS events must inherit this type
		class Subscriber
		{
			friend class Processor;
		public:
			Subscriber() {};
			virtual ~Subscriber() {};
		protected:
			virtual void onEvent(Event&) = 0;
		};

		// Event processor manages list of subscribers and distributes event to it
		class Processor
		{
		private:
			typedef std::map<Subscriber*, Subscriber*> Subscribers;
			std::map<std::string, Subscribers> mSubscribers;
		public:
			Processor() {};
			virtual ~Processor() {};
		public:
			virtual void subscribe(std::string evt, Subscriber&);	// Add subscriber
			virtual void unsubscribe(std::string evt, Subscriber&);	// Remove subscriber
			virtual void sendEvent(Event&);							// Send events to all subscribers
		};

	}; // namespace LBS
}; // namespace Componentality


#endif