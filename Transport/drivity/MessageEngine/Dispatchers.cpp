/****************************************************************************************************/
/* CST Middleware 2008                        * Dispatchers.h										*/
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Representations and auxiliary classes to dispatch messages ranged by priorities.                 */
/****************************************************************************************************/
#include "BasicDefinitions.h"
#include "Dispatchers.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;

DispatcherInterface& DispatcherInterface::AddMessage(void* message_body,
											AbstractUntypedInputInterface& receiver,
											unsigned int priority)
{
	UntypedMessage message;
	message.message_body = message_body;
	message.input = &receiver;
	message.priority = priority;

	std::list<UntypedMessage>::iterator i = queue.begin();

	for (; i != queue.end(); i++)
	{
		if (i->priority > message.priority)
		break;
	};
	queue.insert(i, message);

	return *this;
};

Dispatcher::Dispatcher()
	: scheduling_initiator(*this), message_added_notification(*this),
	  Consumer<SIGNAL_TYPE>(scheduling_initiator),
	  Producer<SIGNAL_TYPE>(message_added_notification),
	  dispatched_notification(*this)
{
};

Dispatcher::~Dispatcher()
{
};

Entity<SIGNAL_TYPE>& Dispatcher::GetMessage(InputGate<SIGNAL_TYPE>& source_address, const SIGNAL_TYPE message)
{
	if (&source_address != &GetInput())
		throw int(0);
	Dispatch();
	return *this;
};

Entity<SIGNAL_TYPE>& Dispatcher::SendMessage(OutputGate<SIGNAL_TYPE>& distribution_address, const SIGNAL_TYPE message)
{
	if ((&distribution_address != &GetOutput()) &&
		(&distribution_address != &GetNotificationGate()))
		throw int(0);
	distribution_address.operator()(SIGNAL);
	return *this;
};

DispatcherInterface& Dispatcher::AddMessage(void* message_body,
											AbstractUntypedInputInterface& receiver,
											unsigned int priority)
{
	DispatcherInterface::AddMessage(message_body, receiver, priority);
	SendMessage(message_added_notification, SIGNAL);
	return *this;
};

DispatcherInterface& Dispatcher::Dispatch()
{
	while (!IsEmpty())
	{
		UntypedMessage msg = DispatcherInterface::GetMessage();
		msg.input->operator()(msg.message_body);
	};
	SendMessage(dispatched_notification, SIGNAL);
	return *this;
};

