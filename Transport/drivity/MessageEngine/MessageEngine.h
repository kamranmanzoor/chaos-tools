/****************************************************************************************************/
/* CST Middleware 2008                        * MessageEngine.h                                     */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* CST Middleware API                                                                               */
/****************************************************************************************************/
#ifndef MESSAGE_ENGINE_H
#define MESSAGE_ENGINE_H

#ifdef GetMessage
#undef GetMessage
#endif

#include "BasicDefinitions.h"
#include "Dispatchers.h"
#include "Entities.h"
#include "Gates.h"

#endif