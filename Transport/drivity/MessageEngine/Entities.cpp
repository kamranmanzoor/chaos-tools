/****************************************************************************************************/
/* CST Middleware 2008                        * Entities.cpp									    */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* The minimum set of message processing class templates                                            */
/****************************************************************************************************/
#include "BasicDefinitions.h"
#include "Entities.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;

