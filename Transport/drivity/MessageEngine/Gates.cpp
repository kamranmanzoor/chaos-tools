/****************************************************************************************************/
/* CST Middleware 2008                        * Gates.cpp    										*/
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Representations of the typical input and output message gates                                    */
/****************************************************************************************************/
#include "BasicDefinitions.h"
#include "Gates.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
