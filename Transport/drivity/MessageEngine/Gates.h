/****************************************************************************************************/
/* CST Middleware 2008                        * Gates.h												*/
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Representations of the typical input and output message gates                                    */
/****************************************************************************************************/
#ifndef GATES_H
#define GATES_H

#include "BasicDefinitions.h"
#include <list>

namespace CSTMiddleWare
{

namespace MessageEngine
{

	template<class Message>
	class DirectOutputGate : public OutputGate<Message>
	{
	protected:
		std::list<InputGate<Message>*> linked_inputs;
	public:
		DirectOutputGate(Entity<Message>& master);
		virtual ~DirectOutputGate();
		virtual OutputGate<Message>& operator()(const Message message);
		virtual OutputGate<Message>& Link(InputGate<Message>& input);
		virtual OutputGate<Message>& Unlink(InputGate<Message>& input);
	};

	template<class Message>
	class TriggeringInputGate : public InputGate<Message>
	{
	public:
		TriggeringInputGate(Entity<Message>& master);
		virtual ~TriggeringInputGate();
		virtual InputGate<Message>& operator()(const Message message);
	};

	template<class Message>
	DirectOutputGate<Message>::DirectOutputGate(Entity<Message>& master)
		: OutputGate<Message>(master)
	{
	};

	template<class Message>
	DirectOutputGate<Message>::~DirectOutputGate()
	{
	};

	template<class Message>
	OutputGate<Message>& DirectOutputGate<Message>::operator ()(const Message message)
	{
		for (typename std::list<InputGate<Message>*>::iterator i = linked_inputs.begin(); i != linked_inputs.end(); i++)
		{
			if (*i != NULL)
			{
				(*i)->operator()(message);
			};
		};
	
		typename std::list<InputGate<Message>*>::iterator i;
		return *this;
	};

	template<class Message>
	OutputGate<Message>& DirectOutputGate<Message>::Link(InputGate<Message> &input)
	{
		for (typename std::list<InputGate<Message>*>::iterator i = linked_inputs.begin(); i != linked_inputs.end(); i++)
		{
			if (*i == &input)
			{
				return *this;
			};
		};
		linked_inputs.push_back(&input);
		return *this;
	};

	template<class Message>
	OutputGate<Message>& DirectOutputGate<Message>::Unlink(InputGate<Message> &input)
	{
		for (typename std::list<InputGate<Message>*>::iterator i = linked_inputs.begin(); i != linked_inputs.end(); i++)
		{
			if (*i == &input)
			{
				linked_inputs.erase(i);
				return *this;
			};
		};
		return *this;
	};

	template<class Message>
	TriggeringInputGate<Message>::TriggeringInputGate(Entity<Message> &master)
		: InputGate<Message>(master)
	{
	};

	template<class Message>
	TriggeringInputGate<Message>::~TriggeringInputGate()
	{
	};

	template<class Message>
	InputGate<Message>& TriggeringInputGate<Message>::operator ()(const Message message)
	{
		this->GetMaster().Lock();
		this->GetMaster().GetMessage(*this, message);
		this->GetMaster().Unlock();
		return *this;
	};


};


};

#endif
