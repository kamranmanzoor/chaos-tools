/****************************************************************************************************/
/* CST Middleware 2008                        * BasicDefinitions.cpp                                  */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Generic interfaces for input/output gates and entities. For inheritance only.                    */
/****************************************************************************************************/
#include "BasicDefinitions.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
