/****************************************************************************************************/
/* CST Middleware 2008                        * Entities.h										    */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* The minimum set of message processing class templates                                            */
/****************************************************************************************************/

#ifndef ENTITIES_H
#define ENTITIES_H

#include "BasicDefinitions.h"
#include "Gates.h"
#include <map>

namespace CSTMiddleWare
{

namespace MessageEngine
{

	/*********************************************************************/
	/* Template Class "Consumer"                                         */
	/* Abstract interface                                                */
	/* Parametrized with "Message"                                       */
	/* Input gates:  "input", reference                                  */
	/* Output gates: none                                                */
	/*********************************************************************/
	/* Provides the abstract interface for all classes consuming         */
	/* messages via single input gate.                                   */
	/* Introduces GetInput() method to obtain input gate for handling    */
	/*********************************************************************/
	template<class Message>
	class Consumer
	{
	private:
		InputGate<Message>&  input;
	public:
		Consumer(InputGate<Message>& _input);
		virtual ~Consumer();
		virtual InputGate<Message>& GetInput();
	};

	/*********************************************************************/
	/* Template Class "Producer"                                         */
	/* Abstract interface                                                */
	/* Parametrized with "Message"                                       */
	/* Input gates:  none                                                */
	/* Output gates: "output", reference                                 */
	/*********************************************************************/
	/* Provides the abstract interface for all classes producing         */
	/* messages via single output gate.                                  */
	/* Introduces GetOutput() method to obtain output gate for handling  */
	/*********************************************************************/
	template<class Message>
	class Producer
	{
	private:
		OutputGate<Message>& output;
	public:
		Producer(OutputGate<Message>& _output);
		virtual ~Producer();
		virtual OutputGate<Message>& GetOutput();
	};

	/*********************************************************************/
	/* Template Class "MultiIn"                                          */
	/* Abstract interface                                                */
	/* Parametrized with "Message", "Selector"                           */
	/* Input gates:  variable size map of input gate pointers,           */
	/*               indexed by value of Selector type                   */
	/* Output gates: none                                                */
	/*********************************************************************/
	/* Provides the abstract interface for all classes consuming         */
	/* messages via multiple input gates (of the same type!).            */
	/* Introduces GetInputGate(Selector) method and operator[](Selector) */
	/* to put and get pointers to the input gates to/from the map.       */
	/* Derived classes must fill the map with their real gates           */
	/*********************************************************************/
	template<class Message, class Selector>
	class MultiIn
	{
	public:
		typedef void (*OnEnumerate)(Selector, void*);
	private:
		std::map<Selector, InputGate<Message>*> in_list;
	public:
		MultiIn();
		virtual ~MultiIn();
		virtual InputGate<Message>*& operator[](Selector s);
		virtual InputGate<Message>*& GetInputGate(Selector s) {return this->operator[](s);};
		virtual void Enumerate(OnEnumerate oe, void* arg)
		{
			for (typename std::map<Selector, InputGate<Message>*>::iterator i = in_list.begin();
				i != in_list.end(); i++)
				oe(i->first, arg);
		};
	};

	/*********************************************************************/
	/* Template Class "MultiOut"                                         */
	/* Abstract interface                                                */
	/* Parametrized with "Message", "Selector"                           */
	/* Input gates: none                                                 */
	/* Output gates: variable size map of output gate pointers,          */
	/*               indexed by value of Selector type                   */
	/*********************************************************************/
	/* Provides the abstract interface for all classes producing         */
	/* messages via multiple output gates (of the same type!).           */
	/* Introduces GetOutputGate(Selector) method and operator[](Selector)*/
	/* to put and get pointers to the output gates to/from the map.      */
	/* Derived classes must fill the map with their real gates           */
	/*********************************************************************/
	template<class Message, class Selector>
	class MultiOut
	{
	public:
		typedef void (*OnEnumerate)(Selector, void*);
	protected:
		std::map<Selector, OutputGate<Message>*> out_list;
	public:
		MultiOut();
		virtual ~MultiOut();
		virtual OutputGate<Message>*& operator[](Selector s);
		virtual OutputGate<Message>*& GetOutputGate(Selector s) {return this->operator[](s);};
		virtual void Enumerate(OnEnumerate oe, void* arg)
		{
			for (typename std::map<Selector, OutputGate<Message>*>::iterator i = out_list.begin();
				i != out_list.end(); i++)
				oe(i->first, arg);
		};
	};

	/*********************************************************************/
	/* Template Class "PassthroughInterface"                             */
	/* Abstract interface                                                */
	/* Parametrized with "Message"				                         */
	/* Input gates: "Input", reference                                   */
	/* Output gates: "Output", reference                                 */
	/*********************************************************************/
	/* Provides the abstract interface for all classes transferring      */
	/* messages of given (one) type through itself with delays or        */
	/* intermediate processing. Methods GetMessage and SendMessage are   */
	/* implemented for direct transfer messages from input to output     */
	/* and must be redefined if another behavior is assumed.             */
	/* Real input and output gates must be provided upon construction.   */
	/*********************************************************************/
	template<class Message>
	class PassthroughInterface : public Consumer<Message>, public Producer<Message>, public Entity<Message>
	{
	public:
		PassthroughInterface(InputGate<Message>& in, OutputGate<Message>& out);
		virtual ~PassthroughInterface();
	protected:
		virtual Entity<Message>& GetMessage(InputGate<Message>& source_address, const Message message);
		virtual Entity<Message>& SendMessage(OutputGate<Message>& distribution_address, const Message message);
	};

	template<class Message>
	class DirectPassthrough : public PassthroughInterface<Message>
	{
	protected:
		TriggeringInputGate<Message>		input;
		DirectOutputGate<Message> 	        output;
	public:
		DirectPassthrough();
		virtual ~DirectPassthrough();
	};

	template<class Message>
	class DirectLatch : public DirectPassthrough<Message>, public Entity<SIGNAL_TYPE>
	{
	protected:
		TriggeringInputGate<SIGNAL_TYPE>		go_signal;
		DirectOutputGate<SIGNAL_TYPE>			busy_status;
		TriggeringInputGate<SIGNAL_TYPE>		reset;
		bool									go_allowed;
		Message*								latch_itself;
	public:
		DirectLatch();
		virtual ~DirectLatch();
		virtual InputGate<SIGNAL_TYPE>& GetSignal();
		virtual OutputGate<SIGNAL_TYPE>& GetStatus();
		virtual InputGate<SIGNAL_TYPE>& GetReset();
	protected:
		virtual Entity<Message>& GetMessage(InputGate<Message>& source_address, const Message message);
		virtual Entity<Message>& SendMessage(OutputGate<Message>& distribution_address, const Message message);
		virtual Entity<SIGNAL_TYPE>& GetMessage(InputGate<SIGNAL_TYPE>& source_address, const SIGNAL_TYPE message);
		virtual Entity<SIGNAL_TYPE>& SendMessage(OutputGate<SIGNAL_TYPE>& distribution_address, const SIGNAL_TYPE message);
	private:
		virtual void Lock() {Entity<Message>::Lock(); Entity<SIGNAL_TYPE>::Lock();};
		virtual void Unlock() {Entity<Message>::Unlock(); Entity<SIGNAL_TYPE>::Unlock();};
	};

	template<class Message>
	class Notifier : public DirectPassthrough<Message>, public Producer<SIGNAL_TYPE>, public Entity<SIGNAL_TYPE>
	{
	private:
		DirectOutputGate<SIGNAL_TYPE>		notification;
	public:
		Notifier();
		virtual ~Notifier();
		virtual OutputGate<SIGNAL_TYPE>& GetNotificationGate(){return notification;};
		virtual OutputGate<Message>& GetMessageOutputGate(){return DirectPassthrough<Message>::GetOutput();};
	protected:
		virtual Entity<SIGNAL_TYPE>& SendMessage(OutputGate<SIGNAL_TYPE>& distribution_address, const SIGNAL_TYPE message);
		virtual Entity<Message>& SendMessage(OutputGate<Message>& distribution_address, const Message message);
	private:
		virtual void Lock() {Entity<Message>::Lock(); Entity<SIGNAL_TYPE>::Lock();};
		virtual void Unlock() {Entity<Message>::Unlock(); Entity<SIGNAL_TYPE>::Unlock();};
	};

	template<class Message>
	class Terminator : public Consumer<Message>, public Entity<Message>
	{
	private:
		TriggeringInputGate<Message> input;
	public:
		Terminator();
		virtual ~Terminator();
	};

	template<class Message>
	class Router : public Consumer<Message>, public Entity<SIGNAL_TYPE>,
		public MultiIn<SIGNAL_TYPE, bool>, public MultiOut<Message, bool>, public Entity<Message>
	{
	protected:
		TriggeringInputGate<Message>		input;
		TriggeringInputGate<SIGNAL_TYPE>	selector_Y;
		TriggeringInputGate<SIGNAL_TYPE>	selector_N;
		DirectOutputGate<Message>			output_Y;
		DirectOutputGate<Message>			output_N;
		bool								selector_state;
	public:
		Router(bool default_state = false);
		virtual ~Router();
	protected:
		virtual Entity<SIGNAL_TYPE>& GetMessage(InputGate<SIGNAL_TYPE>& source_address, const SIGNAL_TYPE message);
		virtual Entity<Message>& GetMessage(InputGate<Message>& source_address, const Message message);
		virtual Entity<Message>& SendMessage(OutputGate<Message>& distribution_address, const Message message);
	private:
		virtual void Lock() {Entity<Message>::Lock(); Entity<SIGNAL_TYPE>::Lock();};
		virtual void Unlock() {Entity<Message>::Unlock(); Entity<SIGNAL_TYPE>::Unlock();};
	};

	template<class Message>
	class Execute : public Consumer<Message>, public Entity<Message>
	{
	private:
		TriggeringInputGate<Message> input;
	public:
		Execute();
		virtual ~Execute();
	protected:
		virtual Entity<Message>& GetMessage(InputGate<Message>& source_address, const Message message);
		virtual void Run(const Message message) = 0;
	};

	template<class Message>
	class Synchronize : public Entity<Message>, public Entity<SIGNAL_TYPE>, public Producer<SIGNAL_TYPE>,
		public MultiIn<Message, bool>, public MultiOut<Message, bool>
	{
	protected:
		TriggeringInputGate<Message>				input_L;
		TriggeringInputGate<Message>				input_R;
		DirectOutputGate<Message>					output_L;
		DirectOutputGate<Message>					output_R;
		DirectOutputGate<SIGNAL_TYPE>				busy_notification;
		Message										latch;
		InputGate<Message>*							latch_source;
	public:
		Synchronize();
		virtual ~Synchronize();
		virtual OutputGate<SIGNAL_TYPE>& GetNotificationGate() {return busy_notification;};
	protected:
		virtual Entity<SIGNAL_TYPE>& SendMessage(OutputGate<SIGNAL_TYPE>& distribution_address, const SIGNAL_TYPE message);
		virtual Entity<Message>& GetMessage(InputGate<Message>& source_address, const Message message);
		virtual Entity<Message>& SendMessage(OutputGate<Message>& distribution_address, const Message message);
	private:
		virtual void Lock() {Entity<Message>::Lock(); Entity<SIGNAL_TYPE>::Lock();};
		virtual void Unlock() {Entity<Message>::Unlock(); Entity<SIGNAL_TYPE>::Unlock();};
	};

	template<class MessageX, class MessageY>
	struct Joint
	{
		MessageX x;
		MessageY y;

		Joint() : x(), y() {}
		Joint( const MessageX& xx, const MessageY& yy ) : x(xx), y(yy) {}
	};

	template<class MessageX, class MessageY>
	class JoinXY : public Consumer<MessageX>, 
		         public Consumer<MessageY>, 
				 public Producer< Joint<MessageX, MessageY> >,
				 public Entity<MessageX>, 
				 public Entity<MessageY>, 
				 public Entity< Joint<MessageX, MessageY> >
	{
	protected:
		TriggeringInputGate<MessageX>					input_X;
		TriggeringInputGate<MessageY>					input_Y;
		DirectOutputGate< Joint<MessageX, MessageY> >	output;
		Joint<MessageX, MessageY>						formed_message;
		bool											x_present;
		bool											y_present;
	public:
		JoinXY();
		virtual ~JoinXY();
		virtual InputGate<MessageX>& GetInputX() {return input_X;};
		virtual InputGate<MessageY>& GetInputY() {return input_Y;};
	protected:
		virtual Entity<MessageX>& GetMessage(InputGate<MessageX>& source_address, const MessageX message);
		virtual Entity<MessageY>& GetMessage(InputGate<MessageY>& source_address, const MessageY message);
		virtual Entity< Joint<MessageX, MessageY> >& SendMessage
			(OutputGate< Joint<MessageX, MessageY> >& distribution_address, 
			 const Joint<MessageX, MessageY> message);
	private:
		virtual void Lock() {Entity<MessageX>::Lock(); Entity<MessageY>::Lock(); Entity< Joint<MessageX, MessageY> >::Lock();};
		virtual void Unlock() {Entity<MessageX>::Unlock(); Entity<MessageY>::Unlock(); Entity< Joint<MessageX, MessageY> >::Unlock();};
	};

	template<class Message>
	class Join : public Producer< Joint<Message, Message> >,
		         public Entity<Message>, 
				 public Entity< Joint<Message, Message> >
	{
	protected:
		TriggeringInputGate<Message>					input_X;
		TriggeringInputGate<Message>					input_Y;
		DirectOutputGate< Joint<Message, Message> >		output;
		Joint<Message, Message>							formed_message;
		bool											x_present;
		bool											y_present;
	public:
		Join();
		virtual ~Join();
		virtual InputGate<Message>& GetInputX()	 {return input_X;};
		virtual InputGate<Message>& GetInputY()  {return input_Y;};
	protected:
		virtual Entity<Message>& GetMessage(InputGate<Message>& source_address, const Message message);
		virtual Entity< Joint<Message, Message> >& SendMessage
			(OutputGate< Joint<Message, Message> >& distribution_address, 
			 const Joint<Message, Message> message);
	private:
		virtual void Lock() {Entity<Message>::Lock(); Entity< Joint<Message, Message> >::Lock();};
		virtual void Unlock() {Entity<Message>::Unlock(); Entity< Joint<Message, Message> >::Unlock();};
	};

	template<class MessageX, class MessageY>
	class SplitXY : 
		public Consumer< Joint<MessageX, MessageY> >, 
		public Producer<MessageX>, 
		public Producer<MessageY>,
		public Entity< Joint<MessageX, MessageY> >,
		public Entity<MessageX>,
		public Entity<MessageY>
	{
	protected:
		TriggeringInputGate< Joint<MessageX, MessageY> > input;
		DirectOutputGate<MessageX>						 output_X;
		DirectOutputGate<MessageY>						 output_Y;
	public:
		SplitXY();
		virtual ~SplitXY();
		virtual OutputGate<MessageX>& GetOutputX() {return output_X;};
		virtual OutputGate<MessageY>& GetOutputY() {return output_Y;};
	protected:
		virtual Entity< Joint<MessageX, MessageY> >& GetMessage
			(InputGate< Joint<MessageX, MessageY> >& source_address, 
			 const Joint<MessageX, MessageY> message);
		virtual Entity<MessageX>& SendMessage(OutputGate<MessageX>& destination_address, const MessageX message);
		virtual Entity<MessageY>& SendMessage(OutputGate<MessageY>& destination_address, const MessageY message);
	private:
		virtual void Lock() {Entity<MessageX>::Lock(); Entity<MessageY>::Lock(); Entity< Joint<MessageX, MessageY> >::Lock();};
		virtual void Unlock() {Entity<MessageX>::Unlock(); Entity<MessageY>::Unlock(); Entity< Joint<MessageX, MessageY> >::Unlock();};
	};

	template<class Message>
	class Split : 
		public Consumer< Joint<Message, Message> >, 
		public Entity< Joint<Message, Message> >,
		public Entity<Message>
	{
	protected:
		TriggeringInputGate< Joint<Message, Message> >	 input;
		DirectOutputGate<Message>						 output_X;
		DirectOutputGate<Message>						 output_Y;
	public:
		Split();
		virtual ~Split();
		virtual OutputGate<Message>& GetOutputX() {return output_X;};
		virtual OutputGate<Message>& GetOutputY() {return output_Y;};
	protected:
		virtual Entity< Joint<Message, Message> >& GetMessage
			(InputGate< Joint<Message, Message> >& source_address, 
			 const Joint<Message, Message> message);
		virtual Entity<Message>& SendMessage(OutputGate<Message>& destination_address, const Message message);
	private:
		virtual void Lock() {Entity<Message>::Lock(); Entity< Joint<Message, Message> >::Lock();};
		virtual void Unlock() {Entity<Message>::Unlock(); Entity< Joint<Message, Message> >::Unlock();};
	};

	template<class MessageIn, class MessageOut>
	class Translator : public Consumer<MessageIn>, public Producer<MessageOut>,
					   public Entity<MessageIn>, public Entity<MessageOut>
	{
	protected:
		TriggeringInputGate<MessageIn> input;
		DirectOutputGate<MessageOut>   output;
	public:
		Translator();
		virtual ~Translator();
	protected:
		virtual MessageOut operator()(const MessageIn src_msg) = 0;
	protected:
		virtual Entity<MessageIn>& GetMessage(InputGate<MessageIn>& source_address, const MessageIn message);
		virtual Entity<MessageOut>& SendMessage(OutputGate<MessageOut>& destination_address, const MessageOut message);
	private:
		virtual void Lock() {Entity<MessageIn>::Lock(); Entity<MessageOut>::Lock();};
		virtual void Unlock() {Entity<MessageIn>::Unlock(); Entity<MessageOut>::Unlock();};
	};

	template<class Message>
	class Function :   public DirectPassthrough<Message>
	{
	public:
		Function();
		virtual ~Function();
	protected:
		// This method to be redefined by derivatives
		virtual Message operator()(const Message src_msg) = 0;
	protected:
		virtual Entity<Message>& GetMessage(InputGate<Message>& source_address, const Message message);
	};

	template<class Message>
	class Sequencer : public Execute<Message>
	{
	private:
		DirectOutputGate<Message> out_a;
		DirectOutputGate<Message> out_b;
	public:
		Sequencer();
		virtual ~Sequencer();
	public:
		virtual OutputGate<Message>& GetFirstOutput()  {return out_a;};
		virtual OutputGate<Message>& GetSecondOutput() {return out_b;};
	protected:
		virtual void Run(const Message);
	};

	/**********************************************************************************************************/
	/************************************** I M P L E M E N T A T I O N ***************************************/
	/**********************************************************************************************************/

	template<class Message>
	Consumer<Message>::Consumer(InputGate<Message>& _input) : input(_input)
	{
	};

	template<class Message>
	Consumer<Message>::~Consumer()
	{
	};

	template<class Message>
	InputGate<Message>& Consumer<Message>::GetInput()
	{
		return input;
	};

	template<class Message>
	Producer<Message>::Producer(OutputGate<Message>& _output) : output(_output)
	{
	};

	template<class Message>
	Producer<Message>::~Producer()
	{
	};

	template<class Message>
	OutputGate<Message>& Producer<Message>::GetOutput()
	{
		return output;
	};

	template<class Message>
	PassthroughInterface<Message>::PassthroughInterface(InputGate<Message> &in, 
							 OutputGate<Message> &out)
							 : Consumer<Message>(in), Producer<Message>(out)
	{
	};

	template<class Message>
	PassthroughInterface<Message>::~PassthroughInterface()
	{
	};

	template<class Message>
	Entity<Message>& PassthroughInterface<Message>::GetMessage(InputGate<Message> &source_address, 
									const Message message)
	{
		if (&source_address != &this->GetInput())
			throw int(0);
		SendMessage(this->GetOutput(), message);
		return *this;
	};

	template<class Message>
	Entity<Message>& PassthroughInterface<Message>::SendMessage(OutputGate<Message> &distribution_address, 
									 const Message message)
	{
		if (&distribution_address != &this->GetOutput())
			throw int(0);
		distribution_address.operator()(message);
		return *this;
	};

	template<class Message>
	DirectPassthrough<Message>::DirectPassthrough() 
		: input(*this), output(*this), PassthroughInterface<Message>(input, output)
	{
	};

	template<class Message>
	DirectPassthrough<Message>::~DirectPassthrough()
	{
	};

	template<class Message>
	DirectLatch<Message>::DirectLatch() 
		:	go_signal(*this),
			busy_status(*this), 
			latch_itself(NULL), 
			reset(*this)
	{
		go_allowed = false;
	};

	template<class Message>
	DirectLatch<Message>::~DirectLatch()
	{
		if (latch_itself)
		{
			delete latch_itself;
		};
	};

	template<class Message>
	Entity<Message>& DirectLatch<Message>::GetMessage(InputGate<Message>& source_address, const Message message)
	{
		if (&source_address == &this->GetInput())
		{
			if (latch_itself)
			{
				SendMessage(this->GetStatus(), SIGNAL);
			}
			else
			{
				if (go_allowed)
				{
					go_allowed = false;
					SendMessage(this->GetOutput(), message);
				}
				else
				{
					latch_itself = new Message(message);
				};
			};
			return *this;
		}
		else throw int(0);
	};

	template<class Message>
	Entity<Message>& DirectLatch<Message>::SendMessage(OutputGate<Message>& distribution_address, 
														const Message message)
	{
		if (&distribution_address == &this->GetOutput())
			return DirectPassthrough<Message>::SendMessage(distribution_address, message);
		else throw int(0);
	};

	template<class Message>
	Entity<SIGNAL_TYPE>& DirectLatch<Message>::GetMessage(InputGate<SIGNAL_TYPE>& source_address, 
															const SIGNAL_TYPE message)
	{
		if (&GetSignal() == &source_address)
		{
			if (latch_itself)
			{
				Message* temp_storage = latch_itself;
				latch_itself = NULL;
				go_allowed = false;
				SendMessage(this->GetOutput(), *temp_storage);
				delete temp_storage;
			}
			else
			{
				go_allowed = true;
			};
			return *this;
		}
		else if (&GetReset() == &source_address)
		{
			go_allowed = false;
			return *this;
		}
		else throw int(0);
	};

	template<class Message>
	Entity<SIGNAL_TYPE>& DirectLatch<Message>::SendMessage(OutputGate<SIGNAL_TYPE>& distribution_address, const SIGNAL_TYPE message)
	{
		if (&GetStatus() == &distribution_address)
		{
			GetStatus().operator()(message);
			return *this;
		}
		else throw int(0);
	};

	template<class Message>
	InputGate<SIGNAL_TYPE>& DirectLatch<Message>::GetSignal() 
	{
		return this->go_signal;
	};

	template<class Message>
	OutputGate<SIGNAL_TYPE>& DirectLatch<Message>::GetStatus() 
	{
		return this->busy_status;
	};

	template<class Message>
	InputGate<SIGNAL_TYPE>& DirectLatch<Message>::GetReset()
	{
		return this->reset;
	};

	template<class Message>
	Notifier<Message>::Notifier()
		: notification(*this), Producer<SIGNAL_TYPE>(notification)
	{
	};

	template<class Message>
	Notifier<Message>::~Notifier()
	{
	};

	template<class Message>
	Entity<SIGNAL_TYPE>& Notifier<Message>::SendMessage(OutputGate<SIGNAL_TYPE>& distribution_address, 
														const SIGNAL_TYPE message)
	{
		if (&GetNotificationGate() == &distribution_address)
		{
			GetNotificationGate().operator()(message);
			return *this;
		}
		else throw int(0);
	};

	template<class Message>
	Entity<Message>& Notifier<Message>::SendMessage(OutputGate<Message>& distribution_address, 
													const Message message)
	{
		SendMessage(GetNotificationGate(), SIGNAL);
		return DirectPassthrough<Message>::SendMessage(distribution_address, message);
	};

	template<class Message>
	Terminator<Message>::Terminator() : input(*this), Consumer<Message>(input)
	{
	};

	template<class Message>
	Terminator<Message>::~Terminator()
	{
	};

	template<class Message, class Selector>
	MultiIn<Message, Selector>::MultiIn()
	{
	};

	template<class Message, class Selector>
	MultiIn<Message, Selector>::~MultiIn()
	{
	};

	template<class Message, class Selector>
	InputGate<Message>*& MultiIn<Message, Selector>::operator[](Selector s)
	{
		InputGate<Message>*& result = this->in_list[s];
		return result;
	};

	template<class Message, class Selector>
	MultiOut<Message, Selector>::MultiOut()
	{
	};

	template<class Message, class Selector>
	MultiOut<Message, Selector>::~MultiOut()
	{
	};

	template<class Message, class Selector>
	OutputGate<Message>*& MultiOut<Message, Selector>::operator[](Selector s)
	{
		OutputGate<Message>*& result = this->out_list[s];
		return result;
	};

	template<class Message>
	Router<Message>::Router(bool default_state) 
		:	input(*this),
			output_Y(*this),
			output_N(*this),
			selector_Y(*this),
			selector_N(*this),
			Consumer<Message>(input)
	{
		this->GetOutputGate(true) = &output_Y;
		this->GetOutputGate(false) = &output_N;
		this->GetInputGate(true) = &selector_Y;
		this->GetInputGate(false) = &selector_N;
		selector_state = default_state;
	};

	template<class Message>
	Router<Message>::~Router()
	{
	};

	template<class Message>
	Entity<SIGNAL_TYPE>& Router<Message>::GetMessage(InputGate<SIGNAL_TYPE>& source_address, 
													 const SIGNAL_TYPE message)
	{
		if (&source_address == &selector_N)
			selector_state = false;
		else if (&source_address == &selector_Y)
			selector_state = true;
		else
		{
			throw int(0);
		};
		return *this;
	};

	template<class Message>
	Entity<Message>& Router<Message>::GetMessage(InputGate<Message>& source_address, const Message message)
	{
		bool actual_state = selector_state;
		if (actual_state == false)
			return SendMessage(output_N, message);
		else
			return SendMessage(output_Y, message);
	};

	template<class Message>
	Entity<Message>& Router<Message>::SendMessage(OutputGate<Message>& distribution_address, const Message message)
	{
		if ((&distribution_address != &output_N) &&
			(&distribution_address != &output_Y))
			throw int(0);
		distribution_address.operator()(message);
		return *this;
	};

	template<class Message>
	Execute<Message>::Execute() : input(*this), Consumer<Message>(input)
	{
	};

	template<class Message>
	Execute<Message>::~Execute()
	{
	};

	template<class Message>
	Entity<Message>& Execute<Message>::GetMessage(InputGate<Message>& source_address, const Message message)
	{
		if (&source_address == &input)
			Run(message);
		else 
			throw int(0);
		return *this;
	};

	template<class Message>
	Synchronize<Message>::Synchronize()
		: input_L(*this), input_R(*this), output_L(*this), output_R(*this), 
		busy_notification(*this), Producer<SIGNAL_TYPE>(busy_notification)
	{
		this->GetOutputGate(true) = &output_R;
		this->GetOutputGate(false)= &output_L;
		this->GetInputGate(true)  = &input_R;
		this->GetInputGate(false) = &input_L;
		latch_source = NULL;
	};

	template<class Message>
	Synchronize<Message>::~Synchronize()
	{
	};

	template<class Message>
	Entity<SIGNAL_TYPE>& Synchronize<Message>::SendMessage(OutputGate<SIGNAL_TYPE>& distribution_address, 
		const SIGNAL_TYPE message)
	{
		if (&distribution_address == &GetNotificationGate())
			GetNotificationGate().operator ()(message);
		else throw int(0);
		return *this;
	};

	template<class Message>
	Entity<Message>& Synchronize<Message>::GetMessage(InputGate<Message>& source_address, 
		const Message message)
	{
		if (NULL == latch_source)
		{
			latch = message;
			latch_source = &source_address;
		}
		else if (latch_source == &source_address)
		{
			SendMessage(GetNotificationGate(), SIGNAL);
		}
		else
		{
			if (latch_source == &input_R)
			{
				latch_source = NULL;
				SendMessage(output_R, latch);
				SendMessage(output_L, message);
			};
			if (latch_source == &input_L)
			{
				latch_source = NULL;
				SendMessage(output_L, latch);
				SendMessage(output_R, message);
			};
		};
		return *this;
	};

	template<class Message>
	Entity<Message>& Synchronize<Message>::SendMessage(OutputGate<Message>& distribution_address, 
		const Message message)
	{
		if ((&distribution_address != &output_L) &&
			(&distribution_address != &output_R))
			throw int(0);
		distribution_address.operator ()(message);
		return *this;
	};

	template<class MessageX, class MessageY>
	JoinXY<MessageX, MessageY>::JoinXY()
		:	input_X(*this), input_Y(*this), output(*this),
			Consumer<MessageX>(input_X), Consumer<MessageY>(input_Y),
			Producer< Joint<MessageX, MessageY> >(output)
	{
		x_present = false;
		y_present = false;
	};

	template<class MessageX, class MessageY>
	JoinXY<MessageX, MessageY>::~JoinXY()
	{
	};

	template<class MessageX, class MessageY>
	Entity<MessageX>& JoinXY<MessageX, MessageY>::GetMessage(InputGate<MessageX>& source_address, 
															const MessageX message)
	{
		if (&source_address != &GetInputX())
			throw int(0);
		formed_message->x = message;
		x_present = true;
		if (y_present)
		{
			x_present = y_present = false;
			SendMessage(this->GetOutput(), formed_message);
		};
		return *this;
	};

	template<class MessageX, class MessageY>
	Entity<MessageY>& JoinXY<MessageX, MessageY>::GetMessage(InputGate<MessageY>& source_address, 
															const MessageY message)
	{
		if (&source_address != &GetInputY())
			throw int(0);
		formed_message->y = message;
		y_present = true;
		if (x_present)
		{
			x_present = y_present = false;
			SendMessage(this->GetOutput(), formed_message);
		};
		return *this;
	};

	template<class MessageX, class MessageY>
	Entity< Joint<MessageX, MessageY> >& JoinXY<MessageX, MessageY>::SendMessage
			(OutputGate< Joint<MessageX, MessageY> >& distribution_address, 
			 const Joint<MessageX, MessageY> message)
	{
		if (&distribution_address != &this->GetOutput())
			throw int(0);
		this->GetOutput().operator()(message);
		return *this;
	};

	template<class Message>
	Join<Message>::Join()
		:	input_X(*this), input_Y(*this), output(*this),
			Producer< Joint<Message, Message> >(output)
	{
		x_present = false;
		y_present = false;
	};

	template<class Message>
	Join<Message>::~Join()
	{
	};

	template<class Message>
	Entity<Message>& Join<Message>::GetMessage(InputGate<Message>& source_address, 
															const Message message)
	{
		if (&source_address == &GetInputX())
		{
			formed_message->x = message;
			x_present = true;
			if (y_present)
			{
				x_present = y_present = false;
				SendMessage(this->GetOutput(), formed_message);
			};
		}
		else if (&source_address == &GetInputY())
		{
			formed_message->y = message;
			y_present = true;
			if (x_present)
			{
				x_present = y_present = false;
				SendMessage(this->GetOutput(), formed_message);
			};
		}
		else throw int(0);
		return *this;
	};

	template<class Message>
	Entity< Joint<Message, Message> >& Join<Message>::SendMessage
			(OutputGate< Joint<Message, Message> >& distribution_address, 
			 const Joint<Message, Message> message)
	{
		if (&distribution_address != &this->GetOutput())
			throw int(0);
		this->GetOutput().operator()(message);
		return *this;
	};

	template<class MessageX, class MessageY>
	SplitXY<MessageX, MessageY>::SplitXY()
		: input(*this), output_X(*this), output_Y(*this),
		  Consumer< Joint<MessageX, MessageY> >(input),
		  Producer<MessageX>(output_X),
		  Producer<MessageY>(output_Y)
	{
	};

	template<class MessageX, class MessageY>
	SplitXY<MessageX, MessageY>::~SplitXY()
	{
	};

	template<class MessageX, class MessageY>
	Entity< Joint<MessageX, MessageY> >& SplitXY<MessageX, MessageY>::GetMessage
		(InputGate< Joint<MessageX, MessageY> >& source_address, 
		 const Joint<MessageX, MessageY> message)
	{
		if (&source_address != &this->GetInput())
			throw int(0);
		SendMessage(GetOutputX(), message.x);
		SendMessage(GetOutputY(), message.y);
		return *this;
	};

	template<class MessageX, class MessageY>
	Entity<MessageX>& SplitXY<MessageX, MessageY>::SendMessage(OutputGate<MessageX>& destination_address, 
		const MessageX message)
	{
		if (&destination_address == &GetOutputX())
			destination_address.operator ()(message);
		else throw int(0);
		return *this;
	};

	template<class MessageX, class MessageY>
	Entity<MessageY>& SplitXY<MessageX, MessageY>::SendMessage(OutputGate<MessageY>& destination_address, 
		const MessageY message)
	{
		if (&destination_address == &GetOutputY())
			destination_address.operator ()(message);
		else throw int(0);
		return *this;
	};

	template<class Message>
	Split<Message>::Split()
		: input(*this), output_X(*this), output_Y(*this),
		  Consumer< Joint<Message, Message> >(input)
	{
	};

	template<class Message>
	Split<Message>::~Split()
	{
	};

	template<class Message>
	Entity< Joint<Message, Message> >& Split<Message>::GetMessage
		(InputGate< Joint<Message, Message> >& source_address, 
		 const Joint<Message, Message> message)
	{
		if (&source_address != &this->GetInput())
			throw int(0);
		SendMessage(GetOutputX(), message.x);
		SendMessage(GetOutputY(), message.y);
		return *this;
	};

	template<class Message>
	Entity<Message>& Split<Message>::SendMessage(OutputGate<Message>& destination_address, 
		const Message message)
	{
		if (&destination_address == &GetOutputX())
			destination_address.operator ()(message);
		else if (&destination_address == &GetOutputY())
			destination_address.operator ()(message);
		else throw int(0);
		return *this;
	};

	template<class MessageIn, class MessageOut>
	Translator<MessageIn, MessageOut>::Translator()
		: input(*this), output(*this),
		  Consumer<MessageIn>(input), Producer<MessageOut>(output)
	{
	};

	template<class MessageIn, class MessageOut>
	Translator<MessageIn, MessageOut>::~Translator()
	{
	};

	template<class MessageIn, class MessageOut>
	Entity<MessageIn>& Translator<MessageIn, MessageOut>::GetMessage(InputGate<MessageIn>& source_address, 
																	const MessageIn message)
	{
		if (&source_address != &this->GetInput())
			throw int(0);
		SendMessage(this->GetOutput(), operator()(message));
		return *this;
	};

	template<class MessageIn, class MessageOut>
	Entity<MessageOut>& Translator<MessageIn, MessageOut>::SendMessage(OutputGate<MessageOut>& destination_address, 
																	const MessageOut message)
	{
		if (&destination_address != &this->GetOutput())
			throw int(0);
		destination_address.operator()(message);
		return *this;
	};

	template<class Message>
	Function<Message>::Function()
	{
	};

	template<class Message>
	Function<Message>::~Function()
	{
	};

	template<class Message>
	Entity<Message>& Function<Message>::GetMessage(InputGate<Message>& source_address, const Message message)
	{
		if (&source_address != &this->GetInput())
			throw int(0);
		this->SendMessage(this->GetOutput(), operator()(message));
		return *this;
	};

	template<class Message>
	Sequencer<Message>::Sequencer() : out_a(*this), out_b(*this)
	{
	};

	template<class Message>
	Sequencer<Message>::~Sequencer()
	{
	};

	template<class Message>
	void Sequencer<Message>::Run(const Message source)
	{
		SendMessage(out_a, source);
		SendMessage(out_b, source);
	};

};

};

#endif
