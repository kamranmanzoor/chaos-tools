/***************************************************************************************/
/* Componentailty open software: xJSON                * (C) Componentality Oy, 2013    */
/***************************************************************************************/
/* Command line converter from JSON to xJSON and vice versa                            */
/* Use as a sample only due to it always stores vocabulary inline with xJSON data      */
/***************************************************************************************/

#include "../libXjson/xjson_conv.h"
#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
	if (argc < 4)
	{
		std::cout << "xjson (text2bin|bin2text) <input file> <output file>" << std::endl;
		return 1;
	};
	std::ifstream ifile(argv[2], std::ios::binary);
	ifile.seekg(0, std::ios::end);
	size_t size = (size_t) ifile.tellg();
	ifile.seekg(0);
	char* buf = new char[size];
	ifile.read(buf, size);
	ifile.close();
	std::string input;
	for (size_t i = 0; i < size; i++)
		input += buf[i];
	delete[] buf;
	std::string output;
	if (std::string("text2bin") == argv[1])
	{
		CST::Common::xJSON::JSONtoBIN j2b;
		size_t begin = 0;
		size_t end = input.size();
		output = j2b.serialize() + j2b(input, begin, end);
	}
	if (std::string("bin2text") == argv[1])
	{
		CST::Common::xJSON::BINtoJSON b2j;
		output = b2j(input);
	}
	std::ofstream ofile(argv[3], std::ios::binary);
	ofile.write(output.c_str(), output.size());
	ofile.close();
	return 0;
}

