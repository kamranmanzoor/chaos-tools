/****************************************************************************************************/
/* CST Middleware 2009                        * CompoSHA.cpp										*/
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* Componentality adapter for SHA1 open source code.												*/
/****************************************************************************************************/
#include "CompoSHA.h"
#include "sha1.h"
#include <memory.h>

using namespace CST::Common;

SHA_Converter::SHA_Converter()
{
};

SHA_Converter::~SHA_Converter()
{
};

DIGEST SHA_Converter::operator()(const blob src)
{
	::SHA1 processor;
	processor.Input((unsigned char*)(src.mData), src.mSize);
	DIGEST result;
	processor.Result(&result.content[0]);
	return result;
};

std::string CST::Common::SHA_serialize(DIGEST& digest)
{
	std::string result;
	for (int i = 0; i < 5; i++)
	{
		result += (char) ((digest.content[i] >> 0) & 0xFF);
		result += (char) ((digest.content[i] >> 8) & 0xFF);
		result += (char) ((digest.content[i] >> 16) & 0xFF);
		result += (char) ((digest.content[i] >> 24) & 0xFF);
	};
	return result;
};

DIGEST CST::Common::SHA_deserialize(const std::string& str)
{
	DIGEST result;
	memset(&result, 0, sizeof(DIGEST));
	if (str.size() != 20)
		return result;
	for (int i = 0; i < 5; i++)
	{
		unsigned int x;
		x = (unsigned char)str[i * 4 + 0];
		result.content[i] |= x << 0;
		x = (unsigned char)str[i * 4 + 1];
		result.content[i] |= x << 8;
		x = (unsigned char)str[i * 4 + 2];
		result.content[i] |=  x << 16;
		x = (unsigned char)str[i * 4 + 3];
		result.content[i] |=  x << 24;
	};
	return result;
};