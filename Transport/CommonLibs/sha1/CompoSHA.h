/****************************************************************************************************/
/* CST Middleware 2009                        * CompoSHA.h											*/
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* Componentality adapter for SHA1 open source code.												*/
/****************************************************************************************************/
#ifndef COMPO_SHA_H
#define COMPO_SHA_H

#include "../common/common_utilities.h"
#include <string>

namespace CST
{
	namespace Common
	{

		typedef struct _DIGEST {unsigned int content[5];} DIGEST;

		std::string SHA_serialize(DIGEST&);
		DIGEST SHA_deserialize(const std::string&);

		// Component creates SHA1 digest for the given message
		class SHA_Converter
		{
		public:
			SHA_Converter();
			virtual ~SHA_Converter();
		public:
			virtual DIGEST operator()(const blob);
		};

	}; // namespace SHA1

}; // namespace Crypto

#endif