/****************************************************************************************************/
/* CST Middleware 2009                        * JSON.h											    */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/* Copyright (C) Componentality Oy, 2012-2013                                                       */
/* Copyright (C) Konstantin Khait, 2014                                                             */
/****************************************************************************************************/
/* Easy JSON parser and composer.	                    											*/
/****************************************************************************************************/

#ifndef JSON_H
#define JSON_H

#include <string>
#include <vector>
#include "Parseable.h"
#include "Serializable.h"

namespace CST
{

	namespace Common
	{

		namespace LibJSON
		{

			// Basic JSON object prototype. Base class for all JSON DOM components
			class JsonObject: public Parseable, public Serializable
			{
			public:
				// Type of JSON objects
				enum JSON_TYPE
				{
					OBJECT,				// List of pairs in {}
					ARRAY,				// List of items in []
					STRING,				// Quoted string
					NUMBER				// Everything else: numbers, booleans, null's
				};
			protected:
				JSON_TYPE type;
			public:
				JsonObject(const JSON_TYPE _type) : type(_type) {};
				virtual ~JsonObject() {};
				virtual JSON_TYPE GetObjectType() const {return type;};
			public:
				// Object parsing. Returns parsing start, end and parsed object. Can be used for parsing
				// of JSON object of unknown yet type
				// Source string and parsing boundaries are to be provided
				virtual std::pair<std::pair<size_t, size_t>, JsonObject*> ParseObj(const std::string&, const size_t start, const size_t end);
			};

			// JSON string
			class String : public JsonObject
			{
			protected:
				std::string value;
			public:
				String(const std::string = "");
				virtual ~String();
				virtual void SetValue(const std::string);
				virtual std::string GetValue() const;
			public:
				virtual std::pair<size_t, size_t> Parse(const std::string&, const size_t, const size_t);
				virtual std::string Serialize(const std::map<std::string, std::string> options);
			};

			// JSON number, including boolean and null's
			class Number : public JsonObject
			{
			public:
				enum TYPE
				{
					INTEGER,
					FLOAT,
					BOOLEAN,
					NULLVAL
				};
				union VALUE
				{
					long	integer_value;
					double	float_value;
					bool    boolean_value;
				};
			protected:
				TYPE	type;
				VALUE	value;
			public:
				Number();
				Number(const VALUE, const TYPE);
				virtual ~Number();
				virtual TYPE GetType() const;
				virtual VALUE GetValue() const;
				virtual void Set(const VALUE, const TYPE);
			public:
				virtual std::pair<size_t, size_t> Parse(const std::string&, const size_t, const size_t);
				virtual std::string Serialize(const std::map<std::string, std::string> options);
			};

			// JSON object
			class Object : public JsonObject
			{
			protected:
				std::map<std::string, JsonObject*> objlist;
			public:
				Object();
				virtual ~Object();
				virtual const std::map<std::string, JsonObject*> GetContent() const;
				virtual void AddObject(const std::string, JsonObject* const);
				virtual void RemoveObject(const std::string);
				virtual JsonObject* operator[](const std::string) const;
			public:
				virtual std::pair<size_t, size_t> Parse(const std::string&, const size_t, const size_t);
				virtual std::string Serialize(const std::map<std::string, std::string> options);
			};

			// JSON array
			class Array : public JsonObject
			{
			protected:
				std::vector<JsonObject*> objlist;
			public:
				Array();
				Array(JsonObject* const, ...);
				virtual ~Array();
			public:
				virtual const std::vector<JsonObject*> GetContent() const;
				virtual void Push(JsonObject* const);
				virtual JsonObject* operator[](const size_t);
				virtual size_t Size() const { return objlist.size(); }
			public:
				virtual std::pair<size_t, size_t> Parse(const std::string&, const size_t, const size_t);
				virtual std::string Serialize(const std::map<std::string, std::string> options);
			};

		}; // namespace LibJSON

	}; // namespace Common

}; // namespace CST


#endif