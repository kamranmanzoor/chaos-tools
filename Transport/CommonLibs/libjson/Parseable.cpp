/****************************************************************************************************/
/* CST Middleware 2009                        * Parseable.cpp									    */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/* Copyright (C) Componentality Oy, 2012-2013                                                       */
/* Copyright (C) Konstantin Khait, 2014                                                             */
/****************************************************************************************************/
/* Abstract description for entity to parse.               											*/
/****************************************************************************************************/

#include "Parseable.h"

using namespace CST::Common::LibJSON;

std::string Parseable::SkipSpaces(const std::string src)
{
	size_t left_counter;
	size_t right_counter;

	for (left_counter = 0; left_counter < src.size(); left_counter++)
		if (src[left_counter] > ' ')
			break;
	for (right_counter = src.size(); right_counter > 0; right_counter--)
		if (src[right_counter - 1] > ' ')
			break;

	return src.substr(left_counter, right_counter - left_counter);
};

Parseable::Parseable()
{
};

Parseable::~Parseable()
{
};

const std::pair<size_t, size_t> Parseable::ERROR_VAL = std::pair<size_t, size_t>((size_t)-1, (size_t)-1);