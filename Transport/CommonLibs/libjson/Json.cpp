/****************************************************************************************************/
/* CST Middleware 2009                        * JSON.cpp										    */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/* Copyright (C) Componentality Oy, 2012-2013                                                       */
/* Copyright (C) Konstantin Khait, 2014                                                             */
/****************************************************************************************************/
/* Easy JSON parser and composer.	                    											*/
/****************************************************************************************************/


#include "Json.h"
#include "strstream"

using namespace CST::Common::LibJSON;

String::String(const std::string src) : JsonObject(STRING), value(src)
{
};

String::~String()
{
};

void String::SetValue(const std::string src)
{
	value = src;
};

std::string String::GetValue() const
{
	return value;
};

std::pair<size_t, size_t> String::Parse(const std::string& src, const size_t lbound, const size_t rbound)
{
	enum STATE
	{
		LSPACES,
		RSPACES,
		BODY,
		ESC,
		LQUOTE
	} state = LSPACES;

	value.clear();
	size_t i = lbound;
	for (; i < rbound; i++)
	{
		switch (state)
		{
		case LSPACES:
			{
				if (src[i] > ' ')
				{
					i--;
					state = LQUOTE;
				};
			};
			break;
		case LQUOTE:
			{
				if (src[i] != '\"')
					return ERROR_VAL;
				else
					state = BODY;
			};
			break;
		case RSPACES:
			{
				if (src[i] > ' ')
					return std::pair<size_t, size_t>(lbound, i);
			};
			break;
		case BODY:
			{
				if (src[i] == '\"')
					state = RSPACES;
				else
					if (src[i] == '\\')
						state = ESC;
					else
						value += src[i];
			};
			break;
		case ESC:
			{
				switch (src[i])
				{
				case 'n':
					value += '\n';
					break;
				case 'r':
					value += '\r';
					break;
				case 't':
					value += '\t';
					break;
				default:
					value += src[i];
				};
				state = BODY;
			};
			break;
		default:
			return ERROR_VAL;
		}; // switch (state)
	};

	return std::pair<size_t, size_t>(lbound, rbound);
};

std::string String::Serialize(const std::map<std::string, std::string>)
{
	std::string result;

	for (size_t i = 0; i < value.size(); i++)
	{
		switch (value[i])
		{
		case '\n':
			result += "\\n";
			break;
		case '\r':
			result += "\\r";
			break;
		case '\t':
			result += "\\t";
			break;
		case '\"':
			result += "\\\"";
			break;
		case '\\':
			result += "\\\\";
			break;
		default:
			result += value[i];
		};
	};
	return '\"' + result + '\"';
};

////////////////////////////////////////////////////////////////////////////////////

Number::Number() : JsonObject(NUMBER)
{
	type = FLOAT;
	value.float_value = 0.0;
};

Number::Number(const VALUE value, const TYPE type) : JsonObject(NUMBER)
{
	this->value = value;
	this->type = type;
};

Number::~Number()
{
};

Number::TYPE Number::GetType() const
{
	return type;
};

Number::VALUE Number::GetValue() const
{
	return value;
};

void Number::Set(const VALUE value, const TYPE type)
{
	this->value = value;
	this->type  = type;
};
				
std::pair<size_t, size_t> Number::Parse(const std::string& src, const size_t lbound, const size_t rbound)
{
	size_t _lbound = lbound;
	bool negative = false;
	std::string sub;

	for (;src[_lbound] <= ' '; _lbound++);

	switch (src[_lbound])
	{
	case 'n':
		sub = src.substr(_lbound, 4);
		if (sub == "null")
		{
			type = NULLVAL;
			return std::pair<size_t, size_t>(_lbound, _lbound + 4);
		}
		else
			return ERROR_VAL;
	case 't':
		sub = src.substr(_lbound, 4);
		if (sub == "true")
		{
			type = BOOLEAN;
			value.boolean_value = true;
			return std::pair<size_t, size_t>(_lbound, _lbound + 4);
		}
		else
			return ERROR_VAL;
	case 'f':
		sub = src.substr(_lbound, 5);
		if (sub == "false")
		{
			type = BOOLEAN;
			value.boolean_value = false;
			return std::pair<size_t, size_t>(_lbound, _lbound + 5);
		}
		else
			return ERROR_VAL;
	case '-':
		negative = true;
	case '+':
		type = INTEGER;
		value.integer_value = 0;
		break;
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		type = INTEGER;
		value.integer_value = src[_lbound] - '0';
		break;
	default:
		return ERROR_VAL;
	};
	
	bool decimal = false;
	size_t dec_cnt = 0;

	_lbound += 1;
	bool exit = false;

	for (; !exit && (_lbound < rbound); _lbound++)
	{
		switch (src[_lbound])
		{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			if (!decimal)
				value.integer_value = value.integer_value * 10 + (src[_lbound] - '0');
			else
			{
				double add = 0.1;
				for (size_t i = 0; i < dec_cnt; i++)
					add = add / 10;
				dec_cnt += 1;
				value.float_value += (double) (src[_lbound] - '0') * add; 
			};
			break;
		case '.':
			type = FLOAT;
			value.float_value = value.integer_value;
			decimal = true;
			break;
		default:
			exit = true;
			_lbound -= 1;
		};
	};

	if (negative)
		if (type == INTEGER)
			value.integer_value *= -1;
		else
			value.float_value *= -1.0;

	return std::pair<size_t, size_t>(lbound, _lbound);
};

std::string Number::Serialize(const std::map<std::string, std::string> options)
{
	std::strstream sstream;
	switch (type)
	{
	case NULLVAL:
		return "null";
	case BOOLEAN:
		if (value.boolean_value)
			return "true";
		else
			return "false";
	case INTEGER:
		sstream << value.integer_value;
		break;
	case FLOAT:
		sstream.precision(12);
		sstream << value.float_value;
		break;
	default:
		return "";
	};
	
	sstream.flush();
	sstream.seekg(0, std::ios::end);
	std::streamoff size = sstream.tellg();
	sstream.seekg(0);
	if (size == 0)
		return "";
	char* buf = new char[(size_t)size + 1];
	buf[size] = 0;
	sstream.read(buf, size);
	std::string result(buf);
	delete[] buf;
	return result;
};

/////////////////////////////////////////////////////////////////////////////////////////

Array::Array() : JsonObject(ARRAY)
{
};

Array::~Array()
{
	for (std::vector<JsonObject*>::iterator i = objlist.begin(); i != objlist.end(); i++)
		delete *i;
};

const std::vector<JsonObject*> Array::GetContent() const
{
	return objlist;
};

void Array::Push(JsonObject* const add)
{
	objlist.push_back(add);
};

JsonObject* Array::operator[](const size_t idx)
{
	if (idx < objlist.size())
		return objlist[idx];
	else
		return NULL;
};

std::pair<size_t, size_t> Array::Parse(const std::string& src, const size_t lbound, const size_t rbound)
{
	size_t _lbound = lbound;

	for (; (src[_lbound] <= ' ') && (_lbound < rbound); _lbound++);

	if (src[_lbound++] != '[')
		return ERROR_VAL;
	
	for (; (src[_lbound] <= ' ') && (_lbound < rbound); _lbound++);
	
	if (src[_lbound] == ']') 
		return std::pair<size_t, size_t>(lbound, _lbound + 1);

	while (_lbound < rbound)
	{
		std::pair<std::pair<size_t, size_t>, JsonObject*> intermediate_result = ParseObj(src, _lbound, rbound);
		if (intermediate_result.first == ERROR_VAL)
			return ERROR_VAL;

		Push(intermediate_result.second);
		_lbound = intermediate_result.first.second;

		for (; (src[_lbound] <= ' ') && (_lbound < rbound); _lbound++);

		if (src[_lbound] == ',')
			_lbound += 1;
		else
			if (src[_lbound] == ']')
				break;
			else
				return ERROR_VAL;

		for (; (src[_lbound] <= ' ') && (_lbound < rbound); _lbound++);
	};

	return std::pair<size_t, size_t>(lbound, _lbound + 1);
};

std::string Array::Serialize(const std::map<std::string, std::string> options)
{
	std::string result = "[";

	for (std::vector<JsonObject*>::iterator i = objlist.begin(); i != objlist.end(); i++)
	{
		if (i != objlist.begin())
			result += ',';
		result += (*i)->Serialize(options);
	};
	result += ']';
	return result;
};

///////////////////////////////////////////////////////////////////////////////////

Object::Object() : JsonObject(OBJECT)
{
};

Object::~Object()
{
	for (std::map<std::string, JsonObject*>::iterator i = objlist.begin(); i != objlist.end(); i++)
		delete i->second;
};

const std::map<std::string, JsonObject*> Object::GetContent() const
{
	return objlist;
};

void Object::AddObject(const std::string key, JsonObject* const value)
{
	objlist[key] = value;
};

void Object::RemoveObject(const std::string key)
{
	if (objlist.find(key) != objlist.end())
		objlist.erase(objlist.find(key));
};

JsonObject* Object::operator[](const std::string key) const
{
	if (objlist.find(key) != objlist.end())
		return objlist.find(key)->second;
	else
		if (objlist.find("\"" + key + "\"") != objlist.end())
			return objlist.find("\"" + key + "\"")->second;
		else
			return NULL;
};

std::pair<size_t, size_t> Object::Parse(const std::string& src, const size_t lbound, const size_t rbound)
{
	size_t _lbound = lbound;

	for (; (src[_lbound] <= ' ') && (_lbound < rbound); _lbound++);

	if (src[_lbound++] != '{')
		return ERROR_VAL;
	
	for (; (src[_lbound] <= ' ') && (_lbound < rbound); _lbound++);
	
	if (src[_lbound] == '}') 
		return std::pair<size_t, size_t>(lbound, _lbound + 1);

	while (_lbound < rbound)
	{
		std::string key;

		while (src[_lbound] != ':')
		{
			if (src[_lbound] > ' ')
				key += src[_lbound];
			if (_lbound < rbound)
				_lbound++;
			else
				return ERROR_VAL;
		};

		_lbound += 1;

		for (; (src[_lbound] <= ' ') && (_lbound < rbound); _lbound++);

		std::pair<std::pair<size_t, size_t>, JsonObject*> intermediate_result = ParseObj(src, _lbound, rbound);
		if (intermediate_result.first == ERROR_VAL)
			return ERROR_VAL;

		AddObject(key, intermediate_result.second);
		_lbound = intermediate_result.first.second;

		for (; (src[_lbound] <= ' ') && (_lbound < rbound); _lbound++);

		if (src[_lbound] == ',')
			_lbound += 1;
		else
			if (src[_lbound] == '}')
				break;
			else
				return ERROR_VAL;

		for (; (src[_lbound] <= ' ') && (_lbound < rbound); _lbound++);
	};

	return std::pair<size_t, size_t>(lbound, _lbound + 1);
};

std::string Object::Serialize(const std::map<std::string, std::string> options)
{
	std::string result = "{";

	for (std::map<std::string, JsonObject*>::iterator i = objlist.begin(); i != objlist.end(); i++)
	{
		if (i != objlist.begin())
			result += ',';
		result += i->first + ':';
		result += i->second->Serialize(options);
	};
	result += '}';
	return result;
};

////////////////////////////////////////////////////////////////////////////////////////////

std::pair<std::pair<size_t, size_t>, JsonObject*> JsonObject::ParseObj(const std::string& src, const size_t lbound, const size_t rbound)
{
	JsonObject* obj;
	std::pair<size_t, size_t> result;
	std::pair<std::pair<size_t, size_t>, JsonObject*> val;

	obj = new Number;
	result = obj->Parse(src, lbound, rbound);
	if (result != ERROR_VAL)
	{
		 result = std::pair<size_t, size_t>(lbound, result.second);
		 val.first = result;
		 val.second = obj;
		 return val;
	};
	delete obj;

	obj = new String;
	result = obj->Parse(src, lbound, rbound);
	if (result != ERROR_VAL)
	{
		 result = std::pair<size_t, size_t>(lbound, result.second);
		 val.first = result;
		 val.second = obj;
		 return val;
	};
	delete obj;

	obj = new Array;
	result = obj->Parse(src, lbound, rbound);
	if (result != ERROR_VAL)
	{
		 result = std::pair<size_t, size_t>(lbound, result.second);
		 val.first = result;
		 val.second = obj;
		 return val;
	};
	delete obj;

	obj = new Object;
	result = obj->Parse(src, lbound, rbound);
	if (result != ERROR_VAL)
	{
		 result = std::pair<size_t, size_t>(lbound, result.second);
		 val.first = result;
		 val.second = obj;
		 return val;
	};
	delete obj;

	val.first = ERROR_VAL;
	val.second = NULL;
	return val;
};
