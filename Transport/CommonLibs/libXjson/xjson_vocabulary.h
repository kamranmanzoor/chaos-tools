/***************************************************************************************/
/* Componentailty open software: xJSON                * (C) Componentality Oy, 2013    */
/***************************************************************************************/
/* xJSON vocabulary implementation. The goal of the vocabulary is to provide a         */
/* replacement feature for long strings. String is substituted with its index in       */
/* vocabulary. If string is repeated, the only index will be replicated so far.        */
/***************************************************************************************/
#ifndef XJSON_VOCABULARY_H
#define XJSON_VOCANULARY_H

#include <vector>
#include <string>
#include "../libjson/Serializable.h"
#include "../libjson/Parseable.h"

namespace CST
{
	namespace Common
	{
		namespace xJSON
		{
			// Serialization flag. If provided to BaseVocabulary serialize (as a map key), the appropriate
			// value is used as a start point of the vocabulary serialization
			extern const std::string STARTPOS_FLAG;

			// Save and load unsigned integer numbers in the most compact possible form
			// with 2-bit coding of the size
			std::string saveUInt(const unsigned long long val);
			unsigned long long loadUInt(const std::string str, int* bits = NULL);

			// Basic vocabulary: implements indexed table of strings with easy lookup of strings index
			class BaseVocabulary
			{
			protected:
				std::vector<std::string> mStorage;			// Data storage
				std::map<std::string, size_t> mIndex;		// Index for search by string
				std::vector<size_t> mFrequencies;			// Frequencies keeper
			public:
				BaseVocabulary() {};
				virtual ~BaseVocabulary() {};
			protected:
				// Add new item, index not to be used except special cases
				virtual void add(const std::string&, const size_t idx = (size_t)-1);
				// Retrieve item with given index
				virtual std::string get(const size_t);
				// Retrieve index for given string
				virtual size_t get(const std::string&);
				// Get total size of vocabulary
				virtual size_t getSize();
				// Reset vocabulary to empty
				virtual void reset();
			};

			// Serializable vocabulary
			class Vocabulary : 
				protected BaseVocabulary,
				public CST::Common::LibJSON::Serializable, 
				public CST::Common::LibJSON::Parseable
			{
			public:
				Vocabulary() {};
				Vocabulary(const Vocabulary& src) { operator=(src); }
				virtual ~Vocabulary() {};
				// Retrieve item with given index
				virtual std::string operator[](const size_t);
				// Retrieve index for given item
				virtual size_t operator[](const std::string);
				// Add new string (or update frequency if string is already there)
				virtual Vocabulary& operator+=(const std::string);
				// Copy
				virtual Vocabulary& operator=(const Vocabulary& src);
			public:
				// Save vocabulary as a string. If options contain STARTPOS_FLAG, its value will be converted to uint
				// and used as start position for vocabulary serialization
				virtual std::string Serialize(const CST::Common::LibJSON::OPTIONS options = CST::Common::LibJSON::OPTIONS());
				// Load vocabulary from binary string representation
				virtual std::pair<size_t, size_t> Parse(const std::string&, const size_t, const size_t);
				// Sort vocabulary by frequency and provide new index list
				virtual std::string sort();
				// Apply vocabulary re-sorting indeces. Attention: vocabulary must be a copy of the source
				// one as it was prior to sort()
				virtual size_t apply_sort(const std::string&, const size_t = 0);
			};
		}
	} // namespace CommonLibs
} // namespace CST

#endif