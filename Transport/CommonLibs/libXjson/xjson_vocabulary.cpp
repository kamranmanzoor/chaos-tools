#include "xjson_vocabulary.h"
#include <stdio.h>

#ifdef WIN32
#define sscanf sscanf_s
#endif

using namespace CST::Common::xJSON;

const std::string CST::Common::xJSON::STARTPOS_FLAG = "STARTPOS_OPT";

const unsigned char UNDER64 = 0x00;
const unsigned char UNDER16384 = 0x40;
const unsigned char UNDER1073741824 = 0x80;
const unsigned char LARGE = 0xC0;

std::string CST::Common::xJSON::saveUInt(const unsigned long long val)
{
	std::string result;
	unsigned char x0 = (val >> 0) & 0xFF;
	unsigned char x1 = (val >> 8) & 0xFF;
	unsigned char x2 = (val >> 16) & 0xFF;
	unsigned char x3 = (val >> 24) & 0xFF;
	unsigned char x4 = (val >> 32) & 0xFF;
	unsigned char x5 = (val >> 40) & 0xFF;
	unsigned char x6 = (val >> 48) & 0xFF;
	unsigned char x7 = (val >> 56) & 0xFF;
	if (val < 64)
	{
		result += (char)(x0 | UNDER64);
		return result;
	} else
	if (val < 16384)
	{
		result += (char)(x1 | UNDER16384);
		result += (char)(x0);
		return result;
	} else
	if (val < 1073741824L)
	{
		result += (char)(x3 | UNDER1073741824);
		result += (char)(x2);
		result += (char)(x1);
		result += (char)(x0);
		return result;
	}
	else
	{
		result += (char)(x7 | LARGE);
		result += (char)(x6);
		result += (char)(x5);
		result += (char)(x4);
		result += (char)(x3);
		result += (char)(x2);
		result += (char)(x1);
		result += (char)(x0);
		return result;
	}
}

unsigned long long CST::Common::xJSON::loadUInt(const std::string str, int* bits)
{
	if (bits)
		*bits = 0;
	if (str.empty())
		return (unsigned long long) -1;
	unsigned char x0 = (unsigned char) str[0];
	if ((x0 & 0xC0) == UNDER64)
	{
		if (bits)
			*bits = 8;
		return x0 & 0x3F;
	}
	else if ((x0 & 0xC0) == UNDER16384)
	{
		if (str.size() < 2)
			return (unsigned long long) - 1;
		if (bits)
			*bits = 16;
		unsigned long long x1 = (unsigned char) str[1];
		unsigned long long result;
		result = ((x0 & 0x3F) << 8) | x1;
		return result;
	}
	else if ((x0 & 0xC0) == UNDER1073741824)
	{
		if (str.size() < 4)
			return (unsigned long long) - 1;
		if (bits)
			*bits = 32;
		unsigned long long x1 = (unsigned char)str[1];
		unsigned long long x2 = (unsigned char)str[2];
		unsigned long long x3 = (unsigned char)str[3];
		unsigned long long result;
		result = ((x0 & 0x3F) << 24) | (x1 << 16) | (x2 << 8) | (x3 << 0);
		return result;
	}
	else
	{
		if (str.size() < 8)
			return (unsigned long long) - 1;
		if (bits)
			*bits = 64;
		unsigned long long x1 = (unsigned char)str[1];
		unsigned long long x2 = (unsigned char)str[2];
		unsigned long long x3 = (unsigned char)str[3];
		unsigned long long x4 = (unsigned char)str[4];
		unsigned long long x5 = (unsigned char)str[5];
		unsigned long long x6 = (unsigned char)str[6];
		unsigned long long x7 = (unsigned char)str[7];
		unsigned long long result;
		result = ((x0 & 0x3F) << 24) | (x1 << 16) | (x2 << 8) | (x3 << 0);
		result <<= 32;
		result |= (x4 << 24) | (x5 << 16) | (x6 << 8) | (x7 << 0);
		return result;
	}
}

std::string Vocabulary::operator[](const size_t idx)
{
	return get(idx);
}

size_t Vocabulary::operator[](const std::string str)
{
	return get(str);
}

Vocabulary& Vocabulary::operator+=(const std::string new_item)
{
	add(new_item);
	return *this;
}

Vocabulary& Vocabulary::operator=(const Vocabulary& src)
{
	mIndex = src.mIndex;
	mStorage = src.mStorage;
	mFrequencies = src.mFrequencies;
	return *this;
}

std::string Vocabulary::Serialize(const CST::Common::LibJSON::OPTIONS options)
{
	size_t startpos = 0;
	if (options.find(STARTPOS_FLAG) != options.end())
	{
		sscanf(options.find(STARTPOS_FLAG)->second.c_str(), "%u", &startpos);
	}
	std::string result = saveUInt(startpos);
	for (size_t i = startpos; i < getSize(); i++)
	{
		result += get(i) + (char)0;
	}
	result += (char)0;
	return result;
}

std::pair<size_t, size_t> Vocabulary::Parse(const std::string& str, const size_t _start, const size_t _end)
{
	std::string toparse = str.substr(_start);
	size_t idx = _start;
	int bits;
	unsigned long long startpos = loadUInt(toparse, &bits);
	if (!bits)
		return ERROR_VAL;
	idx += bits / 8;
	size_t pos = (size_t)startpos;
	while (str[idx])
	{ 
		std::string item;
		while (str[idx])
		{
			item += str[idx++];
			if (idx >= str.size())
				return ERROR_VAL;
		};
		add(item, pos++);
		idx += 1;
		if (idx >= str.size())
			return ERROR_VAL;
	}
	return std::pair<size_t, size_t>(_start, idx + 1);
}

std::string Vocabulary::sort()
{
	std::string result;
	std::vector<std::string> newstorage(mStorage.size());
	std::vector<size_t> freqs = mFrequencies;
	for (size_t i = 0; i < newstorage.size(); i++)
	{
		size_t maxidx = 0;
		for (size_t j = 0; j < mStorage.size(); j++)
		{
			if (freqs[j] > freqs[maxidx])
				maxidx = j;
		}
		newstorage[i] = mStorage[maxidx];
		mFrequencies[i] = freqs[maxidx];
		mIndex[newstorage[i]] = i;
		freqs[maxidx] = 0;
		result += saveUInt(maxidx);
	}
	mStorage = newstorage;
	result += (char)0xFF;
	return result;
}

size_t Vocabulary::apply_sort(const std::string& str, const size_t start)
{
	std::vector<std::string> newstorage(mStorage.size());
	std::vector<size_t> newfreqs(mFrequencies.size());
	size_t newidx = 0;
	size_t i;
	for (i = start; i < str.size();)
	{
		int bits;
		if ((char)0xFF == str[i])
			break;
		size_t idx = (size_t)loadUInt(str.substr(i), &bits);
		i += bits / 8;
		newstorage[newidx] = mStorage[idx];
		newfreqs[newidx] = mFrequencies[idx];
		mIndex[newstorage[newidx]] = newidx++;
	}
	mStorage = newstorage;
	mFrequencies = newfreqs;
	return i + 1;
}

/*****************************************************************************************/

void BaseVocabulary::add(const std::string& str, const size_t idx)
{
	size_t index = idx == (size_t)-1 ? getSize() : idx;
	if ((mIndex.find(str) != mIndex.end()) && (idx == (size_t)-1))
	{
		index = mIndex[str];
		mFrequencies[index] += 1;
	}
	else
	{
		if (index >= mStorage.size())
			mStorage.resize(index + 1);
		mStorage[index] = str;
		mIndex[str] = index;
		if (index >= mFrequencies.size())
		{
			mFrequencies.resize(index + 1);
			mFrequencies[index] = 1;
		}
	}
}

std::string BaseVocabulary::get(const size_t idx)
{
	if (idx < getSize())
		return mStorage[idx];
	else
		return std::string();
}

size_t BaseVocabulary::get(const std::string& str)
{
	if (mIndex.find(str) != mIndex.end())
		return mIndex[str];
	else
		return (size_t)-1;
}

size_t BaseVocabulary::getSize()
{
	return mStorage.size();
}

void BaseVocabulary::reset()
{
	mStorage.clear();
	mIndex.clear();
}
