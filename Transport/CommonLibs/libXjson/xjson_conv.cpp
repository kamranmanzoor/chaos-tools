#include "xjson_conv.h"
#include <stdio.h>

#ifdef WIN32
#define sprintf sprintf_s
#endif

using namespace CST::Common::xJSON;

std::string JSONtoBIN::convert(CST::Common::LibJSON::Number& n)
{
	std::string result;
	CST::Common::LibJSON::Number::VALUE val = n.GetValue();
	switch (n.GetType())
	{
	case CST::Common::LibJSON::Number::NULLVAL:
		result += (char)NULLVAL;
		break;
	case CST::Common::LibJSON::Number::BOOLEAN:
		if (val.boolean_value)
			result += (char)BOOL_TRUE;
		else
			result += (char)BOOL_FALSE;
		break;
	case CST::Common::LibJSON::Number::INTEGER:
		if (val.integer_value > 0)
		{
			result += (char)POSITIVE_INT;
			result += saveUInt((unsigned long long)val.integer_value);
		}
		else
		{
			result += (char)NEGATIVE_INT;
			result += saveUInt((unsigned long long)-val.integer_value);
		}
		break;
	case CST::Common::LibJSON::Number::FLOAT:
		double f = val.float_value;
		if (f < 0)
		{
			result += (char)NEGATIVE_FLOAT;
			f = -f;
		}
		else
			result += (char)POSITIVE_FLOAT;
		long long intpart = (long long)f;
		long long decpart = (long long)((f - (double)intpart) * 10000);
		result += saveUInt((unsigned long long) intpart);
		result += saveUInt((unsigned long long) decpart);
		break;
	}
	return result;
}

std::string JSONtoBIN::convert(CST::Common::LibJSON::String& s, const size_t voclimit)
{
	std::string result;
	std::string str = s.GetValue();
	if (str.size() <= voclimit)
	{
		*this += str;
		result += (char)VOCITEM;
		result += saveUInt((*this)[str]);
	}
	else
	{
		result += (char)STRING;
		result += str;
		result += (char)0;
	}
	return result;
}

std::string JSONtoBIN::convert(CST::Common::LibJSON::Array& arr)
{
	std::string result;
	result += (char)ARRAY;
	for (size_t i = 0; i < arr.Size(); i++)
	{
		CST::Common::LibJSON::JsonObject* jobj = arr[i];
		if (!jobj)
			continue;
		switch (jobj->GetObjectType())
		{
		case CST::Common::LibJSON::JsonObject::NUMBER:
			result += convert(*(CST::Common::LibJSON::Number*)jobj);
			break;
		case CST::Common::LibJSON::JsonObject::STRING:
			result += convert(*(CST::Common::LibJSON::String*)jobj, this->mVocLimit);
			break;
		case CST::Common::LibJSON::JsonObject::ARRAY:
			result += convert(*(CST::Common::LibJSON::Array*)jobj);
			break;
		case CST::Common::LibJSON::JsonObject::OBJECT:
			result += convert(*(CST::Common::LibJSON::Object*)jobj);
			break;
		}
	}
	result += (char)0xFF;
	return result;
}

std::string JSONtoBIN::convert(CST::Common::LibJSON::Object& obj)
{
	std::string result;
	result += (char)OBJECT;
	std::map<std::string, CST::Common::LibJSON::JsonObject*> content = obj.GetContent();
	for (std::map<std::string, CST::Common::LibJSON::JsonObject*>::iterator i = content.begin(); i != content.end(); i++)
	{
		*this += i->first;
		CST::Common::LibJSON::JsonObject* jobj = i->second;
		if (!jobj)
			continue;
		result += saveUInt((*this)[i->first]);
		switch (jobj->GetObjectType())
		{
		case CST::Common::LibJSON::JsonObject::NUMBER:
			result += convert(*(CST::Common::LibJSON::Number*)jobj);
			break;
		case CST::Common::LibJSON::JsonObject::STRING:
			result += convert(*(CST::Common::LibJSON::String*)jobj, this->mVocLimit);
			break;
		case CST::Common::LibJSON::JsonObject::ARRAY:
			result += convert(*(CST::Common::LibJSON::Array*)jobj);
			break;
		case CST::Common::LibJSON::JsonObject::OBJECT:
			result += convert(*(CST::Common::LibJSON::Object*)jobj);
			break;
		};
	};
	result += (char)0xFF;
	return result;
}

std::string JSONtoBIN::operator()(CST::Common::LibJSON::Object& obj)
{
	return convert(obj);
}

std::string JSONtoBIN::serialize()
{
	if (mLastSize == getSize())
		return std::string();
	std::string result;
	std::map<std::string, std::string> opts;
	char buf[25];
	sprintf(buf, "%d", this->mLastSize);
	opts[STARTPOS_FLAG] = buf;
	result += (char)VOCABULARY;
	result += Vocabulary::Serialize(opts);
	mLastSize = this->getSize();
	return result;
};

std::string JSONtoBIN::update()
{
	std::string result;
	result += (char)VOCUPDATE;
	result += Vocabulary::sort();
	return result;
}

void JSONtoBIN::renew()
{
	reset();
	mLastSize = 0;
}

std::string JSONtoBIN::operator()(const std::string& str, size_t& begin, size_t& end)
{
	CST::Common::LibJSON::Object obj;
	std::pair<size_t, size_t> result = obj.Parse(str, begin, end);
	if (result == ERROR_VAL)
		return std::string();
	begin = result.first;
	end = result.second;
	return operator()(obj);
}

/**********************************************************************************/

CST::Common::LibJSON::Object* BINtoJSON::convert(const std::string& str)
{
	CST::Common::LibJSON::Object* result = NULL;
	for (size_t i = 0; i < str.size();)
	{
		switch (str[i])
		{
		case (char)VOCABULARY:
			{
				std::pair<size_t, size_t> bounds = Vocabulary::Parse(str, i + 1, str.size());
				if (bounds == ERROR_VAL)
					return result;
				else
					i = bounds.second;
			}
			break;
		case (char)VOCUPDATE:
			{
				i = Vocabulary::apply_sort(str, i + 1);
				if (i == (size_t)-1)
					return result;
				}
			break;
		case (char)OBJECT:
			{
				std::pair<size_t, CST::Common::LibJSON::Object*> res = readObject(str, i + 1);
				return res.second;
			}
		}
	}
	return result;
}

CST::Common::LibJSON::JsonObject* BINtoJSON::getJobj(const std::string& str, size_t& idx)
{
	int bits;
	switch (str[idx])
	{
	case (char)VOCITEM:
	case (char)STRING:
		{
			std::pair<size_t, CST::Common::LibJSON::String*> s = readString(str, idx);
			if (s.first == (size_t)-1)
				return NULL;
			else
			{
				idx = s.first;
				return s.second;
			}
		}
		break;
	case (char)OBJECT:
		{
			std::pair<size_t, CST::Common::LibJSON::Object*> o = readObject(str, idx + 1);
			if (o.first == (size_t)-1)
				return NULL;
			else
			{
				idx = o.first;
				return o.second;
			}
		}
		break;
	case (char)ARRAY:
		{
			std::pair<size_t, CST::Common::LibJSON::Array*> a = readArray(str, idx + 1);
			if (a.first == (size_t)-1)
				return NULL;
			else
			{
				idx = a.first;
				return a.second;
			}
		}
		break;
	case (char)POSITIVE_INT:
		{
			CST::Common::LibJSON::Number& n = *new CST::Common::LibJSON::Number;
			idx += 1;
			unsigned long long val = loadUInt(str.substr(idx), &bits);
			idx += bits / 8;
			CST::Common::LibJSON::Number::VALUE value;
			value.integer_value = (int)val;
			n.Set(value, CST::Common::LibJSON::Number::INTEGER);
			return &n;
		}
		break;
	case (char)NEGATIVE_INT:
		{
			CST::Common::LibJSON::Number& n = *new CST::Common::LibJSON::Number;
			idx += 1;
			unsigned long long val = loadUInt(str.substr(idx), &bits);
			idx += bits / 8;
			CST::Common::LibJSON::Number::VALUE value;
			value.integer_value = -(int)val;
			n.Set(value, CST::Common::LibJSON::Number::INTEGER);
			return &n;
		}
		break;
	case (char)POSITIVE_FLOAT:
		{
			CST::Common::LibJSON::Number& n = *new CST::Common::LibJSON::Number;
			idx += 1;
			unsigned long long intpart = loadUInt(str.substr(idx), &bits);
			idx += bits / 8;
			unsigned long long decpart = loadUInt(str.substr(idx), &bits);
			idx += bits / 8;
			CST::Common::LibJSON::Number::VALUE value;
			value.float_value = (double)intpart + ((double)decpart) / 10000;
			n.Set(value, CST::Common::LibJSON::Number::FLOAT);
			return &n;
		}
		break;
	case (char)NEGATIVE_FLOAT:
		{
			CST::Common::LibJSON::Number& n = *new CST::Common::LibJSON::Number;
			idx += 1;
			unsigned long long intpart = loadUInt(str.substr(idx), &bits);
			idx += bits / 8;
			unsigned long long decpart = loadUInt(str.substr(idx), &bits);
			idx += bits / 8;
			CST::Common::LibJSON::Number::VALUE value;
			value.float_value = -((double)intpart + ((double)decpart) / 10000);
			n.Set(value, CST::Common::LibJSON::Number::FLOAT);
			return &n;
		}
		break;
	case (char)BOOL_FALSE:
		{
			CST::Common::LibJSON::Number& n = *new CST::Common::LibJSON::Number;
			idx += 1;
			CST::Common::LibJSON::Number::VALUE value;
			value.boolean_value = false;
			n.Set(value, CST::Common::LibJSON::Number::BOOLEAN);
			return &n;
		}
		break;
	case (char)BOOL_TRUE:
		{
			CST::Common::LibJSON::Number& n = *new CST::Common::LibJSON::Number;
			idx += 1;
			CST::Common::LibJSON::Number::VALUE value;
			value.boolean_value = true;
			n.Set(value, CST::Common::LibJSON::Number::BOOLEAN);
			return &n;
		}
		break;
	case (char)NULLVAL:
		{
			CST::Common::LibJSON::Number& n = *new CST::Common::LibJSON::Number;
			idx += 1;
			CST::Common::LibJSON::Number::VALUE value;
			value.boolean_value = false;
			n.Set(value, CST::Common::LibJSON::Number::NULLVAL);
			return &n;
		}
		break;
	}
	return NULL;
}

std::pair<size_t, CST::Common::LibJSON::Object*> BINtoJSON::readObject(const std::string& str, const size_t begin)
{
	size_t idx = begin;
	std::pair<size_t, CST::Common::LibJSON::Object*> result;
	result.first = (size_t)-1;
	result.second = new CST::Common::LibJSON::Object;
	while (str[idx] != (char)0xFF)
	{
		int bits;
		size_t vocidx = (size_t) loadUInt(str.substr(idx), &bits);
		std::string key = (*this)[vocidx];
		idx += bits / 8;
		CST::Common::LibJSON::JsonObject* jobj = getJobj(str, idx);
		if (jobj)
			result.second->AddObject(key, jobj);
	}
	result.first = idx + 1;
	return result;
}

std::pair<size_t, CST::Common::LibJSON::String*> BINtoJSON::readString(const std::string& str, const size_t begin)
{
	size_t idx = begin;
	std::pair<size_t, CST::Common::LibJSON::String*> result;
	result.first = (size_t)-1;
	result.second = new CST::Common::LibJSON::String;
	switch (str[idx++])
	{
	case (char)VOCITEM:
		{
			int bits;
			unsigned long long vocidx = loadUInt(str.substr(idx), &bits);
			result.second->SetValue((*this)[(size_t)vocidx]);
			idx += bits / 8;
		}
		break;
	case (char)STRING:
		{
			std::string s;
			while (str[idx])
			{
				s += str[idx++];
				if (idx >= str.size())
					return result;
			}
			idx += 1;
			result.second->SetValue(s);
		}
	}
	result.first = idx;
	return result;
}

std::pair<size_t, CST::Common::LibJSON::Array*> BINtoJSON::readArray(const std::string& str, const size_t begin)
{
	size_t idx = begin;
	std::pair<size_t, CST::Common::LibJSON::Array*> result;
	result.first = (size_t)-1;
	result.second = new CST::Common::LibJSON::Array;
	while (str[idx] != (char)0xFF)
	{
		CST::Common::LibJSON::JsonObject* jobj = getJobj(str, idx);
		if (jobj)
			result.second->Push(jobj);
	}
	result.first = idx + 1;
	return result;
}

std::string BINtoJSON::operator()(const std::string& str)
{
	std::string result;

	CST::Common::LibJSON::Object* obj = convert(str);
	if (obj)
	{
		result = obj->Serialize(std::map<std::string, std::string>());
		delete obj;
	}
	return result;
}